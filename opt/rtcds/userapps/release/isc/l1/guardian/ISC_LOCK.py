# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
from guardian import GuardState, GuardStateDecorator
from guardian.manager import NodeManager
from guardian.ligopath import userapps_path
import cdsutils
import time
import gpstime
import math
import subprocess
import os
from os import path
import csv


##################################################
# GUARDIAN SETTINGS

# nominal operational state
nominal = 'LOW_NOISE'
# initial request on initialization
request = 'IDLE'
# turn on monitoring of epics settings
ca_monitor = False
ca_monitor_notify = False

##################################################
# IMPORT GUARDIAN CODE

# system config:
# sys/<ifo>/guardian/ifoconfig.py
import ifoconfig
# SQZ config file (sqz/<ifo>/guardian/sqzconfig.py):
import sqzconfig

import isclib.drmi_wfs
import isclib.matrices as matrix
import bs_m2_actuator
import isclib.asdc_centering_matrix as asdc_centering_matrix
import isclib.quad_esd_switch as quad_esd_switch
import isclib.tcs_flipperscript as tcs_flipperscript

from isclib.findIrResonances import ScanAlsOffset
import isclib.findIrResonances as irResonances
# for alignment stuff
from optic import Optic
from align_dither import DigitalDemodulation, AlignmentDither

from isclib.epics_average import EzAvg, ezavg

import cdslib
FRONT_ENDS = cdslib.CDSFrontEnds()

from ISC_FILTER_BANKS import ISC_FILTER_BANKS

##################################################
# SCRIPT STUFF 

from subprocess import Popen, PIPE, check_output, call

class myProc:
    def __init__(self, path):
        self.file = path
        self.exit_con = None
        self.proc = None

# This list is all the EXTERNAL processes/scripts.  The states use the
# names below to call, kill, and cleanup the scripts/processes.
process_list = {
    'find_ir_resonances': myProc(
        '/opt/rtcds/userapps/release/lsc/l1/scripts/transition/als/findIRResonances.py',
#        '/opt/rtcds/userapps/release/lsc/l1/scripts/transition/als/findIRResonances_noFDDs.py',
        ),
    'align_restore': myProc(
        '/opt/rtcds/userapps/release/sus/common/scripts/align_restore.py',
        ),
}

# These functions are for calling the subscripts.  The kill() command
# doesn't work on cdsustils.servo commands called from within a bash
# script.  Servo commands should thus be called as daemon threads in a
# python script, then they can be killed cleanly.
def startScript(script_name):
    global process_list
    process_list[script_name].exit_con = None
    process_list[script_name].proc = Popen(process_list[script_name].file)
    process_list[script_name].exit_con = process_list[script_name].proc.poll()

def scriptRunning(script_name):
    global process_list
    if process_list[script_name].proc == None:
        return False
    process_list[script_name].exit_con = process_list[script_name].proc.poll()
    return process_list[script_name].exit_con == None

def exitCondition(script_name):
    global process_list
    return process_list[script_name].exit_con

def cleanupScript(script_name):
    global process_list
    process_list[script_name].proc = None
    process_list[script_name].exit_con = None    
    
def killScript(script_name):
    global process_list
    if process_list[script_name].proc != None:
        try:
            # Terminate and remove from the register of living processes
            process_list[script_name].proc.kill()
            cleanupScript(script_name)
            print('killed ' + script_name)
        except:
            print('Tried to kill ' + script_name + ' which was not actually running')


def killAllScripts():
    global process_list
    for i, v in enumerate(process_list.keys()):
        killScript(v)

##################################################

# check for some error conditions
def MC_in_fault():
    # currently take as input the state node object, which is for
    # controlling other guardian nodes as a manager

    # currently these faults are only checked in acquire and in the
    # fault state, if the MC is not already locked.  This is intended
    # to help a user diagnose problems if the MC is not locking, but
    # not to interefere with the MC lock needlessly by taking it out
    # of the locked state when it is still locked
    message = []
    flag = False

    # a hierarchy of conditions to check if there is light on the refl
    # PD (only give user most relevant fault message)
    if ezca['PSL-PMC_LOCK_ON'] != 1:
        notify("PMC unlocked")
        flag = True
    elif ezca['PSL-PERISCOPE_A_DC_ERROR_FLAG'] != 1:
        notify("PSL periscope PD error (low light?)")
        flag = True
    elif ezca['IMC-REFL_DC_OUTPUT'] < ifoconfig.imc_refl_no_light_threshold:
        notify("PSL REFL PD no light (check PZT/MC1 alignment)")
        flag = True

    ## check if FSS is locked # REFCAV bypass
    #if ezca['PSL-FSS_AUTOLOCK_STATE'] != 4:
    #    notify("FSS unlocked")
    #    flag = True

    # check HAM 2/3 ISI watchdogs
    # FIXME: this should look at ISI guardian nodes
    if ezca['ISI-HAM2_WD_MON_STATE_INMON'] != 1:
        notify("HAM2 ISI tripped")
        flag = True
    if ezca['ISI-HAM3_WD_MON_STATE_INMON'] != 1:
        notify("HAM3 ISI tripped")
        flag = True

    # TODO: check for MC2 trips here

    return flag

# return True if requested bit is set
def bitget(i,bit):
    if i == 0:
        return False
    if bit==0:
        return i & 1 == 1
    for z in range(bit):
        i >>= 1
    return i & 1 == 1

# check for lock
def MC_is_locked():
    return ezca['IMC-MC2_TRANS_SUM_INMON'] >= 50*ezca['IMC-PWR_IN_OUTPUT']

def ALS_X_locked():
    return ezca['ALS-C_TRX_A_LF_OUT16'] > 60

# When ALS DIFF loses lock, the integrators in the servo immediately
# runs away and hit the limits.  This is a pretty good indicator of
# the DIFF losing lock.
def ALS_Y_locked():
    return ezca['ALS-C_TRY_A_LF_OUT16'] > 60

# Only use POP18I, that way this doesn't screw it up if the user wants to tweak just the 
# PRMI before moving on to the DRMI.
def DRMI_locked():
    return ezca['LSC-POPAIR_B_RF18_I_NORM_MON'] >= ifoconfig.drmi_locked_threshold_pop18i


def MC_locked_confirm():
    #JCB 2016/12/1, request from Keith to remove NDS call (part of cdsutils.avg) to see if it improves NDS response time
    #a_value = cdsutils.avg(4, 'IMC-IMC_TRIGGER_INMON')
    a_value = 0
    for x in range(0,40):
        a_value = a_value + ezca['IMC-IMC_TRIGGER_INMON']
        time.sleep(0.1)
    a_value = a_value/40.0
    if a_value > 85.0:
        return True
    else:
        return False

def ifoBurt(file_name):
    global process_list

    # build the burt command
    ifo = IFO.lower()
    snap = userapps_path('isc', ifo, 'burtfiles', file_name)
    log('BURT RESTORE: ' + snap)

    # run burtwb (this is blocking, but quick)
    call(['burtwb', '-f', snap])
    #ezca.burtwb(snap)


def clear_filters_down():
    """clear filter histories

    see USERAPPS/isc/l1/guardian/ISC_FILTER_BANKS.py for list of
    filters to clear

    """
    for filt in ISC_FILTER_BANKS:
        ezca.write(filt+'_RSET', 2, wait=True, log=True)

def set_esd_state(etm,state):

    if state == "LV":
        hvstate = 0
    elif state == "HV":
        hvstate = 1

    for quadrant in ['UL','UR','LL','LR']:
        ezca["SUS-{}_BIO_L3_{}_STATEREQ".format(etm,quadrant)] = int(not(hvstate)) + 1 
        ezca["SUS-{}_BIO_L3_{}_HVDISCONNECT_SW".format(etm,quadrant)] = hvstate
        ezca["SUS-{}_BIO_L3_{}_VOLTAGE_SW".format(etm,quadrant)] = hvstate
        ezca["SUS-{}_L3_ESDOUTF_{}_LIN_BYPASS_SW".format(etm,quadrant)] = not(hvstate)


def set_asc_inp_mtrx():
    # Go to 3 REFL WFS   # AE changed pitches for MK sensing oct 2018
    matrix.asc_input_pit['INP2','REFL_A_RF9_I'] = -0.33 # 0.11*4   
    matrix.asc_input_pit['INP2','REFL_A_RF45_I'] = -2.7  #-0.82*4 
    matrix.asc_input_pit['INP2','REFL_B_RF9_I'] =  0   
    matrix.asc_input_pit['INP2','REFL_B_RF45_I'] = 4.42 # 0.94*4  

    matrix.asc_input_yaw['INP2','REFL_A_RF9_I'] = -1.14 #0.02*4   #1.3    #-1.25
    matrix.asc_input_yaw['INP2','REFL_A_RF45_I'] = -3.16 #-0.94*4 #-0.98 #0.125
    matrix.asc_input_yaw['INP2','REFL_B_RF9_I'] =  0   #0  #-1.3   #1.25
    matrix.asc_input_yaw['INP2','REFL_B_RF45_I'] = 5.53#1.56*4  #0.76  #0.82
'''
    ### PR2 WFS
    # Set input matrix

    if ifoconfig.popx:
        matrix.asc_input_pit.zero(row='PRC2')
        matrix.asc_input_yaw.zero(row='PRC2')
        time.sleep(0.1)
        matrix.asc_input_pit['PRC2','POP_X_RF36_I'] = 100
        matrix.asc_input_yaw['PRC2','POP_X_RF36_I'] = -100
    else:
        matrix.asc_input_pit['PRC2','REFL_A_RF9_I'] = -1.68 # -3.98 AE new matrix jan 2023, scaled for 1 pregain 
        matrix.asc_input_pit['PRC2','REFL_A_RF45_I'] = 3.04     # 1.75 
        matrix.asc_input_pit['PRC2','REFL_B_RF9_I'] = 0
        matrix.asc_input_pit['PRC2','REFL_B_RF45_I'] = 0.2 # 0.41
        matrix.asc_input_pit['PRC2','REFL_B_RF45_Q'] = 0 
        # matrix.asc_input_pit['PRC2','POP_X_RF36_I'] = 36 # 0 
        matrix.asc_input_pit['PRC2','POP_X_RF36_I'] = 0

        matrix.asc_input_yaw['PRC2','REFL_A_RF9_I'] = -2.2 # -11.28 
        matrix.asc_input_yaw['PRC2','REFL_A_RF45_I'] = 3.34   # 2.83
        matrix.asc_input_yaw['PRC2','REFL_B_RF9_I'] = 0
        matrix.asc_input_yaw['PRC2','REFL_B_RF45_I'] = 0.54   # 0.99
        # matrix.asc_input_yaw['PRC2','POP_X_RF36_I'] = -120
        matrix.asc_input_yaw['PRC2','POP_X_RF36_I'] = 0
'''

def set_asc_pr2_mtrx():

    if ifoconfig.popx:
        matrix.asc_input_pit.zero(row='PRC2')
        matrix.asc_input_yaw.zero(row='PRC2')
        time.sleep(0.1)
        matrix.asc_input_pit['PRC2','POP_X_RF36_I'] = 100
        matrix.asc_input_yaw['PRC2','POP_X_RF36_I'] = -100
    else:
        matrix.asc_input_pit['PRC2','REFL_A_RF9_I'] = -1.68 # -3.98 AE new matrix jan 2023, scaled for 1 pregain 
        matrix.asc_input_pit['PRC2','REFL_A_RF45_I'] = 3.04     # 1.75 
        matrix.asc_input_pit['PRC2','REFL_B_RF9_I'] = 0
        matrix.asc_input_pit['PRC2','REFL_B_RF45_I'] = 0.2 # 0.41
        matrix.asc_input_pit['PRC2','REFL_B_RF45_Q'] = 0 
        # matrix.asc_input_pit['PRC2','POP_X_RF36_I'] = 36 # 0 
        matrix.asc_input_pit['PRC2','POP_X_RF36_I'] = 0

        matrix.asc_input_yaw['PRC2','REFL_A_RF9_I'] = -2.2 # -11.28 
        matrix.asc_input_yaw['PRC2','REFL_A_RF45_I'] = 3.34   # 2.83
        matrix.asc_input_yaw['PRC2','REFL_B_RF9_I'] = 0
        matrix.asc_input_yaw['PRC2','REFL_B_RF45_I'] = 0.54   # 0.99
        # matrix.asc_input_yaw['PRC2','POP_X_RF36_I'] = -120
        matrix.asc_input_yaw['PRC2','POP_X_RF36_I'] = 0


def set_bafflePd_dark_offsets():
 
    ezca['AOS-ITMX_BAFFLEPD_1_OFFSET'] = 0.118
    ezca['AOS-ITMX_BAFFLEPD_4_OFFSET'] = 0.093
    ezca['AOS-ETMX_BAFFLEPD_1_OFFSET'] = 0.150
    ezca['AOS-ETMX_BAFFLEPD_4_OFFSET'] = 0.202
    ezca['AOS-ITMY_BAFFLEPD_1_OFFSET'] = 0.130
    ezca['AOS-ITMY_BAFFLEPD_4_OFFSET'] = 0.095
    ezca['AOS-ETMY_BAFFLEPD_1_OFFSET'] = 0.310
    ezca['AOS-ETMY_BAFFLEPD_4_OFFSET'] = 0.448


def turn_off_violins():

    for sus in ['ETMX','ETMY','ITMX','ITMY']:
        for mode in range(40):
            ezca.switch('SUS-{}_L2_DAMP_MODE{}'.format(sus,mode+1),'OUTPUT','OFF')

#################################################
# DECORATORS
#################################################

class assert_mc_lock(GuardStateDecorator):
    def pre_exec(self):
        if not MC_is_locked():
            log('MC lost lock!')
            return 'LOCKLOSS'

class assert_als_y_lock(GuardStateDecorator):
    def pre_exec(self):
        if not ALS_Y_locked():
            log('ALS Y lost lock!')
            return 'LOCKLOSS'

class assert_drmi_lock(GuardStateDecorator):
    def pre_exec(self):
        if not DRMI_locked():
            log('DRMI lost lock!')
            return 'LOCK_DRMI'
'''
class isi_watchdog_checker(GuardStateDecorator):
    def pre_exec(self):
        isis = []
        for ham in range(2,6+1):
            isis.append("HAM"+str(ham))
        for bsc in ["ITMX","ITMY","BS","ETMX","ETMY"]:
            for stage in ["ST1","ST2"]:
                isis.append(bsc+"_"+stage)

        for isi in isis:
            if not(ezca["ISI-{}_WD_MON_STATE_INMON".format(isi)] == 1):
                notify("{} WD is tripped".format(isi))

'''

##################################################
# NODES
##################################################

#newnode
nodes = NodeManager([
    'IMC_LOCK',
    'TIDAL_X',
    'TIDAL_Y',
    'FAST_SHUTTER',
    'OMC_LOCK',
])

nodes_loose = NodeManager([
    'ALS_XARM',
    'ALS_YARM',
    'SQZ_MANAGER',
    'SEI_BS', 
    'SUS_PI',
    'TCS_RH',
    'TCS_ITMX_CO2',
    'TCS_ITMY_CO2',
    'DIAG_INF',
    'ALIGN_BS',
    'INITIAL_ALIGNMENT',
    'SUS_1STVIOLIN',
    #'SUS_2NDVIOLIN',
])

nodes_optics = NodeManager([
    'SUS_BS',
    'SUS_ETMX',
    'SUS_ETMY',
    'SUS_ITMX',
    'SUS_ITMY',
    'SUS_PRM',
    'SUS_SRM',
    'SUS_PR2',
    'SUS_SR2',
])

'''
nodes_sei = NodeManager([
    'SEI_ITMX',
    'SEI_ITMY',
    'SEI_ETMX',
    'SEI_ETMY',
    'SEI_HAM2',
    'SEI_HAM3',
    'SEI_HAM4',
    'SEI_HAM5',
    'SEI_HAM6',
])
'''

##################################################
# Variables
##################################################

# Convenient access for QUADs and HSTSs alignments.

itmx = Optic("ITMX")
etmx = Optic("ETMX")
itmy = Optic("ITMY")
etmy = Optic("ETMY")
prm = Optic("PRM")
srm = Optic("SRM")


##################################################
# STATES
##################################################

##################################################
class INIT(GuardState):
    index = 0
    request = True

    def main(self):
        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed() #newnode
        log(os.environ['NDSSERVER'])
        #return matrix
        return True

    def run(self):
        if MC_in_fault():
            return 'MC_FAULT'
        return True

##################################################
class IDLE(GuardState):
    index = 1
    goto = True

    def main(self):
        # Kill all the running scripts
        killAllScripts()
 
    def run(self):
        #if MC_in_fault():
            #return 'MC_FAULT'
        
        #if MC_locked_confirm():
            #return True            
        return True

        #notify('MC not locked: cannot leave IDLE state')

##################################################
# wait for all faults to be cleared, the jump to IDLE to find state
class MC_FAULT(GuardState):
    index = 6
    redirect = False
    request = False

    def run(self):
        if MC_in_fault():
            return
        return 'IDLE'

##################################################
# This is for an algorithm failure, when the algorithm gets confused.  
class LOCK_FAULT(GuardState):
    index = 7
    redirect = False
    request = False

    def run(self):
        notify("LOCK FAULT: check ALS status")
        # Just sit here until a user requests one of the "goto" states: IDLE or DOWN
        return True

##################################################
class LOCKLOSS(GuardState):
    """Record lockloss event"""
    index = 2
    redirect = False
    request = False

    def run(self):
        #TODO: do we need equivalent of below?
        if ezca['ODC-OBSERVATORY_MODE'] == 10:
            ezca['ODC-OBSERVATORY_MODE'] = 21
        return True

##################################################
# reset everything to the good values for acquistion.  These values are stored in the down script.

class DOWN(GuardState):
    index = 10
    goto = True

    def main(self):

        # kill all the running scripts #FIXME: No longer needed?
        killAllScripts()

        for stage in ['M0','L1','L2','L3']:
            ezca.switch('LSC-{}_LOCK'.format(stage),'OUTPUT','OFF')

        # reassert managment over all nodes before setting their state
        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed() #newnode
        
        # command subordinate guardians
        nodes['IMC_LOCK'].set_request('LOCKED_1W')
        ezca['PSL-GUARD_POWER_REQUEST'] = 1

        nodes['OMC_LOCK'].set_request('DOWN')

        ezca.switch('LSC-DARM','INPUT','OFFSET','OUTPUT','OFF')
        ezca['LSC-DARM_GAIN'] = 0

        ezca['LSC-CONTROL_ENABLE'] = 'OFF'
        ezca['ASC-WFS_SWTCH'] = 'OFF'
        ezca['ASC-ADS_GAIN'] = 0

        # shut off violin damping for all quads (quickly, so do it near the top)
        ezca['SUS-ITMX_L2_DAMP_OUT_SW'] = 'OFF'
        ezca['SUS-ITMY_L2_DAMP_OUT_SW'] = 'OFF'
        ezca['SUS-ETMX_L2_DAMP_OUT_SW'] = 'OFF'
        ezca['SUS-ETMY_L2_DAMP_OUT_SW'] = 'OFF'

        # set tm p/y gains back to -1 
        ezca['SUS-ITMX_M0_DAMP_P_GAIN'] = -1
        ezca['SUS-ETMX_M0_DAMP_P_GAIN'] = -1
        ezca['SUS-ITMY_M0_DAMP_P_GAIN'] = -1
        ezca['SUS-ETMY_M0_DAMP_P_GAIN'] = -1

        ezca['SUS-ITMX_M0_DAMP_Y_GAIN'] = -1
        ezca['SUS-ETMX_M0_DAMP_Y_GAIN'] = -1
        ezca['SUS-ITMY_M0_DAMP_Y_GAIN'] = -1
        ezca['SUS-ETMY_M0_DAMP_Y_GAIN'] = -1

        # Switch of OL DAMPING (DC)
        #for sus in ['ITMX','ITMY','ETMX','ETMY']:
        #    for dof in ['P','Y']:
        #        ezca.switch('SUS-'+sus+'_L2_OLDAMP_'+dof,'INPUT','OFF')

        #for sus in ['ITMX','ITMY','ETMX','ETMY']:
        #    for dof in ['PIT','YAW']:
        #        ezca.switch('SUS-'+sus+'_L3_OPLEV_'+dof,'OFFSET','OFF')

        #for sus in ['ITMX','ITMY','ETMX','ETMY']:
        #    for dof in ['P','Y']:
        #        ezca['SUS-'+sus+'_L2_OLDAMP_'+dof+'_RSET'] = 2

        if nodes_loose['INITIAL_ALIGNMENT'].state != 'INITIAL_ALIGN_IDLE':
            nodes_loose['INITIAL_ALIGNMENT'] = 'INITIAL_ALIGN_IDLE'
        elif nodes_loose['ALIGN_BS'].state != 'BS_ALIGN_IDLE':
            nodes_loose['ALIGN_BS'] = 'BS_ALIGN_IDLE'

        if nodes_loose['ALS_XARM'].state != 'SHUTTERED':
            nodes_loose['ALS_XARM'] = 'QPDS_LOCKED'
        if nodes_loose['ALS_YARM'].state != 'SHUTTERED':
            nodes_loose['ALS_YARM'] = 'QPDS_LOCKED'

        ########################################################################################
        ### Ramp down DriveAlign outputs
        # "other actuators" equals non DARM actuators
        quads = ["ETMX","ETMY","ITMX","ITMY"]
        other_actuators = quads
        for actuator in ifoconfig.darm_actuators:
            other_actuators.remove(actuator)

        # L2
        vals = {'TRAMP':0.1,'GAIN':0}
        for chan in ['TRAMP','GAIN']:
            for sus in ["ETMX","ETMY"]:
                ezca.write("SUS-{0}_L2_DRIVEALIGN_L2L_{1}".format(sus,chan),vals[chan])
                time.sleep(0.1)

        # L1
        vals = {'TRAMP':0.1,'GAIN':0}
        for chan in ['TRAMP','GAIN']:
            # ramp down the darm actuator drivealign banks (either ETMX or ETMY)
            for sus in ifoconfig.darm_actuators:
                # zero L2 stage for darm actuator quads
                #ezca.write("SUS-{0}_L2_DRIVEALIGN_L2L_{1}".format(sus,chan),vals[chan])
                # zero L1 stage for darm actuator quads
                for coup in ['L2L','P2P','Y2Y','L2P','L2Y']:
                    ezca.write("SUS-{0}_L1_DRIVEALIGN_{1}_{2}".format(sus,coup,chan),vals[chan])
            # ramp down the other quad drivealign banks 
            for sus in other_actuators:
                 for coup in ['P2P','Y2Y']:
                     ezca.write("SUS-{0}_L1_DRIVEALIGN_{1}_{2}".format(sus,coup,chan),vals[chan])
            time.sleep(0.1)

        ezca["SUS-ETMX_L1_DRIVEALIGN_L2P_RSET"] = 2
        ezca["SUS-ETMX_L1_DRIVEALIGN_L2Y_RSET"] = 2

        #Put HEPI tidal offloading back to default
        ezca['HPI-ETMX_ISCINF_LONG_GAIN']=1   #BOTHACTUATORS 20190107
        #Put L2 L gains back to nominal 
        ezca['SUS-ETMX_L2_LOCK_L_GAIN']=1    #BOTHACTUATORS 20190107
        ezca['SUS-ETMY_L2_LOCK_L_GAIN']=1    #BOTHACTUATORS 20190107

        # close SQZ diverter
        #ezca['SYS-MOTION_C_BDIV_E_CLOSE'] = 1        #Uncomment this line to leave squeezer self managed - nominally squeezer is self managed

        #In case SQZ_MANAGER needs to revive sub-guardians
        ezca['GRD-SQZ_MANAGER_REQUEST'] = 'INIT'
        time.sleep(0.1)

        if sqzconfig.nosqz:
            nodes_loose['SQZ_MANAGER'] = 'NO_SQUEEZING'
        else:
            nodes_loose['SQZ_MANAGER'] = 'WAIT_IFO_2'
            #nodes_loose['SQZ_MANAGER'] = 'DOWN'

        #Halt PI monitoring/damping, moved 09/05/2023, JCB VB
        nodes_loose['SUS_PI'] = 'IFO_DOWN'

        # clear WFS (AM 20150830 mod oFn 20161219)
#        for chan in ['TRAMP','GAIN']:  #Some race condition with snap restore results in not getting gains back turn off outputs instead CB 20180929
#        Put in a bogus channel write to test when ASC fails to reload
        ezca['ASC-CHARD_P_TRAMP'] = 4.9 # From a nominal value of 5 just to create a sdf diff for testing
 
        #for chan in ['OUTPUT']:  
        for rot in ['P','Y']:
            #for mode in ['D','C']:
            for stiffness in ['DHARD','CHARD','DSOFT','CSOFT']:
                ezca.switch('ASC-' + stiffness + '_' + rot, 'OUTPUT', 'OFF')
            for dof in ['INP2','PRC2','SRC1','SRC2','MICH']:
                ezca.switch('ASC-' + dof + '_' + rot, 'OUTPUT', 'OFF')
        time.sleep(0.1)
        
        # Turn off HAM1 FF TO CHARD
        ezca['ASC-INP1_P_TRAMP'] = 1
        ezca['ASC-INP1_P_GAIN'] = 0

        # Turn off offsets in TMS suspensions
        for tms in ["X","Y"]:
            for dof in ["P","Y"]:
                ezca.switch('SUS-TMS{}_M1_TEST_{}'.format(tms,dof), 'OFFSET', 'OFF')

	    # Turn off TM oplev damping #SMA 11/20/2016 Turn'em off! 
        # don't use now so commented out
        # for sus in ['ITMX','ITMY','ETMX','ETMY']:
            # ezca['SUS-' + sus + '_L1_OL1:SUS-ITMY_BIO_L1_MONLDAMP_P_GAIN'] = 0

        # ramp down PRM and PR2 top mass drivealign outputs AP 09/14/2016 
        ezca['SUS-PRM_M1_DRIVEALIGN_L2L_TRAMP'] = 0.1
        ezca['SUS-PR2_M1_DRIVEALIGN_L2L_TRAMP'] = 0.1
        ezca['SUS-SR2_M1_DRIVEALIGN_L2L_TRAMP'] = 0.1
        time.sleep(0.05)
        ezca['SUS-PRM_M1_DRIVEALIGN_L2L_GAIN'] = 0
        ezca['SUS-PR2_M1_DRIVEALIGN_L2L_GAIN'] = 0

        #Turn off PR2 M2 A2L
        ezca.switch('SUS-PR2_M2_DRIVEALIGN_P2L','OUTPUT','OFF')
        ezca.switch('SUS-PR2_M2_DRIVEALIGN_Y2L','OUTPUT','OFF')

	    # moved here by AE to prioritize SUS zeroing
	    # turn off tidal offload
        nodes['TIDAL_X'] = 'SERVOS_OFF'
        nodes['TIDAL_Y'] = 'SERVOS_OFF'

        #ezca.switch('SUS-ETMY_M0_TEST_L','OFFSET','OFF')
        
        # turn off BS stage 2
        nodes_loose['SEI_BS'] = 'ISOLATED_DAMPED'


        ##################################################
        # RESET LSC DARM filters
        ezca['LSC-L3_ISCINF_RSET'] = 2
        for stage in ['L3','L2','L1','M0']:
            ezca['LSC-{}_LOCK_RSET'.format(stage)] = 2
            time.sleep(0.1)

        ##################################################
        # SUS RESET

        # Resets violin, bounce and roll mode damping filters
        # FIXME: burtwb was not working on l1guardian1
        #ifoBurt('quad_mode_damp.snap')
        FRONT_ENDS['asc'].sdf_load('l1asc_quad_mode_damp', loadto='edb', confirm=False)
        FRONT_ENDS['susetmx'].sdf_load('l1susetmx_quad_mode_damp', loadto='edb', confirm=False)
        FRONT_ENDS['susetmy'].sdf_load('l1susetmy_quad_mode_damp', loadto='edb', confirm=False)
        FRONT_ENDS['susitmx'].sdf_load('l1susitmx_quad_mode_damp', loadto='edb', confirm=False)
        FRONT_ENDS['susitmy'].sdf_load('l1susitmy_quad_mode_damp', loadto='edb', confirm=False)


        nodes_loose['SUS_1STVIOLIN'] = 'UNDAMP_ALL'
        #turn_off_violins()

        # Turn off PRM/2 M3 dac offsets
        for optic in ['PRM', 'PR2', 'SR2']:
            ezca['SUS-' + optic + '_M3_DRIVEALIGN_L2L_TRAMP'] = 5
            time.sleep(0.1)
            ezca.switch('SUS-' + optic + '_M3_DRIVEALIGN_L2L','OFFSET','OFF')

        # switch off tidal
        # HACK: wait=False here because this kept throwing "Did not
        # observe effect of writing value to switch channel" EZCA
        # errors.  maybe because of long ramp times???
        # hack no work - no 'wait' option recognized
        ezca.switch('SUS-ETMX_M0_TIDAL_L', 'INPUT', 'OFFSET', 'OFF', wait=False)
        ezca.switch('SUS-ETMY_M0_TIDAL_L', 'INPUT', 'OFFSET', 'OFF', wait=False)
        ezca['SUS-ETMX_M0_TIDAL_L_GAIN'] = 0
        ezca['SUS-ETMY_M0_TIDAL_L_GAIN'] = 0

        # Make sure DIAG_INF is constantly chaecking for 1e20s
        nodes_loose['DIAG_INF'] = 'RESET_INF'


        # CLEAR FILTER HISTORIES
        clear_filters_down()

        # restore SUS down settings
        #ifoBurt('sus_down.snap') #/opt/rtcds/userapps/release/isc/l1/burtfiles/down_state_snaps
        FRONT_ENDS['susbs'].sdf_load('l1susbs_sus_down', loadto='edb', confirm=False)
        FRONT_ENDS['susetmx'].sdf_load('l1susetmx_sus_down', loadto='edb', confirm=False)
        FRONT_ENDS['susetmy'].sdf_load('l1susetmy_sus_down', loadto='edb', confirm=False)
        FRONT_ENDS['susitmx'].sdf_load('l1susitmx_sus_down', loadto='edb', confirm=False)
        FRONT_ENDS['susitmy'].sdf_load('l1susitmy_sus_down', loadto='edb', confirm=False)
        FRONT_ENDS['suspr2'].sdf_load('l1suspr2_sus_down', loadto='edb', confirm=False)
        FRONT_ENDS['susprm'].sdf_load('l1susprm_sus_down', loadto='edb', confirm=False)
        FRONT_ENDS['sussr2'].sdf_load('l1sussr2_sus_down', loadto='edb', confirm=False)
        FRONT_ENDS['sussrm'].sdf_load('l1sussrm_sus_down', loadto='edb', confirm=False)

        # restore LSC and ASC down settings
        #l1lsc_isc_down was a copy of l1lsc_safe, with L1:IMC-L filter settings removed completely
        #Relying on IMC_LOCK to do the right thing with it
        #JCB 20230912, adding in emergency DOWN alternate no check it has been pushed path
        if 'EZCA_CA' in os.environ and os.environ['EZCA_CA'] == 'NOFAIL':
            FRONT_ENDS['lsc'].sdf_load('l1lsc_isc_down', loadto='edb', confirm=False)
            FRONT_ENDS['omc'].sdf_load('safe', loadto='edb', confrim=False)
            FRONT_ENDS['asc'].sdf_load('safe', loadto='edb', confirm=False)
            #Give some time to actually load and not immediately jump to the next state below
            time.sleep(2)
        else:
            FRONT_ENDS['lsc'].sdf_load('l1lsc_isc_down', loadto='edb')
            FRONT_ENDS['omc'].sdf_load('safe', loadto='edb')
            FRONT_ENDS['asc'].sdf_load('safe', loadto='edb')

        # ESSENTIAL FAST RESETS ABOVE HERE
        ####################################################
        ####################################################
        # slower reseting of locking settings

        # set ALL front end TABLES to the 'safe' state - S. Aston 09/28/2018
        #subprocess.call(['/opt/rtcds/userapps/trunk/isc/l1/guardian/All_SDF_safe.sh'])
        FRONT_ENDS.sdf_load('safe', loadto='table', fast=True)
        # set lsc file to be the one run in down AE 20190208
        if 'EZCA_CA' in os.environ and os.environ['EZCA_CA'] == 'NOFAIL':
            FRONT_ENDS['lsc'].sdf_load('l1lsc_isc_down',loadto='table', confirm=False)
            time.sleep(2)
        else:
            FRONT_ENDS['lsc'].sdf_load('l1lsc_isc_down',loadto='table')

        #Compensate SPOPs for the T=0.3 ND filters
        for dmd in ['18','90']:
            for ph in ['I','Q']:
                ezca.switch('LSC-POPAIR_B_RF{}_{}'.format(dmd,ph),'FM4','ON')


        #Load SUS_BS_M2_EUL2OSEM ramping matrix
        ezca['SUS-BS_M2_EUL2OSEM_LOAD_MATRIX'] = 1

        # Reset HAM6 WD counter
        ezca['ISI-HAM6_SATCLEAR'] = 1

        # Set IMC VCO frequency to mid range
        #ezca['IMC-VCO_TUNEOFS'] = 0
        ezca['IMC-VCO_TUNEOFS'] = 4.0  #Use this offset with rogue VCO

        # Turn off intensity noise line injection
        #VB20240618 likely not needed, as injection should not be getting turned on anymore (see FINALIZE_LOW_NOISE), but leaving in to be sure
        ezca['PSL-ISS_TRANSFER2_INJ_GAIN'] = 0  

        # Power control should not step REFL and DC gains here
        ezca['PSL-GUARD_STEP_REFL_GAIN'] = 0
        ezca['PSL-GUARD_STEP_DC_GAIN'] = 0

        # Set Fast Shutter Trigger back to 1V
        ezca['SYS-MOTION_C_SHUTTER_G_THRESHOLD'] = 1.0

        # restore exposure of AS camera
        ezca['CAM-AS_EXP'] = 20000 # 
        for optic in ['ETMX','ETMY','ITMX','ITMY']:
            ezca['CAM-'+optic+'_EXP'] = 30000

        matrix.lsc_input.TRAMP = 0
        matrix.lsc_input_arm.TRAMP = 0
        time.sleep(0.1)
        matrix.lsc_input.load()
        matrix.lsc_input_arm.load()

        # AE thinks BS oplev should be engaged asap
        ezca.switch('SUS-BS_M2_OLDAMP_P','OUTPUT','ON')

        ##################################################
        # IMC and PSL
        #ezca['PSL-ISS_SECONDLOOP_SERVO_ON'] = 0
        #ezca['PSL-ISS_SECONDLOOP_CLOSED'] = 0

        ##################################################
        # BECKHOFF RESET

        #ifoBurt('beckhoff_whitening_down.snap')     #ajm180830 commented out because doesn't work anymore
        #JCB 20180906
        ezca.burtwb('/opt/rtcds/userapps/release/isc/l1/burtfiles/down_state_snaps/l1edcu_ecatc1_plc2_beckhoff_whitening_down.snap')

        # ALS down settings
        ezca['ALS-C_COMM_PLL_COMP1'] = 0
        ezca['ALS-C_COMM_PLL_COMP2'] = 0
        ezca['ALS-C_COMM_VCO_TUNEOFS'] = 0
        ezca['ALS-C_DIFF_VCO_TUNEOFS'] = 0

        # Open Beam Diverters and shutters
        for bdiv in ['A','B','D']:
            ezca['SYS-MOTION_C_BDIV_' + bdiv + '_OPEN'] = 1

        # BECKHOFF RESET
        ##################################################

        ##################################################
        # CAL RESET

        #JCB 2019/01/15 updated for new calcs model
        # Calibration lines and subtraction initialisation values, subtraction by VB 20230412, JCB modified 20230419
        #Removing SUS L1 line and adding OSC1 line (which gets shipped to ETMX L1 AND ETMY L1 at half strength
        # and oppsoite sign. JCB 20230426
        #for stage in ['ETMY_L3','ETMX_L3','ETMX_L2','ETMX_L1']:
        #for stage in ['ETMY_L3','ETMX_L3','ETMX_L2']:
        #    ezca['SUS-'+stage+'_CAL_LINE_TRAMP'] = 10
        #    ezca['SUS-'+stage+'_CAL_LINE_CLKGAIN'] = 0
        ezca['CAL-CS_LINE_1_OSC_TRAMP'] = 15.0
        ezca['CAL-CS_LINE_1_OSC_CLKGAIN'] = 0
        #JCB 20240227 Turning all corner lines off
        ezca['CAL-CS_LINE_2_OSC_TRAMP'] = 15.0
        ezca['CAL-CS_LINE_2_OSC_CLKGAIN'] = 0
        ezca['CAL-CS_LINE_3_OSC_TRAMP'] = 15.0
        ezca['CAL-CS_LINE_3_OSC_CLKGAIN'] = 0
        for osc in ['1','2','3']:
            ezca['CAL-CS_TDEP_SUS_LINE' + osc + '_GRM_REAL_FORCED_VALUE'] = ifoconfig.sus_sub_init['line'+osc]['real']
            ezca['CAL-CS_TDEP_SUS_LINE' + osc + '_GRM_IMAG_FORCED_VALUE'] = ifoconfig.sus_sub_init['line'+osc]['imag']
            ezca['CAL-CS_TDEP_SUS_LINE' + osc +'_GRM_REAL_OVERRIDE'] = 1
            ezca['CAL-CS_TDEP_SUS_LINE' + osc +'_GRM_IMAG_OVERRIDE'] = 1
        #ezca['CAL-CS_TDEP_SUS_LINE1_SYNCED_PHASE'] = 0
        #ezca['OAF-CAL_DARM_FF_SUS_L3_PHASE'] = 176
        ezca['CAL-PCALX_OSC_SUM_ON'] = 0
        ezca['CAL-PCALY_OSC_SUM_ON'] = 0
        for osc in ['1','2','3','4','5','6','7','8','9']:
            ezca['CAL-PCALY_PCALOSC' + osc + '_OSC_SINGAIN'] = 0
            ezca['CAL-PCALX_PCALOSC' + osc + '_OSC_SINGAIN'] = 0
            ezca['CAL-CS_TDEP_PCAL_LINE' + osc + '_GRM_REAL_FORCED_VALUE'] = ifoconfig.pcal_sub_init['line'+osc]['real']
            ezca['CAL-CS_TDEP_PCAL_LINE' + osc + '_GRM_IMAG_FORCED_VALUE'] = ifoconfig.pcal_sub_init['line'+osc]['imag']
            ezca['CAL-CS_TDEP_PCAL_LINE' + osc + '_GRM_REAL_OVERRIDE'] = 1
            ezca['CAL-CS_TDEP_PCAL_LINE' + osc + '_GRM_IMAG_OVERRIDE'] = 1

        ezca['LSC-DARM_RSET'] = 2

        # Turn off OAF CAL DARM L1
        ezca.switch('OAF-CAL_SUM_DARM_L1', 'OUTPUT', 'OFF')

        #Rest CAL SUS OFFSETs and filters in L2 stage (SK), to reduce spectral leackage at low frequencies during lock acquisition
        ezca['CAL-CS_DARM_FE_ETMY_L2_LOCK_L_OFFSET'] = 0
        ezca.switch('CAL-CS_DARM_FE_ETMY_L2_LOCK_L','FM7','FM9','FM10','OFF')
        ezca['CAL-CS_DARM_FE_ETMY_L2_DRIVEALIGN_L2L_GAIN'] = 0

        #################################################
        # Recycling Mirror Settings 

        ezca['SUS-PRM_M1_DRIVEALIGN_L2L_GAIN'] = 1 # Added by AP 2016/09/14
        ezca['SUS-PR2_M1_DRIVEALIGN_L2L_GAIN'] = 1
        ezca['SUS-SR2_M1_DRIVEALIGN_L2L_GAIN'] = 1

        # Turn SR2 M2 angular switches back on
        for dof in ["P","Y"]:
            ezca.write("SUS-SR2_M2_LOCK_OUTSW_"+dof,1)

        # Turn off feedback to PCAL
        ezca['CAL-PCALX_DARM_GAIN'] = 0
        ezca['CAL-PCALY_DARM_GAIN'] = 0

        # turn off M1 of SR2. (Added by MN 2021/12/2. Should be sdf?)
        for dof in ['P','Y']:
            ezca['SUS-SR2_M1_LOCK_%s_GAIN'%dof] = -0.0001
            ezca.switch('SUS-SR2_M1_LOCK_%s'%dof,'INPUT','OFF')

        # turn off test offset
        for rm in ['PRM','SRM']:
            for dof in ['P','Y']:
                ezca.switch('SUS-{}_M1_TEST_{}'.format(rm,dof),'OFFSET','OFF')

        #################################################
        ### TMS settings

        #Clear histories
        for end in ['X','Y']:
            for dof in ['P','Y']:
                ezca['SUS-TMS{}_M1_LOCK_{}_RSET'.format(end,dof)] = 2

        #################################################
        # Set-up locking ESD(s)

        locking_esd_gains = {"ETMX":1.2,"ETMY":1.0}

        # All esds are non-locking, until ticked off from locking esds
        non_locking_esds = ["ETMX","ETMY"]

        # Set locking esds to HV
        for esd in ifoconfig.locking_esds:
            set_esd_state(esd,"HV")
            non_locking_esds.remove(esd)

        # Set non locking esds to LV
        for esd in non_locking_esds:
            set_esd_state(esd,"LV")

        etmx_l3_l2l = locking_esd_gains["ETMX"]*int("ETMX" in ifoconfig.locking_esds)
        etmy_l3_l2l = locking_esd_gains["ETMY"]*int("ETMY" in ifoconfig.locking_esds)

        if "ETMX" in ifoconfig.locking_esds and "ETMY" in ifoconfig.locking_esds:
            etmx_l3_l2l *= 0.5
            etmy_l3_l2l *= 0.5

        ###########################
        # Restore aquisition optic settings
        for optic in ['ETMX' , 'ETMY']:
            for quad in ['UL', 'UR', 'LL', 'LR']:
                ezca['SUS-' + optic + '_L3_ESDOUTF_' + quad + '_GAIN'] = 1
            time.sleep(1)

        ###########################
        # Turn on ASC outputs  Fix for workaround #CB20180928 above
        for chan in ['OUTPUT']:  
            for rot in ['P','Y']:
                for mode in ['D','C']:
                    for stiffness in ['HARD','SOFT']:
                        ezca.switch('ASC-' + mode + stiffness + '_' + rot, chan, 'ON')
                for dof in ['INP2','PRC2','SRC1','SRC2','MICH']:
                    ezca.switch('ASC-' + dof + '_' + rot, chan, 'ON')


        ######################################################
        # Turn off DAC dithers
        for quad in ["ITMX","ITMY","ETMX","ETMY","BS"]:
            ezca.write("SUS-{}_LKIN_P_OSC_CLKGAIN".format(quad),0)
        for quad in ["ETMX","ETMY"]:
            ezca.write("SUS-{}_LKIN_Y_OSC_CLKGAIN".format(quad),0)

        ######################################################
       
        ######################################################
        # ETMX Settings 

        #Restore L1 DriveAlign (clear histories first)
        val = {"RSET":2,"GAIN":1}
        for chan in ["RSET","GAIN"]:
            for cross in ['L2L','P2P','Y2Y','L2P','L2Y']:
                ezca['SUS-ETMX_L1_DRIVEALIGN_'+cross+'_'+chan] = val[chan]
            time.sleep(1)

        # Turn off L2 violin bandpass
        ezca.switch('SUS-ETMX_L2_DRIVEALIGN_L2L','FM4','OFF')

#        # restore driver state HV (or not, by setting hv_state)
#        for quad in ['UL','UR','LL','LR']:
#            ezca['SUS-ETMX_BIO_L3_' + quad + '_STATEREQ'] = etmx_statereq
#            ezca['SUS-ETMX_BIO_L3_' + quad + '_HVDISCONNECT_SW'] = etmx_hvstate
#            ezca['SUS-ETMX_BIO_L3_' + quad + '_VOLTAGE_SW'] = etmx_hvstate
#
#        # Linearisation
#        for osem in ['UL','UR','LL','LR']:
#            ezca['SUS-ETMX_L3_ESDOUTF_' + osem +'_LIN_BYPASS_SW'] = etmx_lin_bypass

        # Turn bias on
        ezca['SUS-ETMX_L3_LOCK_BIAS_TRAMP'] = 10
        time.sleep(0.1)
        ezca.switch('SUS-ETMX_L3_LOCK_BIAS', 'OFFSET', 'ON')
        ezca['SUS-ETMX_L3_LOCK_BIAS_OFFSET'] = ifoconfig.esdx_sign_flip*349
        ezca['SUS-ETMX_L3_LOCK_BIAS_GAIN'] = 320

        # Ramp the gain
        ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_GAIN'] = etmx_l3_l2l  
        ezca['SUS-ETMX_L2_DRIVEALIGN_L2L_RSET'] = 2

        ######################################################
        # ETMY Settings

        #Restore L1 DriveAlign (clear histories first)
        val = {"RSET":2,"GAIN":1}
        for chan in ["RSET","GAIN"]:
            for cross in ['L2L','P2P','Y2Y','L2P']:
                ezca['SUS-ETMY_L1_DRIVEALIGN_'+cross+'_'+chan] = val[chan]
            time.sleep(1)

        # Turn off L2 violin bandpass
        ezca.switch('SUS-ETMY_L2_DRIVEALIGN_L2L','FM4','OFF')

#        # restore driver voltage range
#        for quad in ['UL','UR','LL','LR']:
#            ezca['SUS-ETMY_BIO_L3_' + quad + '_STATEREQ'] = etmy_statereq
#            ezca['SUS-ETMY_BIO_L3_' + quad + '_HVDISCONNECT_SW'] = etmy_hvstate
#            ezca['SUS-ETMY_BIO_L3_' + quad + '_VOLTAGE_SW'] = etmy_hvstate
#
#        # Linearisation
#        for osem in ['UL','UR','LL','LR']:
#            ezca['SUS-ETMY_L3_ESDOUTF_' + osem +'_LIN_BYPASS_SW'] = etmy_lin_bypass

        # Turn bias on
        ezca['SUS-ETMY_L3_LOCK_BIAS_TRAMP'] = 10
        time.sleep(0.1)
        ezca.switch('SUS-ETMY_L3_LOCK_BIAS','OFFSET','ON')
        #Reversed sign in DOWN state to reduce charging while down, full bias here
        #Set to correct bias in LOCK_ALS_ARMS
        if "ETMY" not in ifoconfig.locking_esds:
            ezca['SUS-ETMY_L3_LOCK_BIAS_OFFSET'] = -ifoconfig.esdy_sign_flip * 100 #399 JCB 181015 #400 CA 181012

        # Check Bias Gain is correct 
        ezca['SUS-ETMY_L3_LOCK_BIAS_GAIN'] = 320 
        ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN'] = etmy_l3_l2l
        ezca['SUS-ETMY_L3_LOCK_L_GAIN'] = 1

        ##################################################
        # ITMX Settings

        ezca['SUS-ITMX_L1_DRIVEALIGN_P2P_GAIN'] = 1
        ezca['SUS-ITMX_L1_DRIVEALIGN_Y2Y_GAIN'] = 1

        # Make sure ITM linearization is on
        ezca['SUS-ITMX_L3_ESDOUTF_LIN_BYPASS_SW'] = 'OFF'
        ezca.switch('SUS-ITMX_L3_LOCK_BIAS', 'OFFSET', 'OFF')
#
#        # Set up ITMX ESD
#        if ezca['SUS-ITMX_L3_LOCK_BIAS_OUTPUT'] > -100:
#            # Turn bias on
#            ezca.switch('SUS-ITMX_L3_LOCK_BIAS', 'OFFSET', 'ON')
#            ezca['SUS-ITMX_L3_LOCK_BIAS_OFFSET'] = -100
#
#        if ezca['SUS-ITMX_L3_ESDAMON_DC_MON'] < 1000 and ezca['SUS-ITMX_L3_ESDAMON_DC_MON'] > -1000:
#            ezca['SUS-ITMX_ESD_STARTSTOP'] = 1
#        
#        ezca['SUS-ITMX_L3_DRIVEALIGN_L2L_TRAMP'] = 0
#        ezca['SUS-ITMX_L3_DRIVEALIGN_L2L_GAIN'] = 0
#
        ##################################################
        # ITMY Settings

        ezca['SUS-ITMY_L1_DRIVEALIGN_P2P_GAIN'] = 1
        ezca['SUS-ITMY_L1_DRIVEALIGN_Y2Y_GAIN'] = 1
        
        #################################################
        # Set TM PUMs and triples to high range state

        for optic in ['PRM', 'PR2', 'SRM', 'SR2']:
            for osem in ['UL', 'UR', 'LL', 'LR']:
                ezca['SUS-' + optic + '_BIO_M3_' + osem + '_STATEREQ'] = 2

        #for optic in ['ITMX', 'ITMY', 'ETMX', 'ETMY']:
        #    for osem in ['UL', 'UR', 'LL', 'LR']:
        #        ezca['SUS-' + optic + '_BIO_L2_' + osem + '_STATEREQ'] = 2
        #JCB 2022-05-12, state definitions are in susconst.py in /opt/rtcds/userapps/release/sus/l1/guardian/
        nodes_optics['SUS_ETMX'] = 'COILDRIVER_LOCK_ACQ'
        nodes_optics['SUS_ETMY'] = 'COILDRIVER_LOCK_ACQ'
        nodes_optics['SUS_ITMX'] = 'COILDRIVER_LOCK_ACQ'
        nodes_optics['SUS_ITMY'] = 'COILDRIVER_LOCK_ACQ'
        nodes_optics['SUS_BS'] = 'COILDRIVER_LOCK_ACQ'

       ##################################################
        # BOUNCE ROLL # Removed - S. Aston 01/08/2019

        #for optic in ['ETMY', 'ETMX', 'ITMX', 'ITMY']:
        #    for dof in ['BOUNCE', 'ROLL']:                
        #        ezca['OAF-' + dof + '_' + optic + '_MTRX_1_1'] = 1
        #        ezca['OAF-' + dof + '_' + optic + '_MTRX_1_2'] = 0
        #        ezca.switch('OAF-' + dof + '_' + optic, 'FM3', 'FM4', 'ON', 'FM8', 'FM9', 'OFF')

        # Ensure SUS TEST bank offsets are turned off
        for optic in ['ETMY', 'ETMX', 'ITMX', 'ITMY']:
            for dof in ['P', 'Y']:
                ezca.switch('SUS-' + optic + '_M0_TEST_' + dof,'OFFSET', 'OFF')

        ##################################################
        # Parametric Instabiity 
        # Turn Parametric Instability Master out switches off 
        for optic in ['ETMY', 'ETMX', 'ITMX', 'ITMY']:
                ezca['SUS-' + optic + '_PI_ESD_DRIVER_PI_DAMP_SWITCH']=0

        ##################################################

        ezca['LSC-CONTROL_ENABLE'] = 'ON'
        ezca['ASC-WFS_SWTCH'] = 'ON'

        # ASC: Set the TR QPD whitening gain back to 9 dB
        for end in ['X','Y']:
            for qpd in ['A','B']:
                 ezca['ASC-' + end + '_TR_' + qpd + '_WHITEN_GAIN'] = 15    #9

        # Set the POPAIR_RF18 whitening gain back to 21dB (ajm180223)
        ezca['LSC-POPAIR_B_RF18_WHITEN_GAINSTEP'] = 7

        # Turn of Common Mode Servo IN2
        ezca['LSC-REFL_SERVO_IN2GAIN'] = -32
        ezca['LSC-REFL_SERVO_IN2EN'] = 0

        ###JCB TEMPORARY TURN OFF OF HIGH FREQUENCY PCALX LINES  #FOR ER10 only
        #ezca['CAL-PCALX_OSC_SUM_ON'] = 0
        #ezca['CAL-PCALX_OSC_SUM_MATRIX_1_1'] = 1
        #ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] = 0
        #ezca['CAL-PCALX_PCALOSC1_OSC_TRAMP'] = 10
        #ezca['CAL-PCALX_PCALOSC1_OSC_SINGAIN'] = 0

        ###SMA RESET THOSE VIOLIN DAMPING MONITOR FILTERS WHICH TEND TO INTEGRATE TO INFINITY
        # for optic in ['ETMY', 'ETMX', 'ITMX', 'ITMY']:
        #     for mode in ['1','2','3','4','5','6','7','8','9','10']:
        #         ezca['SUS-' + optic + '_L2_DAMP_MODE' + mode + '_BL_RSET'] = 2
        #         ezca['SUS-' + optic + '_L2_DAMP_MODE' + mode + '_RMSLP_RSET'] = 2

        ### JCB CHECK TO SEE IF PCALX/Y OPTICAL FOLLOWER IS OSCILLATING, IF SO RESET
        if abs(ezca['CAL-PCALX_OPTICALFOLLOWERSERVOOSCILLATION']) > 0.1:
            ezca['CAL-PCALX_DARM_RSET'] = 2
            time.sleep(0.1)
            ezca['CAL-PCALX_OPTICALFOLLOWERSERVOENABLE'] = 0
            time.sleep(0.5)
            ezca['CAL-PCALX_OPTICALFOLLOWERSERVOENABLE'] = 1

        if abs(ezca['CAL-PCALY_OPTICALFOLLOWERSERVOOSCILLATION']) > 0.1:
            ezca['CAL-PCALY_DARM_RSET'] = 2
            time.sleep(0.1)
            ezca['CAL-PCALY_OPTICALFOLLOWERSERVOENABLE'] = 0
            time.sleep(0.5)
            ezca['CAL-PCALY_OPTICALFOLLOWERSERVOENABLE'] = 1
        
        ### VB SHUTTER BAD PCAL BASED ON ifoconfig, preventing noise injection.
        if ifoconfig.use_pcaly == 1:
            ezca['CAL-PCALY_SHUTTERPOWERENABLE'] = 1
        else:
            ezca['CAL-PCALY_SHUTTERPOWERENABLE'] = 0
        if ifoconfig.pcalx_good == 1:
            ezca['CAL-PCALX_SHUTTERPOWERENABLE'] = 1
        else:
            ezca['CAL-PCALX_SHUTTERPOWERENABLE'] = 0
        
        ### AE added to make DARM blrms ok for later
        ezca.switch('OAF-RANGE_RHP','OUTPUT','OFF')
        for flt in ['RBP','RLP']:
            for nnbr in ['1','2','3','4','8']:
                ezca['OAF-RANGE_' + flt + '_' + nnbr + '_RSET'] = 2
                
        # Turn down ring heaters Trial 19 Feb 2024 CB
        if ifoconfig.run_tcs:
            nodes_loose['TCS_RH'].set_request('RH_DEFAULT')
        # Turn CO2 lasers to 0
        nodes_loose['TCS_ITMX_CO2'].set_request('DOWN_BASIC')
        nodes_loose['TCS_ITMY_CO2'].set_request('DOWN_BASIC')
        
        #Keep Fast Shutter Closed Until DRMI
        #ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 0
        ezca['SYS-MOTION_C_FASTSHUTTER_A_BLOCK'] = 1

        #JCB AS port protection reset
        nodes['FAST_SHUTTER'] = 'DOWN'
        ezca['SYS-PROTECTION_AS_XARM'] = 0
        ezca['SYS-PROTECTION_AS_YARM'] = 0

        # put baffle PDs at alignment gains
        for tm in ['ETMX','ETMY','ITMX','ITMY']:
            for nr in ['1','4']:
                ezca['AOS-' + tm + '_BAFFLEPD_' + nr + '_GAINSETTING'] = 6

        # Set quad a2l gains
        for optic in ['ETMX','ETMY','ITMX','ITMY']:
            for dof in ['P','Y']:
                ezca['SUS-{}_L2_DRIVEALIGN_{}2L_GAIN'.format(optic,dof)] = ifoconfig.l2_a2l_gains[optic][dof]

        # Load LSC DARM out matrices
        matrix.lsc_l3_lock.load()
        matrix.lsc_l2_lock.load()
        matrix.lsc_l1_lock.load()
        matrix.lsc_m0_lock.load()

        #Set optic nodes to aligned to be ready to go back to COILDRIVER_LOCK_ACQ state to reset them again.
        nodes_optics['SUS_ETMX'] = 'ALIGNED'
        nodes_optics['SUS_ETMY'] = 'ALIGNED'
        nodes_optics['SUS_ITMX'] = 'ALIGNED'
        nodes_optics['SUS_ITMY'] = 'ALIGNED'
        nodes_optics['SUS_BS'] = 'ALIGNED'

        # AE 220520 changed to turn it on here so it can be off in the safe asc file to prevent race conditions
        ezca['ASC-WFS_SWTCH'] = 'ON'
        ezca['ASC-WFS_GAIN'] = 1

        # moved here by AE 220517, should not enable drives until everything is cleared properly
        ezca['LSC-IFO_TRIG_THRESH_ON']  = -1000000 # forcing IFO trigger to allow signals to pass, for acquisition.
        ezca['LSC-IFO_TRIG_THRESH_OFF'] = -1000000 # Jenne 2018Nov13

        # CLEAR FILTER HISTORIES
        clear_filters_down()

        #JCB set TCS CO2 masks to default 20240205
        tcs_flipperscript.setTCSMask('ITMX',ifoconfig.default_itmx_tcs_mask)  
        tcs_flipperscript.setTCSMask('ITMY',ifoconfig.default_itmy_tcs_mask)  #editted and commented out as we ended up with one mask in and one out CB 20240319  TODO: should be put int he CO2 guadians
        
        #VB20240424 Reset violin guardians to a nominal DOWN state
        nodes_loose['SUS_1STVIOLIN'] = 'IDLE'
        #nodes_loose['SUS_2NDVIOLIN'] = 'IDLE'
        ezca['GRD-SUS_2NDVIOLIN_REQUEST'] = 'IDLE' # push this to IDLE despite not being the the nodes list
        
        log("down main complete")

        # EQ chans:
        self.eq_chans = []
        for arm in ['X','Y']:
            for tm in ['ITM','ETM']:
                self.eq_chans.append('ISI-GND_STS_{0}{1}_{1}_BLRMS_30M_100M'.format(tm,arm))

        self.wind_chans = []
        for station in ['EX','EY','LVEA']:
            self.wind_chans.append('PEM-{}_WIND_WEATHER_MPH'.format(station))

        self.wind_speeds = EzAvg(ezca,60,self.wind_chans)

    @nodes.checker()
    def run(self):
        for node in nodes.get_stalled_nodes():
            log("reviving node: %s" % node.name)
            node.revive()
        if not nodes.arrived:
            for node in nodes:
                if not node.arrived:
                    notify("waiting for node['{}'] == {}...".format(node.name, node.REQUEST))
            return False

        if any(ezca[chan] > 400 for chan in self.eq_chans):
            #[done,vals] = self.wind_speeds.ezAvg()
            #if done:
            #    if any(val>7 for val in vals):
            #        notify('Motion from wind too high for locking')
            #        ezca['ODC-OBSERVATORY_MODE'] = 31
            #        return
            notify('Motion from EQ too high for locking')
            #ezca['ODC-OBSERVATORY_MODE'] = 34
            return

        if not (ezca['SYS-MOTION_C_FASTSHUTTER_A_READY'] == 1):
            notify('Fast Shutter is not ready, cannot proceed')
            return False

        if ezca['GRD-LOCKLOSS_SHUTTER_CHECK_STATE_N'] == 30:
            notify('Check that fast shutter triggered on last lockloss')
            log('Fast shutter check needed! See OPS wiki')
            return

        if ezca['GRD-LOCKLOSS_SHUTTER_CHECK_STATE_N'] == 40:
            notify('Power did not reach threshold to trigger Fast Shutter')
            log('Recommend check of trigger PD signals. See OPS wiki')
            return

        if MC_in_fault():
            return 'MC_FAULT'

        # FIXME: Don't think this is necessary as we wait on IMC_LOCK node 
        #if not MC_locked_confirm():
        #    return

        return True

##################################################
class INITIAL_ALIGNMENT(GuardState):
    index = 9
    request = True

    def main(self):
        # reset LSC trigger for relocking stage AE 20200303
        ezca['LSC-IFO_TRIG_THRESH_ON'] = -1e6
        ezca['LSC-IFO_TRIG_THRESH_OFF'] = -1e6

        set_bafflePd_dark_offsets()

        self.states = ['ALIGNMENT_COMPLETE']

        self.ii = 0
        self.set_state = True
        self.timer['wait'] = 0

    def run(self):

        if not self.timer['wait']:
            return

        if self.set_state:
            nodes_loose['INITIAL_ALIGNMENT'] = self.states[self.ii]
            self.ii+=1
            self.set_state = False
            self.timer['wait'] = 2
            return

        #if not nodes_loose['INITIAL_ALIGNMENT'].complete:
        if not (nodes_loose['INITIAL_ALIGNMENT'].arrived and nodes_loose['INITIAL_ALIGNMENT'].done):
            notify('Initial Alignment is running')
            return

        if self.ii < len(self.states):
            self.set_state = True
            return

        notify('Initial Alignment Complete!!!')

        return True


##################################################

class CORNER_INITIAL_ALIGNMENT(GuardState):
    index = 8
    request = True

    def main(self):
        # reset LSC trigger for relocking stage AE 20200303
        ezca['LSC-IFO_TRIG_THRESH_ON'] = -1e6
        ezca['LSC-IFO_TRIG_THRESH_OFF'] = -1e6

        set_bafflePd_dark_offsets()

        self.states = ['SKIP_ARMS','ALIGNMENT_COMPLETE']

        self.ii = 0
        self.set_state = True
        self.timer['wait'] = 0

    def run(self):

        if not self.timer['wait']:
            return

        if self.set_state:
            nodes_loose['INITIAL_ALIGNMENT'] = self.states[self.ii]
            self.ii+=1
            self.set_state = False
            self.timer['wait'] = 2
            return

        if not (nodes_loose['INITIAL_ALIGNMENT'].arrived and nodes_loose['INITIAL_ALIGNMENT'].done):
            notify('Initial alignment guardian is running')
            return

        if self.ii < len(self.states):
            self.set_state = True
            return

        return True

##################################################
class CHECK_FAST_SHUTTER_AND_WATCHDOGS(GuardState):
    index = 11
    request = False

    def main(self):
        # reset LSC trigger for DRMI and carm offset reduction stage AE 20200303
        ezca['LSC-IFO_TRIG_THRESH_ON'] = -1e6
        ezca['LSC-IFO_TRIG_THRESH_OFF'] = -1e6
#        ezca['ODC-OBSERVATORY_MODE'] = 21

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        tripped = False
        for sus in ifoconfig.suspensions:
            if 'TRIPPED' in ezca['GRD-' + sus + '_STATE_S']:
                notify(sus + ' is still tripped, please untrip to proceed')
                tripped = True
        for sei in ifoconfig.seismics:
            if 'WATCHDOG' in ezca['GRD-' + sei + '_STATE_S']:
                notify(sei + ' is still tripped, please untrip to proceed')
                tripped = True
        if tripped:
            return False

        if ezca['SYS-PROTECTION_AS_TESTNEEDED']:
            return 'MISALIGN_MIRRORS_AS_TEST'
        else:
            return True


##################################################
class MISALIGN_MIRRORS_AS_TEST(GuardState):
    index = 12
    request = False
    redirect = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        # Misalign the DRMI:
        #ezca['SUS-PRM_M1_OPTICALIGN_P_TRAMP'] = 5
        #ezca['SUS-SRM_M1_OPTICALIGN_P_TRAMP'] = 5
        #ezca['SUS-ITMY_M0_OPTICALIGN_Y_TRAMP'] = 5
        #time.sleep(0.2)
        #ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'] =  200
        #ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET'] = 600
        #ezca['SUS-ITMY_M0_OPTICALIGN_Y_OFFSET'] = ezca['SUS-ITMY_M0_OPTICALIGN_Y_OFFSET'] - 50

        nodes_loose['ALS_XARM'] = "QPDS_LOCKED"
        nodes_loose['ALS_YARM'] = "QPDS_LOCKED"

        # Misalign with test banks
        prm.misalign('P', -1500, ezca)
        srm.misalign('P', +1500, ezca)
        itmy.misalign('Y', -50, ezca)

        self.timer['misalign_time'] = 5

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if self.timer['misalign_time']:
            return 'AS_TEST_4W'

##################################################
class AS_TEST_4W(GuardState):
    index = 13
    request = False
    redirect = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed() #newnodes
        time.sleep(1)
        nodes['IMC_LOCK'].set_request('LOCKED_4W')

    @assert_mc_lock
    def run(self):
        if not (nodes['IMC_LOCK'].arrived and nodes['IMC_LOCK'].done):
            log('Still powering up')
            return
        return 'TEST_FAST_SHUTTER'

##################################################
class TEST_FAST_SHUTTER(GuardState):
    index = 14
    request = False
    redirect = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed() #newnodes
        nodes['FAST_SHUTTER'] = 'READY'
 
    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if nodes['FAST_SHUTTER'] == 'SHUTTER_FAILURE':
            notify('Shutter Failed! Stop!')
        elif nodes['FAST_SHUTTER'] == 'READY':
            return 'AS_TEST_RESET'
            
##################################################
class AS_TEST_RESET(GuardState):
    index = 15
    request = False
    redirect = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed() #newnodes
        nodes['IMC_LOCK'] = 'LOCKED_1W'
        ezca['PSL-GUARD_POWER_REQUEST'] = 1
        nodes['FAST_SHUTTER'] = 'DOWN'

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if not (nodes['IMC_LOCK'].arrived and nodes['IMC_LOCK'].done):
            log('Still powering up')
            return
        return 'ALIGN_MIRRORS_AS_TEST'


##################################################
class ALIGN_MIRRORS_AS_TEST(GuardState):
    index = 16
    request = False
    redirect = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        #ezca['SUS-PRM_M1_OPTICALIGN_P_TRAMP'] = 10
        #ezca['SUS-SRM_M1_OPTICALIGN_P_TRAMP'] = 10
        #ezca['SUS-ITMY_M0_OPTICALIGN_Y_TRAMP'] = 10
        #time.sleep(0.2)
        #self.restore_srm_script_process = check_output([process_list['align_restore'].file, 'SRM'])
        #self.restore_prm_script_process = check_output([process_list['align_restore'].file, 'PRM'])
        #ezca['SUS-ITMY_M0_OPTICALIGN_Y_OFFSET'] = ezca['SUS-ITMY_M0_OPTICALIGN_Y_OFFSET'] + 50

        for dof in ['P', 'Y']:
            prm.align(dof, ezca)
            srm.align(dof, ezca)
            itmy.align(dof, ezca)

        self.timer['optic align wait'] = 10

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if self.timer['optic align wait']:
            return 'CHECK_FAST_SHUTTER_AND_WATCHDOGS'


##################################################
#class RELOCK_MC(GuardState):
#    index = 8
#    request = False
#
#    def main(self):
#        if MC_in_fault():
#            return 'MC_FAULT'
#        
#        # The mode cleaner has unlocked during the initial comm/diff steps, we turn off the
#        # output from XARM and reset the MCL gain.   
#
#        ezca.switch('LSC-XARM','OUTPUT','OFF')
#        ezca['LSC-XARM_RSET'] = 2
#        # TODO: once the IMC Guardian is fixed to tdo this, can remove the next line
#        ezca['IMC-MCL_GAIN'] = -125
#        ezca['IMC-VCO_TUNEOFS'] = 0
#
#    def run(self):
#        if MC_locked_confirm():
#            return True


##################################################
cam_dof_number = {'X':{'I':11,'E':13},
                  'Y':{'I':12,'E':14}}

sus_row_number = {'X':{'ITM':7,'ETM':9},
                  'Y':{'ITM':8,'ETM':10}}

drive_mtrx = {'PIT':{'ITM':{'I':-1.07,'E':-1.0},
                     'ETM':{'I':-1.0,'E':-0.78}},

              'YAW':{'ITM':{'I':-1.07,'E':1.0},
                     'ETM':{'I':1.0,'E':-0.78}}
                    }
'''
class LOCK_ALS_XARM(GuardState):
    index = 20
    request = False

    @assert_mc_lock
    def main(self):

        #nodes_loose['SUS_1STVIOLIN'] = 'IDLE'

        # Set ETMY ESD Bias to correct value for lock acquisition
        ezca['SUS-ETMY_L3_LOCK_BIAS_TRAMP'] = 10

        if "ETMY" in ifoconfig.locking_esds:
            ezca['SUS-ETMY_L3_LOCK_BIAS_OFFSET'] = ifoconfig.esdy_sign_flip * 300.0
        else:
            ezca['SUS-ETMY_L3_LOCK_BIAS_OFFSET'] = ifoconfig.esdy_sign_flip * 200.0

        # Turn off ETMX ESD bias - S. Aston 09/30/2015
        # ezca['SUS-ETMX_L3_LOCK_BIAS_TRAMP'] = 10
        # ezca.switch('SUS-ETMX_L3_LOCK_BIAS','OFFSET','OFF')

        for cam in ['11','12','13','14']:
            matrix.asc_ads_output_pit.zero(col='PIT{}'.format(cam))
            matrix.asc_ads_output_yaw.zero(col='YAW{}'.format(cam))
        time.sleep(0.3)

        feedback_cam = 'ETM'
        if feedback_cam == 'ETM':
            # Set ADS output matrix elements for CAM to ETM servos 
            matrix.asc_ads_output_pit['ETMX','PIT13'] = 0.3
            matrix.asc_ads_output_pit['ETMY','PIT14'] = 0.3
            matrix.asc_ads_output_yaw['ETMX','YAW13'] = 0.3
            matrix.asc_ads_output_yaw['ETMY','YAW14'] = 0.3

        if feedback_cam == 'ITM':
            # Set ADS output matrix elements for CAM to ITM servos
            matrix.asc_ads_output_pit['ETMX','PIT11'] = -0.3
            matrix.asc_ads_output_pit['ETMY','PIT12'] = -0.3
            matrix.asc_ads_output_yaw['ETMX','YAW11'] = 0.3
            matrix.asc_ads_output_yaw['ETMY','YAW12'] = 0.3

        if feedback_cam == 'BOTH':
            # Written as the matrix shows, row is actuator, col is camera
            for arm in ['X','Y']:
                for act in ['ITM','ETM']:
                    for cam in ['I','E']:
                        for dof in ['PIT','YAW']:
                            ezca['ASC-ADS_OUT_{0}_MTRX_{1}_{2}'.format(dof,sus_row_number[arm][act],cam_dof_number[arm][cam])] = drive_mtrx[dof][act][cam]

        #if feedback_cam == 'BOTH':
        #    # Written as the matrix shows, row is actuator, col is camera
        #    for arm in ['X','Y']:
        #        for act in ['ITM','ETM']:
        #            for cam in ['I','E']:
        #                matrix.asc_ads_output_pit[act+arm,'PIT{}'.format(cam_dof_num[arm][cam])] = drive_mtrx['PIT'][act][cam]
        #                matrix.asc_ads_output_yaw[act+arm,'YAW{}'.format(cam_dof_num[arm][cam])] = drive_mtrx['YAW'][act][cam]


        nodes_loose['ALS_XARM'] = 'PDH_LOCKED'

        if ifoconfig.align_grn:
            self.final_state = 'ALIGN_ARM'
        else:
            self.final_state = 'PDH_LOCKED'

        self.alsx_ctrl = EzAvg(ezca,1,'ALS-X_REFL_SERVO_FASTMON')

        if ifoconfig.align_grn:
            self.go_to_align = True

        #TODO: Lock both arms here

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not nodes_loose['ALS_XARM'].arrived:
            log('Waiting for ALS X guardian')
            return

        [done,val] = self.alsx_ctrl.ezAvg()
        if not done:
            return
        elif val > 3:
            log('Ctrl signal too big')
            nodes_loose['ALS_XARM'] = 'PREP_PDH_LOCK'
            time.sleep(1)
            nodes_loose['ALS_XARM'] = self.final_state
            time.sleep(2)
            return

        if self.go_to_align:
            nodes_loose['ALS_XARM'] = self.final_state
            self.go_to_align = False
            return


        if ezca['ALS-C_TRX_A_LF_OUTPUT'] < 1200:
            notify("ALS X transmission too low")
            return

        #if ezca['ALS-C_DIFF_A_DEMOD_RFMON'] < -27: #Temp workaround for EPICS channels freezing
        #    notify("Check that ALS not locked on higher order modes")
        #    return False

        #if ezca['ALS-C_COMM_A_DEMOD_RFMON'] < -1: #Temp workaround for EPICS channels freezing
        #    notify("Check that ALS not locked on higher order modes")
        #    return False

        # check that we are DONE in the requested states, since the
        # PDH_LOCKED states have stuff in main that needs to be
        # completed first.
        #return True

        return True
'''

##################################################

class LOCK_ALS_ARMS(GuardState):
    index = 21
    request = False

    @assert_mc_lock
    def main(self):
        # Set ETMY ESD Bias to correct value for lock acquisition
        ezca['SUS-ETMY_L3_LOCK_BIAS_TRAMP'] = 10

        if "ETMY" in ifoconfig.locking_esds:
            ezca['SUS-ETMY_L3_LOCK_BIAS_OFFSET'] = ifoconfig.esdy_sign_flip * 300.0
        else:
            ezca['SUS-ETMY_L3_LOCK_BIAS_OFFSET'] = ifoconfig.esdy_sign_flip * 200.0

        # Turn off ETMX ESD bias - S. Aston 09/30/2015
        # ezca['SUS-ETMX_L3_LOCK_BIAS_TRAMP'] = 10
        # ezca.switch('SUS-ETMX_L3_LOCK_BIAS','OFFSET','OFF')

        for cam in ['11','12','13','14']:
            matrix.asc_ads_output_pit.zero(col='PIT{}'.format(cam))
            matrix.asc_ads_output_yaw.zero(col='YAW{}'.format(cam))
        time.sleep(0.3)

        # Set this to both, if you want itm and etm spots centered, otherwise just set to ETM
        feedback_cam = 'ETM'

        if feedback_cam == 'ETM':
            # Set ADS output matrix elements for CAM to ETM servos 
            matrix.asc_ads_output_pit['ETMX','PIT13'] = 0.3
            matrix.asc_ads_output_pit['ETMY','PIT14'] = 0.3
            matrix.asc_ads_output_yaw['ETMX','YAW13'] = 0.3
            matrix.asc_ads_output_yaw['ETMY','YAW14'] = 0.3

        if feedback_cam == 'ITM':
            # Set ADS output matrix elements for CAM to ITM servos
            matrix.asc_ads_output_pit['ETMX','PIT11'] = -0.3
            matrix.asc_ads_output_pit['ETMY','PIT12'] = -0.3
            matrix.asc_ads_output_yaw['ETMX','YAW11'] = 0.3
            matrix.asc_ads_output_yaw['ETMY','YAW12'] = 0.3

        if feedback_cam == 'BOTH':
            # Written as the matrix shows, row is actuator, col is camera
            for arm in ['X','Y']:
                for act in ['ITM','ETM']:
                    for cam in ['I','E']:
                        for dof in ['PIT','YAW']:
                            ezca['ASC-ADS_OUT_{0}_MTRX_{1}_{2}'.format(dof,sus_row_number[arm][act],cam_dof_number[arm][cam])] = drive_mtrx[dof][act][cam]

        #if feedback_cam == 'BOTH':
        #    # Written as the matrix shows, row is actuator, col is camera
        #    for arm in ['X','Y']:
        #        for act in ['ITM','ETM']:
        #            for cam in ['I','E']:
        #                matrix.asc_ads_output_pit[act+arm,'PIT{}'.format(cam_dof_num[arm][cam])] = drive_mtrx['PIT'][act][cam]
        #                matrix.asc_ads_output_yaw[act+arm,'YAW{}'.format(cam_dof_num[arm][cam])] = drive_mtrx['YAW'][act][cam]


        nodes_loose['ALS_XARM'] = 'PDH_LOCKED'
        nodes_loose['ALS_YARM'] = 'PDH_LOCKED'

        if ifoconfig.align_grn:
            self.final_state = 'ALIGN_ARM'
        else:
            self.final_state = 'PDH_LOCKED'

        chans = ['ALS-X_REFL_SERVO_FASTMON', 'ALS-Y_REFL_SERVO_FASTMON']
        self.als_ctrl = EzAvg(ezca,1,chans)

        if ifoconfig.align_grn:
            self.go_to_align = True

        self.check_xctrl = True
        self.check_yctrl = True

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not (nodes_loose['ALS_XARM'].arrived and nodes_loose['ALS_YARM'].arrived):
            log('Waiting for ALS guardians')
            return

        if self.check_xctrl:
            xctrl = ezavg(ezca,1.0,'ALS-X_REFL_SERVO_FASTMON')
            if abs(xctrl) > 3:
                nodes_loose['ALS_XARM'] = 'PREP_PDH_LOCK'
                time.sleep(1)
                nodes_loose['ALS_XARM'] = 'PDH_LOCKED'
                return
            else:
                self.check_xctrl = False

        if self.check_yctrl:
            yctrl = ezavg(ezca,1.0,'ALS-Y_REFL_SERVO_FASTMON')
            if abs(yctrl) > 3:
                nodes_loose['ALS_YARM'] = 'PREP_PDH_LOCK'
                time.sleep(1)
                nodes_loose['ALS_YARM'] = 'PDH_LOCKED'
                return
            else:
                self.check_yctrl = False

        #---------------------------------------------------
        #[done,vals] = self.als_ctrl.ezAvg()
        #if not done:
        #    return

        #elif abs(vals[0]) > 3:
        #    log('X ctrl signal too big')
        #    nodes_loose['ALS_XARM'] = 'PREP_PDH_LOCK'
        #    time.sleep(1)
        #    nodes_loose['ALS_XARM'] = self.final_state
        #    time.sleep(2)
        #    return
        #elif abs(vals[1]) > 3:
        #    log('Y ctrl signal too big')
        #    nodes_loose['ALS_YARM'] = 'PREP_PDH_LOCK'
        #    time.sleep(1)
        #    nodes_loose['ALS_YARM'] = self.final_state
        #    time.sleep(2)
        #    return
        #---------------------------------------------------

        if self.go_to_align:
            nodes_loose['ALS_XARM'] = self.final_state
            nodes_loose['ALS_YARM'] = self.final_state
            self.go_to_align = False
            return


        if ezca['ALS-C_TRX_A_LF_OUTPUT'] < 1000:
            notify("ALS X transmission too low")
            return
        elif ezca['ALS-C_TRY_A_LF_OUTPUT'] < 1000:
            notify("ALS Y transmission too low")
            return


        #if ezca['ALS-C_TRX_A_LF_OUTPUT'] < 600:
        #    notify("ALS X transmission too low")
        #    return


        #if ezca['ALS-C_TRY_A_LF_OUTPUT'] < 450: # 150
        #    notify("ALS Y transmission too low. See alog 20154 for instructions")
        #    return False

        #if ezca['ALS-C_DIFF_A_DEMOD_RFMON'] < -27: #Temp workaround for EPICS channels freezing
        #    notify("Check that ALS not locked on higher order modes")
        #    return False

        #if ezca['ALS-C_COMM_A_DEMOD_RFMON'] < -1: #Temp workaround for EPICS channels freezing
        #    notify("Check that ALS not locked on higher order modes")
        #    return False

        # check that we are DONE in the requested states, since the
        # PDH_LOCKED states have stuff in main that needs to be
        # completed first.
        #return True

        return True




##################################################
# Feed back the ALS COMM signal to MCL
class START_ALS_COMM(GuardState):
    index = 22
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        ezca.LIGOFilter('LSC-XARM').all_off()
        ezca['LSC-XARM_TRAMP'] = 0
        ezca['LSC-XARM_GAIN'] = 0
        #ezca['LSC-XARM_GAIN'] = -3
        ezca['LSC-XARM_TRAMP'] = 1  #3  
        ezca['IMC-MCL_TRAMP'] = 3   #10

        ezca.switch('LSC-XARM', 'INPUT', 'FM4', 'FM5', 'OUTPUT', 'ON')
        #ezca.switch('LSC-XARM', 'INPUT', 'FM4', 'FM5', 'FM10', 'ON')

        # possibly request ALS_ARM::RELOCK_PDH to re-aquire the lock
        # if it's drifted

        ezca['ALS-C_COMM_PLL_GAIN'] = 0
        ezca['ALS-C_COMM_PLL_INEN'] = 1
        ezca['ALS-C_COMM_PLL_COMP1'] = 1
        time.sleep(1)

        self.timer['vco_tune'] = 30

        self.comm_ctrl = EzAvg(ezca,1.0,'ALS-C_COMM_PLL_CTRLMON')

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        [done,val] = self.comm_ctrl.ezAvg()
        if not done:
            return

        if not abs(val) < 4.0:
            return

        #if self.timer['vco_tune']:
        #    #Note: this will change the comm offset in IR resonance find
        #    log('Tuning COMM VCO offset...')
        #    ofs = ezca['ALS-C_COMM_VCO_TUNEOFS']
        #    ezca['ALS-C_COMM_VCO_TUNEOFS'] -= self.commctrl[0]
        #    time.sleep(0.1)

        #self.commctrl = cdsutils.avg(0.5, 'ALS-C_COMM_PLL_CTRLMON',stddev=True)
        #if not abs(self.commctrl[0]) < 5.0:
        #    return

        return True
###

##################################################
class ALS_COMM_UP(GuardState):
    index = 23
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        # Put the PLL gain back, turn the boost back on
        #ezca['ALS-C_COMM_PLL_GAIN'] = 0
        #time.sleep(0.5)
        #ezca['ALS-C_COMM_PLL_COMP1'] = 1

        #ezca.switch('LSC-XARM','OUTPUT','ON')

        # Transition IMC length to ALS COMM
        log('Cross-ramping gains...')
        ezca['LSC-XARM_GAIN'] = -3
        ezca['IMC-MCL_GAIN'] = 0

        #self.timer['wait'] = 10

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        #if not self.timer['wait']:
        #    return False

        # Turn on the boost
        log('Engaging boost and integrator in XARM')
        ezca.switch('LSC-XARM', 'FM10', 'ON')
        time.sleep(0.1)

        # Integrator + LP
        ezca.switch('LSC-XARM', 'FM6', 'FM3', 'ON')
        time.sleep(0.1)

        # Turn on PSL NPRO temp loop (refcav bypass AJM20190205)
        #ezca['PSL-FSS_TEMP_LOOP_ON_REQUEST'] = 1

        return True

##################################################
class ALS_COMM_LOCKED(GuardState):
    index = 30
    request = True

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        return True

##################################################
class RESET_ALS_DIFF(GuardState):
    index = 40
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        matrix.lsc_output_arm.zero(col='DARM')
        time.sleep(0.3)
        matrix.lsc_output_arm['TM_COMMON','DARM'] = 1
        #ezca['LSC-ARM_OUTPUT_MTRX_5_1'] = 1

        # Make sure that the correct L1 switch is on
        for actuator in ["ETMX","ETMY"]:
            ezca['SUS-'+actuator+'_L1_LOCK_OUTSW_L'] = int(actuator in ifoconfig.darm_actuators)

        # Turn off BS stage 2 - MAKE SURE!
        nodes_loose['SEI_BS'] = 'ISOLATED_DAMPED' # SMA added 12/23/2015

        # set the EPICs channels for the power up
#        ezca['PSL-GUARD_POWER_REQUEST'] = 4
#        ezca['PSL-GUARD_STEP_REFL_GAIN'] = 0
#        ezca['PSL-GUARD_STEP_DC_GAIN'] = 0
#        time.sleep(1)
#        nodes['IMC_LOCK'].set_request('IFO_POWER')

        nodes['IMC_LOCK'] = 'LOCKED_1W'

        ezca['LSC-DARM_TRAMP'] = 1
        time.sleep(0.2)
        ezca['LSC-DARM_GAIN'] = 0

        ezca.LIGOFilter('LSC-DARM').all_off()

        for actuator in ifoconfig.darm_actuators:
            for stage in ["M0","L1"]:
                ezca.write("SUS-{0}_{1}_LOCK_L_TRAMP".format(actuator,stage),10)
        time.sleep(0.2)
        for actuator in ifoconfig.darm_actuators:
            for stage in ["M0","L1"]:
                ezca.write("SUS-{0}_{1}_LOCK_L_GAIN".format(actuator,stage),0)

        self.timer['etmy control down'] = 10

        # Also turn off MICH,PRCL,SRCL feedback here
        ezca.switch('LSC-MICH', 'OUTPUT', 'OFF')
        ezca.switch('LSC-PRCL', 'OUTPUT', 'OFF')
        ezca.switch('LSC-SRCL', 'OUTPUT', 'OFF')

        # Turn off CPS freeze
        ezca['LSC-CPSFF_GAIN'] = 0

        # AJM20230308 temporary setting of L1 LOCK P/Y filters and gains
        for quad in ['ETMX','ETMY','ITMX','ITMY']:
            for dof in ['P','Y']:
                ezca.switch('SUS-{}_L1_LOCK_{}'.format(quad,dof),'FM2','LIMIT','OFF','FM7','FM8','ON')
                ezca.write('SUS-{}_L1_LOCK_{}_GAIN'.format(quad,dof),-1)

        # Turn off useism boost     #AJM201212 commented out. Try starting with it on.
        for actuator in ['ETMX','ETMY']:
            ezca.switch("SUS-{}_L1_LOCK_L".format(actuator),"FM2","OFF")
        ezca.switch('LSC-L1_LOCK','FM2','OFF')  #230321

        # And turn off ASC loops
        isclib.drmi_wfs.drmi_wfs_off()

        #self.l1_gains = {"ETMX":0.9,"ETMY":0.9}
        self.l1_gains = ifoconfig.l1_lock_l_gains

        self.do_once = True

    @assert_mc_lock
    @nodes.checker()
    def run(self):    
        if not self.timer['etmy control down']:
            return

        if not nodes.arrived:
            return

        if self.do_once:
            ezca.write('LSC-L1_LOCK_RSET',2)
            ezca.write('LSC-M0_LOCK_RSET',2)
            for actuator in ['ETMX','ETMY']:
                ezca.write("SUS-{}_L1_LOCK_L_RSET".format(actuator),2)
                ezca.write("SUS-{}_M0_LOCK_L_RSET".format(actuator),2)
                for cross in ["L2L","L2P","L2Y"]:
                    ezca.write("SUS-{}_L1_DRIVEALIGN_{}_RSET".format(actuator,cross),2)

            ezca['LSC-DARM_RSET'] = 2
            time.sleep(0.3)

            for actuator in ifoconfig.darm_actuators:
                ezca.write("SUS-{}_L1_LOCK_L_TRAMP".format(actuator),5)
                ezca.write("SUS-{}_L1_LOCK_L_GAIN".format(actuator),self.l1_gains[actuator])
            ezca.write('LSC-L1_LOCK_TRAMP',5)
            ezca.write('LSC-L1_LOCK_GAIN',ifoconfig.l1_lock_l_gain)
            for actuator in ['ETMX','ETMY']:
                ezca.switch("SUS-{}_L1_LOCK_L".format(actuator),"FM8","OFF","FM9","ON")
            ezca.switch('LSC-L1_LOCK','FM8','OFF','FM9','ON')

            self.do_once = False

        return True

##################################################
# Check to make sure the DIFF beatnote is reasonable before proceeding
class DIFF_BEATNOTE_CHECK(GuardState):
    index = 45
    request = False

    #TODO: only relock YARM if control signal is too large
    def main(self):
        # relock PDH in case of offset
        if abs(ezca['ALS-Y_REFL_SERVO_FASTMON']) < 2.5:
            return True

        #nodes['ALS_YARM'] = 'RELOCK_PDH'
        nodes_loose['ALS_YARM'] = 'RELOCK_PDH'
        time.sleep(1)
        #nodes_loose['ALS_YARM'] = 'PDH_LOCKED'
        if ifoconfig.align_grn:
            nodes_loose['ALS_YARM'] = 'ALIGN_ARM'
        else:
            nodes_loose['ALS_YARM'] = 'PDH_LOCKED'
        time.sleep(1)

        #self.diff_beat = EzAvg(ezca,3.0,'ALS-C_DIFF_A_DEMOD_RFMON')

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        #[done,vals] = self.diff_beat.ezAvg()

        #if not done:
        #    return

        #if vals < ifoconfig.diff_beatnote_threshold:
        #    notify('ALS DIFF beatnote is below threshold')
        #    return

        return True

##################################################
class MISALIGN_RECYCLING_MIRRORS(GuardState):
    index = 46
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        # Misalign the DRMI:
        #ezca['SUS-PRM_M1_OPTICALIGN_P_OFFSET'] =  200  
        #ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET'] = 600

        # TODO: Misalign using Test banks (not sliders)
        prm.misalign('P', -1500, ezca)
        srm.misalign('P', +1500, ezca)

        ezca['CAM-PRM_REFL_90_EXP'] = 100000

        self.timer['misalign_time'] = 5
        # FIXME: Why wait here?

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if self.timer['misalign_time']:
            return True

 ##################################################
class ALS_Y_RELOCK(GuardState):
    index = 48
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        #return True # TH 91518 to skip this during commissioning
        # relock PDH in case of offset
        #nodes['ALS_YARM'] = 'RELOCK_PDH'

        #if not nodes_loose['ALS_YARM'].STATE == 'PDH_LOCKED':
        #    nodes_loose['ALS_YARM'] = 'RELOCK_PDH' #newnodes
        #    time.sleep(3)

        if ifoconfig.align_grn:
            self.als_state = 'ALIGN_ARM'
        else:
            self.als_state = 'PDH_LOCKED'

        nodes_loose['ALS_YARM'] = self.als_state

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        #return True # TH 91518 to skip this during commissioning
        #if nodes_loose['ALS_YARM'].state != self.als_state or not nodes_loose['ALS_YARM'].arrived:
        if not nodes_loose['ALS_YARM'].arrived:
            return

        return True

##################################################
# Begin handing off control of ALS DIFF to the ETMs, using the DARM as the servo filter 
class START_ALS_DIFF(GuardState):
    index = 50
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):

        # Time for ramping on diff
        self.t_diff_start = 20

        # Turn on Diff PLL
        ezca['ALS-C_DIFF_PLL_INEN'] = 1
        time.sleep(0.1)
        # ramp gain to low value
        log('Preparing LSC-DARM filter bank...')
        ezca['LSC-DARM_TRAMP'] = self.t_diff_start
        time.sleep(0.1)
        ezca.switch('LSC-DARM',
                    'FM1', 'FM4', 'FM10', 'OFF',
                    'FM2', 'FM3', 'FM5', 'FM6', 'FM7', 'FM8', 'FM9', 'OUTPUT', 'ON')
        #ezca.switch('LSC-DARM', 'INPUT', 'ON')
        time.sleep(3)   #WAit for filters to ramp on
        #time.sleep(0.1)
        #ezca.switch('ALS-C_DIFF_PLL_CTRL','FM1','FM10','ON','FM5','OFF')
        ezca['LSC-DARM_RSET'] = 2
        time.sleep(0.1)

#        ezca.switch('LSC-DARM','INPUT','ON')
#
#        log('Ramping LSC-DARM gain to -0.008...')
#        ezca['LSC-DARM_GAIN'] = -0.008      #-0.008 ajm 170417
#        self.timer['wait'] = 20

        self.timer["wait"] = 0
        self.engage_darm = True
        self.wait_for_zero = False #Set to true if trying to trigger

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            return

        if self.wait_for_zero:
            if abs(ezca["LSC-DARM_INMON"])<1000:
                return
            self.wait_for_zero = False

        if self.engage_darm:
#        log('Ramping LSC-DARM gain to -0.008...')
            ezca['LSC-DARM_GAIN'] = -0.008
            ezca.switch('LSC-DARM','INPUT','ON')
            self.timer["wait"] = self.t_diff_start
            self.engage_darm = False
            return

        return True

##################################################
class OFFLOAD_DIFF_TO_M0(GuardState):
    index = 52
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):

        # Time for M0 offload
        t_m0 = 8

        log('Beginning feedback to TOP mass...')

        self.m0_total_g = 0.4    #Was this ever 0.8?

        if "ETMX" in ifoconfig.darm_actuators and "ETMY" in ifoconfig.darm_actuators:
            self.m0_gains = {"ETMX":self.m0_total_g/2,"ETMY":self.m0_total_g/2}
        elif "ETMX" in ifoconfig.darm_actuators and "ETMY" not in ifoconfig.darm_actuators:
            # Only offload to ETMX M0, need to double M0 gain.
            self.m0_gains = {"ETMX":self.m0_total_g,"ETMY":0}
        elif "ETMX" not in ifoconfig.darm_actuators and "ETMY" in ifoconfig.darm_actuators:
            # Only offload to ETMY M0, need to double M0 gain.
            self.m0_gains = {"ETMX":0,"ETMY":self.m0_total_g}

        for actuator in ifoconfig.darm_actuators:
            ezca.write("SUS-{0}_M0_LOCK_L_TRAMP".format(actuator),t_m0)
            ezca.write("SUS-{0}_M0_LOCK_L_RSET".format(actuator),2)
        ezca.write('LSC-M0_LOCK_TRAMP',t_m0)
        ezca.write('LSC-M0_LOCK_RSET',2)
        time.sleep(0.1)
        for actuator in ifoconfig.darm_actuators:
            # Ramp to 1/4th first
            ezca.write("SUS-{0}_M0_LOCK_L_GAIN".format(actuator),self.m0_gains[actuator]/4.0)
        ezca.write('LSC-M0_LOCK_GAIN',self.m0_total_g/4.0)

        self.timer['wait'] = t_m0

        self.do_once = True

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):
        if not self.timer['wait']:
            return

        if self.do_once:
            for actuator in ifoconfig.darm_actuators:
                ezca.write("SUS-{0}_M0_LOCK_L_TRAMP".format(actuator),45)   #Changed from 60s ajm20201215
            ezca.write('LSC-M0_LOCK_TRAMP',45)
            time.sleep(0.1)
            for actuator in ifoconfig.darm_actuators:
                ezca.write("SUS-{0}_M0_LOCK_L_GAIN".format(actuator),self.m0_gains[actuator])
            ezca.write('LSC-M0_LOCK_GAIN',self.m0_total_g)
            time.sleep(0.1)
            self.do_once = False

        return True

##################################################
class ALS_DIFF_RAMP_GAIN(GuardState):
    index = 54
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):
        log('Ramping LSC-DARM gain to -0.02...')
        ezca['LSC-DARM_TRAMP'] = 6 #AP changed from 8 to 6
        time.sleep(0.1)
        ezca['LSC-DARM_GAIN'] = -0.02
        time.sleep(8)
        ezca['LSC-DARM_GAIN'] = -0.2
        ezca.switch('ALS-C_DIFF_PLL_CTRL','FM5','ON')
        time.sleep(1)
        self.timer['wait'] = 6 # AP changed from 8 to 6

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        return self.timer['wait']

##################################################
class ALS_DIFF_BOOST(GuardState):
    index = 55
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):
        #FIXME: DIFF PLL BOOST and GAIN already at these values
        log('Turning DIFF PLL Boost on...')
        ezca['ALS-C_DIFF_PLL_COMP1'] = 1
        log('Increasing DIFF PLL gain to -5...')
        ezca['ALS-C_DIFF_PLL_GAIN'] = -5
        time.sleep(0.1)
        log('Ramping LSC-DARM gain to -0.32 ...')
        ezca['LSC-DARM_TRAMP'] = 5 # AP Changed from 10 to 5
        time.sleep(0.3)
        ezca['LSC-DARM_GAIN'] = -0.32    #-0.32  AJM180128 maybe too high?
        self.timer["wait"] = 5

        self.useism_boost = True
        self.darm_int = True

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            return

        # TODO have wait until M0 gain stops ramping
        if self.useism_boost:
            log('Switching on useism boost...')
            for actuator in ['ETMX','ETMY']:
                ezca.switch("SUS-{}_L1_LOCK_L".format(actuator),"FM2","ON")
            ezca.switch("LSC-L1_LOCK","FM2","ON")
            self.useism_boost = False
            self.timer["wait"] = 5
            return

        if self.darm_int:
            log('Switching on DARM integrator...')
            ezca.switch('LSC-DARM', 'FM10', 'ON')
            self.darm_int = False
            self.timer["wait"] = 3     #AJM201210 temp fix, wait to turn on useism.
            return

        return True

##################################################
class ALS_DIFF_LOCKED(GuardState):
    index = 60
    request = True

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):
        return True

##################################################
class COARSE_SCAN_FOR_IR_RESONANCES(GuardState):
    index = 110
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):

        # Note: FSR is ~32.5kHz (really should be 37.5kHz)
        fsr_c = 32500.0
        fsr_d = 33500.0

        if path.exists(irResonances.offsets_filename):
            # Go to previous offsets
            with open(irResonances.offsets_filename,"r") as csvfile:
                reader = csv.DictReader(csvfile)
                list = []
                for row in reader:
                    list.append(dict(row))
                self.offsets = list[-1]

            offsetComm = float(self.offsets["Comm Offset"])
            offsetDiff = float(self.offsets["Diff Offset"])

            #if float(self.offsets["Comm Offset"]) <= 0:
            #    self.offsetCommPlus = float(self.offsets["Comm Offset"]) + fsr_c
            #    self.offsetCommMinus = float(self.offsets["Comm Offset"])
            #else:
            #    self.offsetCommPlus = float(self.offsets["Comm Offset"])
            #    self.offsetCommMinus = float(self.offsets["Comm Offset"]) - fsr_c

            #if float(self.offsets["Diff Offset"]) <= 0:
            #    self.offsetDiffPlus = float(self.offsets["Diff Offset"]) + fsr_c
            #    self.offsetDiffMinus = float(self.offsets["Diff Offset"])
            #else:
            #    self.offsetDiffPlus = float(self.offsets["Diff Offset"]) 
            #    self.offsetDiffMinus = float(self.offsets["Diff Offset"]) - fsr_d


            # 
            self.offsetCommPlus = offsetComm + fsr_c*int((offsetComm <= 0))
            self.offsetCommMinus = offsetComm - fsr_c*int((offsetComm > 0))

            self.offsetDiffPlus = offsetDiff + fsr_d*int((offsetDiff <= 0))
            self.offsetDiffMinus = offsetDiff - fsr_d*int((offsetDiff > 0))
        
        else:
            self.offsetCommMinus = -17500	# Hz
            self.offsetCommPlus = 14800    # Hz

            self.offsetDiffMinus = -36000    #-36000
            self.offsetDiffPlus = 8800      #-2000

        log("Comm plus is {}, Comm minus is {}".format(self.offsetCommPlus,self.offsetCommMinus))
        log("Diff plus is {}, Diff minus is {}".format(self.offsetDiffPlus,self.offsetDiffMinus))


        offsetCommMiddle = (self.offsetCommMinus + self.offsetCommPlus)/2

        # Step size and scan range, both in units of Hz (green or infrared?)
        self.stepComm = 50
        self.rangeComm = 2000
        # Limit to scanning the whole range
        self.scan_limit = math.floor(fsr_c/self.rangeComm)

        # Power channels
        self.xIRChannel = 'LSC-TR_X_NORM_INMON'
        self.yIRChannel = 'LSC-TR_Y_NORM_INMON'        # Power channels

        # Thresholds on IR power
        self.IRPowerThreshold = 10		# cnts for 0.75 W
        #IRMax = 20				# cnts for 0.75 W
        #IRPowerGood = 0.8 * IRMax		# reasonable power

        # Set ramp times to change offsets
        ezca['ALS-C_COMM_PLL_CTRL_TRAMP'] = 5
        ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = 5
        time.sleep(0.5)

        # Read current offsets
        offsetCommCurrent = ezca['ALS-C_COMM_PLL_CTRL_OFFSET']
        offsetDiffCurrent = ezca['ALS-C_DIFF_PLL_CTRL_OFFSET']

        # Compute distance from standart offsets
        distCommMinus = abs(offsetCommCurrent - self.offsetCommMinus)
        distCommPlus = abs(offsetCommCurrent - self.offsetCommPlus)
        distDiffMinus = abs(offsetDiffCurrent - self.offsetDiffMinus)
        distDiffPlus = abs(offsetDiffCurrent - self.offsetDiffPlus)

        # Move DIFF offset to standard value
        #if distDiffMinus < distDiffPlus:
        #    ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = self.offsetDiffMinus
        #else:
        #    ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = self.offsetDiffPlus
        # Move DIFF offset to last known location
        ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = offsetDiff


        # Move Comm offset to set cavities off resonance
        ezca['ALS-C_COMM_PLL_CTRL_OFFSET'] = offsetCommMiddle
        time.sleep(6)


        # Set TR QPD offsets
        ezca['LSC-TR_X_QPD_B_SUM_OFFSET'] = -1.0*cdsutils.avg(2,'LSC-TR_X_QPD_B_SUM_INMON')
        ezca['LSC-TR_Y_QPD_B_SUM_OFFSET'] = -1.0*cdsutils.avg(2,'LSC-TR_Y_QPD_B_SUM_INMON')

        # Move COMM offset to standard value
        #if distCommMinus < distCommPlus:
	    #    ezca['ALS-C_COMM_PLL_CTRL_OFFSET'] = self.offsetCommMinus
        #else:
	    #    ezca['ALS-C_COMM_PLL_CTRL_OFFSET'] = self.offsetCommPlus

        # Move COMM offset to last known location
        ezca['ALS-C_COMM_PLL_CTRL_OFFSET'] = offsetComm

        time.sleep(5)

        log('Offsets moved to standard values')

        # Initialize the scan
        self.commScan = ScanAlsOffset(ezca,'ALS-C_COMM_PLL_CTRL',self.xIRChannel,self.yIRChannel,self.stepComm,self.rangeComm)

        log('Begin Scan')

        self.final_set = True
        self.scan_count = 0
        self.flip_count = 0

        #self.comm_positive = True
        #self.diff_positive = True 

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self): 

        if self.flip_count > 1:
            self.scan_count += 1
            log("Didn't find both resonances, doubling range and scanning again.")
            self.rangeComm *= 2.0
            self.commScan = ScanAlsOffset(ezca,"ALS-C_COMM_PLL_CTRL",self.xIRChannel,self.yIRChannel,self.stepComm,self.rangeComm)
            self.flip_count = 0        

        if self.scan_count > self.scan_limit:
           log("Resonances not found after scanning over entire frequency range")
           return
           #return "DOWN"

        [done,xRes,yRes] = self.commScan.scan()
        if not done:
            return

        # If in the right quadrant, it moves on, if not we try to flip offsets that put us in the correct quadrant and scan again.
        if xRes['power'] < self.IRPowerThreshold and yRes['power'] < self.IRPowerThreshold:
            log('flip comm and scan again')
            self.commScan.flipOffset('ALS-C_COMM_PLL_CTRL',self.offsetCommPlus, self.offsetCommMinus)
            #self.comm_positive = irResonances.flipOffset('ALS-C_COMM_PLL_CTRL',self.offsetCommPlus, self.offsetCommMinus, self.comm_positive)
            #self.commScan.reset()
            self.flip_count += 1
            return
        elif xRes['power'] > self.IRPowerThreshold and yRes['power'] < self.IRPowerThreshold:
            log('flip diff and scan again')
            ezca['ALS-C_COMM_PLL_CTRL_TRAMP']=3
            time.sleep(0.2)
            ezca['ALS-C_COMM_PLL_CTRL_OFFSET'] = xRes['frequency']
            self.commScan.flipOffset('ALS-C_DIFF_PLL_CTRL',self.offsetDiffPlus, self.offsetDiffMinus, wait=10)
            #self.diff_positive = irResonances.flipOffset('ALS-C_DIFF_PLL_CTRL',self.offsetDiffPlus, self.offsetDiffMinus, self.diff_positive)
            #self.commScan.reset()
            self.flip_count += 1
            return
        elif xRes['power'] < self.IRPowerThreshold and yRes['power'] > self.IRPowerThreshold:
            log('flip comm and diff and scan again')
            self.commScan.flipOffset('ALS-C_COMM_PLL_CTRL',self.offsetCommPlus, self.offsetCommMinus)
            #self.comm_positive = irResonances.flipOffset('ALS-C_COMM_PLL_CTRL',self.offsetCommPlus, self.offsetCommMinus, self.comm_positive)
            self.commScan.flipOffset('ALS-C_DIFF_PLL_CTRL',self.offsetDiffPlus, self.offsetDiffMinus, wait=10)
            #self.diff_positive = irResonances.flipOffset('ALS-C_DIFF_PLL_CTRL',self.offsetDiffPlus, self.offsetDiffMinus, self.diff_positive)
            #self.commScan.reset()
            self.flip_count += 1
            return


        # Go to the approximate offsets where the resonances are, only do this once
        if self.final_set:
            self.final_set = False
            
            ezca['ALS-C_COMM_PLL_CTRL_TRAMP'] = 5
            ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = 5
            time.sleep(0.1)

            ezca['ALS-C_COMM_PLL_CTRL_OFFSET'] = xRes['frequency']

            diffOffsetCurrent = ezca['ALS-C_DIFF_PLL_CTRL_OFFSET']
            diffOffsetCoarse = diffOffsetCurrent + (xRes['frequency'] - yRes['frequency'])

            ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = diffOffsetCoarse

            time.sleep(5)

        return True

##################################################
class FINE_SCAN_FOR_IR_RESONANCES(GuardState):
    index = 120
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):

        stepFine = 10   #Hz
        rangeFine = 100  #Hz
        #rangeFine = 50  #Hz

        xIRChannel = 'LSC-TR_X_NORM_INMON'
        yIRChannel = 'LSC-TR_Y_NORM_INMON'

        # Set up ALS Comm and Diff fine scans
        self.commScan = ScanAlsOffset(ezca,'ALS-C_COMM_PLL_CTRL',xIRChannel,yIRChannel,stepFine,rangeFine,sleepTime=0.5,rampTime=0.1)
        self.diffScan = ScanAlsOffset(ezca,'ALS-C_DIFF_PLL_CTRL',xIRChannel,yIRChannel,stepFine,rangeFine,sleepTime=0.5,rampTime=0.1)

        self.done1 = False
        self.done2 = False

        # File for recording the IR resonance offsets
        self.pathname = '/opt/rtcds/userapps/trunk/lsc/l1/scripts/transition/als/'
        self.filename = 'irResonanceOffsets.txt'
        self.writetofile = True

        # CSV File for recording IR resonance offsets
        #pathname = "/opt/rtcds/userapps/trunk/isc/l1/guardian/"
        #filename = "irResonanceOffsets"
        #self.fname = pathname + filename + ".csv"
        if not path.exists(irResonances.offsets_filename):
            with open(irResonances.offsets_filename,"w+") as csvfile:
                writer = csv.DictWriter(csvfile,fieldnames=irResonances.offsets_fieldnames)
                writer.writeheader()
        self.writetocsv = True



    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):

        # Fine scan ALS Comm
        if not self.done1:
            [self.done1,xResFineC,yResFineC] = self.commScan.scan()
            if self.done1:
                ezca['ALS-C_COMM_PLL_CTRL_TRAMP'] = 1
                time.sleep(0.1)
                ezca['ALS-C_COMM_PLL_CTRL_OFFSET'] = xResFineC['frequency']
                time.sleep(2)
                log('The ALS Comm Offset has been set')
            return

        # Fine scan ALS Diff
        if not self.done2:
            [self.done2,xResFineD,yResFineD] = self.diffScan.scan()
            if self.done2:
                ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = 3
                time.sleep(0.1)
                ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = yResFineD['frequency']
                time.sleep(4)
                log('The ALS Diff Offset has been set')
            return

        # Record the ALS offsets that give IR resonances
        if self.writetofile:
            self.writetofile = False
            comm_offset = ezca['ALS-C_COMM_PLL_CTRL_OFFSET']
            diff_offset = ezca['ALS-C_DIFF_PLL_CTRL_OFFSET']
            f_comm_vco = ezca['ALS-C_COMM_VCO_FREQUENCY']
            f_diff_vco = ezca['ALS-C_DIFF_VCO_FREQUENCY']
            f_x_pll = ezca['ALS-X_FIBR_LOCK_BEAT_FREQUENCY']*1e6
            f_y_pll = ezca['ALS-Y_FIBR_LOCK_BEAT_FREQUENCY']*1e6

            f = open(self.pathname + self.filename, "a+")
            f.write(str(comm_offset) + '\t' + str(diff_offset) + '\t' + str(f_comm_vco) + '\t' + str(f_diff_vco) + '\t' + str(f_x_pll) + '\t' + str(f_y_pll) + '\n')

        if self.writetocsv:
            offsets = {"Time": int(gpstime.gpsnow()),
                       "Comm Offset": ezca['ALS-C_COMM_PLL_CTRL_OFFSET'],
                       "Diff Offset": ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'],
                       "Comm VCO freq": ezca['ALS-C_COMM_VCO_FREQUENCY'],
                       "Diff VCO Freq": ezca['ALS-C_DIFF_VCO_FREQUENCY'],
                       "X PLL Freq": ezca['ALS-X_FIBR_LOCK_BEAT_FREQUENCY']*1e6,
                       "Y PLL Freq": ezca['ALS-Y_FIBR_LOCK_BEAT_FREQUENCY']*1e6}
            with open(irResonances.offsets_filename,"a") as csvfile:
                writer = csv.DictWriter(csvfile,fieldnames=irResonances.offsets_fieldnames)
                writer.writerow(offsets)
            self.writetocsv = False

        return True

##################################################
class ALS_COMPLETE(GuardState):
    index = 200
    request = True

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):
        return True

##################################################
class OFFSET_COMM(GuardState):
    index = 210
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):

        ezca['ALS-C_COMM_PLL_CTRL_TRAMP'] = 1

        # Offset ALS COMM by 1.5kHz
        ezca['ALS-C_COMM_PLL_CTRL_OFFSET'] = ezca['ALS-C_COMM_PLL_CTRL_OFFSET'] + 1520

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):

        return True

##################################################
class ALIGN_BS(GuardState):
    request = False
    index = 250
    
    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):

        #nodes_loose['ALIGN_BS'] = 'SKIP_ALIGN_FOR_BS'

        self.states = ['SKIP_ALIGN_FOR_MICH','BS_ALIGNED','BS_ALIGN_IDLE']

        self.ii = 0
        self.set_state = True
        #return True

        self.timer['wait'] = 0

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            log('Waiting on timer')
            return

        if self.set_state:
            nodes_loose['ALIGN_BS'] = self.states[self.ii]
            log('Requesting ALIGN BS to {}'.format(self.states[self.ii]))
            self.ii+=1
            self.set_state = False
            self.timer['wait'] = 2
            return

        if not (nodes_loose['ALIGN_BS'].arrived and nodes_loose['ALIGN_BS'].done):
            #notify('ALIGN BS in state {}'.format(nodes_loose[ALIGN_BS].state))
            return

        if self.ii < len(self.states):
            log('Next state')
            self.set_state = True
            return

        return True



##################################################
#class ALIGN_PSRM_OFFSET_COMM(GuardState):
class ALIGN_RECYCLING_MIRRORS(GuardState):
    index = 310
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):

        #ezca['ALS-C_COMM_PLL_CTRL_TRAMP'] = 1

        # Offset ALS COMM by 1.5kHz
        #ezca['ALS-C_COMM_PLL_CTRL_OFFSET'] = ezca['ALS-C_COMM_PLL_CTRL_OFFSET'] + 1520

        # Set input power to 10W
        if ezca['IMC-IM4_TRANS_SUM_OUTPUT'] > 1.4:
            ezca['PSL-GUARD_POWER_REQUEST'] = 1
            ezca['PSL-GUARD_STEP_REFL_GAIN'] = 0
            ezca['PSL-GUARD_STEP_DC_GAIN'] = 0
            time.sleep(0.1)

            nodes.set_managed()
            #nodes['IMC_LOCK'].set_request('LOCKED_1W')
            nodes['IMC_LOCK'].set_request('IFO_POWER')

        ezca['OMC-PZT2_TRAMP'] = 1.0 
        time.sleep(0.2)        
        omc_storedoffset = float(ezca['OMC-STORE_PZT2_OFFSET'])
        ezca['OMC-PZT2_OFFSET'] = omc_storedoffset - 2.5
        time.sleep(0.2)
        ezca['OMC-PZT2_TRAMP'] = 0.1


        # TODO: Remove the test bank alignment offset
        #for dof in ['P','Y']:
        #    prm.align(dof,ezca)
        #    srm.align(dof,ezca)
        self.align_rms = True


        self.timer['optic align wait'] = 10


    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):

        if not (nodes['IMC_LOCK'].arrived and nodes['IMC_LOCK'].done):
            notify('Still powering down')
            return

        if self.align_rms:
            for dof in ['P','Y']:
                prm.align(dof,ezca)
                srm.align(dof,ezca)
            ezca['CAM-PRM_REFL_90_EXP'] = 1000
            self.align_rms = False
            self.timer['optic align wait'] = 10

        if self.timer['optic align wait']:
            return True


##################################################
class FREEZE_BS(GuardState):
    index = 397
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    def main(self):
        ezca['LSC-CPSFF_GAIN'] = 10.0 #8.3

        return True


##################################################
class POWER_DOWN_500mW(GuardState):
    index = 399
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    def main(self):

        nodes.set_managed()    #newnode

        # set the EPICS channels for the power down
        ezca['PSL-GUARD_POWER_REQUEST']  = 0.5
        ezca['PSL-GUARD_STEP_REFL_GAIN'] = 0
        ezca['PSL-GUARD_STEP_DC_GAIN']   = 0
        time.sleep(1)

        nodes['IMC_LOCK'].set_request('IFO_POWER')

    assert_mc_lock
    @assert_als_y_lock
    def run(self):

        if not nodes['IMC_LOCK'].completed:
            return

        return True


##################################################

def turn_off_drmi():
    # Make sure DRMI WFS are turned off
    for dof in ['DC1', 'DC2', 'DC3', 'DC4', 'DC5', 'INP2', 'PRC1','PRC2', 'MICH', 'SRC1','SRC2']:
        ezca.switch('ASC-' + dof + '_P','INPUT','OFF')
        ezca.switch('ASC-' + dof + '_Y','INPUT','OFF')

    # Turn off DOF filter banks and clear histories
    ezca['LSC-MICH_TRAMP'] = 0
    ezca['LSC-PRCL_TRAMP'] = 0
    ezca['LSC-SRCL_TRAMP'] = 0
    time.sleep(0.1)
    ezca['LSC-MICH_GAIN'] = 0
    ezca['LSC-PRCL_GAIN'] = 0
    ezca['LSC-SRCL_GAIN'] = 0
    time.sleep(0.1)
    ezca['LSC-MICH_RSET'] = 2
    ezca['LSC-PRCL_RSET'] = 2
    ezca['LSC-SRCL_RSET'] = 2

    # Turn OFF M1 and M2 integrators
    for sus in ['SR2','SRM','PRM','PR2']:
        ezca.switch('SUS-' + sus + '_M1_LOCK_L','INPUT','OFF')
        ezca.switch('SUS-' + sus + '_M2_LOCK_L','FM1','OFF','FM7','ON')

    # Reset M1 control
    for sus in ['SR2','SRM','PRM','PR2']:
        ezca['SUS-' + sus + '_M1_LOCK_L_RSET'] = 2
        ezca['SUS-' + sus + '_M1_LOCK_P_RSET'] = 2
        ezca['SUS-' + sus + '_M1_LOCK_Y_RSET'] = 2

    # Reset centering and ASC servoes
    for dof in ['DC1', 'DC2', 'DC3', 'DC4', 'DC5', 'INP2', 'PRC1', 'PRC2', 'MICH', 'SRC1', 'SRC2']:
        ezca['ASC-' + dof + '_P_RSET'] = 2
        ezca['ASC-' + dof + '_Y_RSET'] = 2

###########################

class LOCK_DRMI(GuardState):
    index = 400
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):
        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed()    #newnodes

        turn_off_drmi()

        # Make sure fast shutter is closed while locking DRMI
        ezca['SYS-MOTION_C_FASTSHUTTER_A_BLOCK'] = 1

        # set the EPICs channels for the power down
        #ezca['PSL-GUARD_POWER_REQUEST'] = 0.56  #0.5
        #ezca['PSL-GUARD_STEP_REFL_GAIN'] = 0
        #ezca['PSL-GUARD_STEP_DC_GAIN'] = 0
        #time.sleep(0.1)
        #JCB 9/7/2016
        #nodes['IMC_LOCK'].set_request('IFO_POWER')

        # Turn on BS bounce roll damping
        ezca['SUS-BS_M2_VRDAMP_P_GAIN'] = 1
        ezca['SUS-BS_M2_VRDAMP_Y_GAIN'] = -1
        ezca.switch('SUS-BS_M2_VRDAMP_P','INPUT','OUTPUT','ON')
        ezca.switch('SUS-BS_M2_VRDAMP_Y','INPUT','OUTPUT','ON')

        #In case we dropped out after switching to 3F, but didn't pass through DOWN, reset 3F matrix
        matrix.lsc_input.TRAMP = 0
        matrix.lsc_input.zero(row='MICH')
        matrix.lsc_input.zero(row='PRCL')
        matrix.lsc_input.zero(row='SRCL')
        time.sleep(0.1)
        #matrix.lsc_input['MICH', 'REFLAIR_B_RF135_Q'] = 0 
        #matrix.lsc_input['PRCL', 'REFLAIR_B_RF27_I'] = 0 
        #matrix.lsc_input['SRCL', 'REFLAIR_B_RF27_I'] = 0 
        #matrix.lsc_input['SRCL', 'REFLAIR_B_RF135_I'] = 0
        matrix.lsc_input['MICH', 'REFL_A_RF45_Q'] = 1 #ifoconfig.refl_45Q_mich
        matrix.lsc_input['PRCL', 'REFL_A_RF9_I'] = 1 # ifoconfig.refl_9I_prcl
        matrix.lsc_input['SRCL', 'REFL_A_RF9_I'] = 0.42 # ifoconfig.refl_9I_srcl
        matrix.lsc_input['SRCL', 'REFL_A_RF45_I'] = 1 # ifoconfig.refl_45I_srcl
        time.sleep(0.1)
        matrix.lsc_input.load()

        # Transition actuators to acquire state
        for osem in ['UL', 'UR', 'LL', 'LR']:
            ezca['SUS-BS_BIO_M2_' + osem + '_STATEREQ'] = 2
        #nodes_optics['SUS_BS'] = 'COILDRIVER_LOCK_ACQ'

        for optic in ['PRM', 'PR2', 'SRM', 'SR2']:
            for osem in ['UL', 'UR', 'LL', 'LR']:
                ezca['SUS-' + optic + '_BIO_M3_' + osem + '_STATEREQ'] = 2

        # Turn on BS oplev # AE changed to end of down script 
        ezca.switch('SUS-BS_M2_OLDAMP_P','FM1','INPUT','OUTPUT','ON')

        # Ramp not really needed, as DRMI is triggered
        ezca['LSC-MICH_TRAMP'] = 1
        ezca['LSC-PRCL_TRAMP'] = 1
        ezca['LSC-SRCL_TRAMP'] = 1
        time.sleep(0.1)

        # Triggering Thresholds
        trigs = {'MICH':{'ON':150,'OFF':80},
                 'PRCL':{'ON':80,'OFF':50},
                 'SRCL':{'ON':150,'OFF':80}}

        for dof in ['MICH','PRCL','SRCL']:
            for th in ['ON','OFF']:
                ezca['LSC-{}_TRIG_THRESH_{}'.format(dof,th)] = trigs[dof][th]
                ezca['LSC-{}_FM_TRIG_THRESH_{}'.format(dof,th)] = trigs[dof][th]


        #ezca['LSC-MICH_TRIG_THRESH_ON'] = 150
        #ezca['LSC-MICH_TRIG_THRESH_OFF'] = 80

        #ezca['LSC-PRCL_TRIG_THRESH_ON'] = 80
        #ezca['LSC-PRCL_TRIG_THRESH_OFF'] = 50

        #ezca['LSC-SRCL_TRIG_THRESH_ON'] = 150
        #ezca['LSC-SRCL_TRIG_THRESH_OFF'] = 80

        # Set servo gains (AE 160808)
        ezca['LSC-MICH_GAIN'] = -0.12 #-0.14 #-0.09 
        ezca['LSC-PRCL_GAIN'] = 1.5 
        ezca['LSC-SRCL_GAIN'] = 2.0 # 1.6

        # Set SR2 M2 lock gain
        #ezca['SUS-SR2_M2_LOCK_L_GAIN'] = 0.6
        ezca['SUS-SR2_M2_LOCK_L_GAIN'] = 1.3    #AJM211108 was causing 1.1Hz oscillation

        # Set drivealigns
        for sus in ['PRM','PR2','SRM','SR2']:
            ezca['SUS-' + sus + '_M3_DRIVEALIGN_L2L_GAIN'] = 1

        # We now use SRM for locking
        ezca['SUS-SR2_M2_DRIVEALIGN_L2L_TRAMP'] = 1
        ezca['SUS-SRM_M2_DRIVEALIGN_L2L_TRAMP'] = 1
        time.sleep(0.5)
        ezca['SUS-SR2_M2_DRIVEALIGN_L2L_GAIN'] = 1  #0.5
        ezca['SUS-SRM_M2_DRIVEALIGN_L2L_GAIN'] = 0

        # AP20190409 - Turns off the x0.1 gain (see alog 44997, 45034)
        ezca.switch('SUS-SR2_M1_LOCK_L','FM2','OFF')
        ezca.switch('SUS-SRM_M1_LOCK_L','FM2','OFF')

        # Outputs on
        ezca.switch('LSC-PRCL','FM4','OFF','OUTPUT','ON')
        ezca.switch('LSC-SRCL','OUTPUT','ON')
        ezca.switch('LSC-MICH','OUTPUT','LIMIT','FM4','ON','FM3','FM5','FM6','FM7','OFF')
        
        #5-19-22 TJO
        #Adds SRM controls to offsets which were saved at last lock from text file if current P and Y Offsets are the same as values saved. Sets values in text file to 0 to avoiod multiple additions of controls
        SRMfilepath = '/opt/rtcds/userapps/trunk/sus/l1/scripts/pass_SRM_controls_DO_NOT_EDIT.txt'       
        if path.isfile(SRMfilepath):
            #print("There is a file")
            SRMfile = open(SRMfilepath,'r')
            passedSRMcontrols = SRMfile.readlines()
            SRMfile.close()
            if str(ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET']) == passedSRMcontrols[3][14:-1] and str(ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET']) == passedSRMcontrols[4][14:-1]:
                ezca['SUS-SRM_M1_OPTICALIGN_P_TRAMP'] = 10
                ezca['SUS-SRM_M1_OPTICALIGN_Y_TRAMP'] = 10  
                ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET'] += float(passedSRMcontrols[1][14:-1])
                ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET'] += float(passedSRMcontrols[2][14:-1])
                ezca['SUS-SRM_M1_OPTICALIGN_P_TRAMP'] = 1
                ezca['SUS-SRM_M1_OPTICALIGN_Y_TRAMP'] = 1      
                #commented out, was for testing                
                #print('pass')
                #print(passedSRMcontrols[0])
                #print(ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET'] + float(passedSRMcontrols[1][14:-1]))
                #print(ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET']+ float(passedSRMcontrols[2][14:-1]))
                #print("PASS DATA NOW")
        SRMfile = open(SRMfilepath,'w')
        SRMfile.truncate(0)
        SRMfile.write('DO NOT EDIT THIS FILE (USED in ISC_LOCK see state 400 and 560)' + '\n')
        SRMfile.write('Add to SRM P: ' + str(0) + '\n')
        SRMfile.write('Add to SRM Y: ' + str(0) + '\n')
        SRMfile.write('Current P:    ' + str(ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET']) + '\n')
        SRMfile.write('Current Y:    ' + str(ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET']) + '\n')
        SRMfile.close()

        t_lock = 3.0
        self.pop18 = EzAvg(ezca,t_lock,'LSC-POPAIR_B_RF18_I_NORM_MON')

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):

        [done,a_value] = self.pop18.ezAvg()
        if not done:
            log('Filling buffer')
            return

        #a_value = cdsutils.avg(0.5, 'LSC-POPAIR_B_RF18_I_NORM_MON')

        if a_value < ifoconfig.drmi_locked_threshold_pop18i: # *input_power_scaling:
            log('waiting for DRMI to lock...')
            return
        else:
            log('DRMI locked! Boost!')

        ezca['LSC-MICH_GAIN'] = -0.10
        ezca.switch('LSC-MICH','FM5','FM6','FM7','FM10','ON','LIMIT','OFF') 

        # Start feedback to PRM and SRM M1
        #ezca.switch('SUS-PRM_M1_LOCK_L','INPUT','ON')

        ezca.switch('SUS-PR2_M1_LOCK_L','INPUT','ON')
        ezca.switch('SUS-SR2_M1_LOCK_L','INPUT','ON')

        #ezca.switch('SUS-SRM_M1_LOCK_L','INPUT','ON')

        time.sleep(1)     #(0.5)
            
        # Increase M2/M3 crossover frequency
        #ezca.switch('SUS-PRM_M2_LOCK_L','FM1','ON','FM7','OFF') # This output is currently off, do we need to bother switching it? ajm 20170825
        ezca.switch('SUS-PR2_M2_LOCK_L','FM1','ON','FM7','OFF')
        ezca.switch('SUS-SR2_M2_LOCK_L','FM1','ON','FM7','OFF')
        ezca.switch('SUS-SRM_M2_LOCK_L','FM1','ON','FM7','OFF')    

        return True

##################################################
class DRMI_LOCKED(GuardState):
    index = 420
    request = True

    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def run(self):

        return True


##################################################

def switch_asdc_centering_mtrx(mode):

    asdc_mtrxs = asdc_centering_matrix.asdc_omtrx_vals

    for dof in ['P','Y']:
        ii = 0
        for om in ['OM1','OM2','OM3']:
            jj = 0
            for sensor in ['DC3','DC4']:
                if dof == 'P':
                    matrix.asc_output_pit[om,sensor] = asdc_mtrxs[mode+'_'+dof][ii,jj]
                if dof == 'Y':
                    matrix.asc_output_yaw[om,sensor] = asdc_mtrxs[mode+'_'+dof][ii,jj]
                jj+=1
            ii+=1

##################################################
class ENGAGE_DRMI_ASC_CENTERING(GuardState):
    index = 430
    request = False
    
    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def main(self):

        #switch_asdc_centering_mtrx('INITIAL')
        switch_asdc_centering_mtrx('FINAL')

        # Open OMC fast shutter for AS WFS
        ezca['SYS-MOTION_C_FASTSHUTTER_A_UNBLOCK'] = 1
        time.sleep(0.3)
    
        # Turn on DC centering
        #for dof in ['DC1','DC2','DC3','DC4']:
        #    for ww in ['P', 'Y']:
        #        ezca.switch('ASC-' + dof + '_' + ww,'INPUT','ON')

        ezca.switch('ASC-DC1_P','INPUT','ON')
        ezca.switch('ASC-DC1_Y','INPUT','ON')
        ezca.switch('ASC-DC2_P','INPUT','ON')
        ezca.switch('ASC-DC2_Y','INPUT','ON')
        ezca.switch('ASC-DC3_P','INPUT','ON')
        ezca.switch('ASC-DC3_Y','INPUT','ON')
        #ezca.switch('ASC-DC4_P','INPUT','ON')
        #ezca.switch('ASC-DC4_Y','INPUT','ON')

        # POP X DC centering
        if ifoconfig.popx:
            ezca.switch('ASC-DC5_P','INPUT','ON')
            ezca.switch('ASC-DC5_Y','INPUT','ON')

        #ezca['ASC-AS_A_RF45_WHITEN_SET_2'] = 1
        #ezca['ASC-AS_B_RF45_WHITEN_SET_2'] = 1

        self.timer['wait'] = 0

        self.chans = []
        for sensor in ['REFL_A','REFL_B','AS_A']:
            for dof in ['PIT','YAW']:
                self.chans.append("ASC-{}_DC_{}_OUTPUT".format(sensor,dof))

        self.refl_th = 0.2
        self.as_th = 0.2

        self.qpds = EzAvg(ezca,1,self.chans)    #TODO: remove self from chans

        #self.thresholds = {'REFL_A':0.3,'REFL_B':0.3,'AS_A':0.3}

        self.check_centering = True


    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return

        [done,avgs] = self.qpds.ezAvg()
        if not done:
            log('filling buffer')
            return

        if self.check_centering:
            #avgs = cdsutils.avg(1,self.chans)
            refa_p = avgs[0]
            refa_y = avgs[1]
            refb_p = avgs[2]
            refb_y = avgs[3]
            asa_p = avgs[4]
            asa_y = avgs[5]

            if (abs(refa_p) > self.refl_th or abs(refa_y) > self.refl_th or
            abs(refb_p) > self.refl_th or abs(refb_y) > self.refl_th or
            abs(asa_p) > self.as_th or abs(asa_y) > self.as_th):
                notify('Waiting for qpd spots to center')
                return

        return True

##################################################w
class ENGAGE_DRMI_ASC(GuardState):
    index = 431
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def main(self):

        # Set INP2 and PRC2 pre gains
        for dof1 in ['INP2','PRC2']:
            for dof2 in ['P','Y']:
                ezca.write("ASC-{}_{}_PRE_TRAMP".format(dof1,dof2),0)
        time.sleep(0.3)
        #Note these pre-gains are already set in SDF
        ezca['ASC-PRC2_P_PRE_GAIN'] = 0.5   #1.0    #AJM20230117 lowered because rephased REFL WFS
        ezca['ASC-PRC2_Y_PRE_GAIN'] = 0.5   #1.0
        ezca['ASC-INP2_P_PRE_GAIN'] = 0.5   #1.0
        ezca['ASC-INP2_Y_PRE_GAIN'] = 0.5   #1.0

        ezca['ASC-SRCL2_P_PRE_GAIN'] = 0.3
        ezca['ASC-SRCL2_Y_PRE_GAIN'] = 0.3
 
       # Turn off BS oplev
        ezca.switch('SUS-BS_M2_OLDAMP_P','OUTPUT','OFF')
        ezca.switch('SUS-BS_M2_OLDAMP_Y','OUTPUT','OFF')

        # Turn on RF alignment servos
        for dof in ['MICH','PRC2','INP2','SRC1','SRC2']:
            ezca.switch('ASC-' + dof + '_P','INPUT','ON')
            ezca.switch('ASC-' + dof + '_Y','INPUT','ON')

        self.timer['wait'] = 10
        # Turn on PRC boost
        ezca.switch('LSC-PRCL','FM4','ON')

        ezca['ASC-DC3_Y_GAIN'] = 3


    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return

        return True

##################################################

#TODO: Make a generic function for offloading

class OFFLOAD_DRMI_ASC(GuardState):
    index = 440
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def main(self):

        self.cals = {'PRM':{'P':1.875,'Y':2.681},
                     'PR2':{'P':1.875,'Y':2.681},
                     'BS':{'P':143.3,'Y':140.1},
                     'SRM':{'P':1.875,'Y':2.681},
                     'SR2':{'P':-1724.1,'Y':-3333.3}, #(offloads from M2)
                     'IM4':{'P':9.927,'Y':5.431},
                     }

        # set ramp times to t_offl sec
        t_offl  = 10
        #for opt in ['PRM','PR2','SRM','SR2','BS','IM4']:
        #    for ww in ['P', 'Y']:
        #        ezca['SUS-' + opt + '_M1_OPTICALIGN_' + ww + '_TRAMP'] = t_offl

        self.sus_offload_list = ['PR2','SR2','BS','IM4']

        self.achans = []
        for sus in self.sus_offload_list:
            for dof in ['P','Y']:
                if sus == 'SR2' or sus == 'BS':
                    stage = 'M2'
                else:
                    stage = 'M1'
                self.achans.append('SUS-{}_{}_LOCK_{}_OUT16'.format(sus,stage,dof))

        #control = dict(zip(achans, aout))

        self.control = EzAvg(ezca,15,self.achans)

        # wait for ramps
        self.timer['wait'] = 0

        self.offload = True

    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return

        [done,vals] = self.control.ezAvg()
        if not done:
            log('Averaging control signals')
            return

        if self.offload:
            ii = 0
            #Calculate offloads and store in dictionary
            offload_vals = {}
            offload_max = 0
            for sus in self.sus_offload_list:
                offload_vals[sus] = {}
                for dof in ['P','Y']:
                    offload_vals[sus][dof] = vals[ii]/self.cals[sus][dof]
                    if abs(offload_vals[sus][dof]) > abs(offload_max):
                        offload_max = offload_vals[sus][dof]
                    ii+=1

            #Set tramp based on offset size
            if 0 <= abs(offload_max) < 15:
                t_offl = 10
            elif 15 <= abs(offload_max) < 30:
                t_offl = 20
            else:
                t_offl = 30

            for sus in self.sus_offload_list:
                for dof in ['P','Y']:
                    ezca['SUS-{}_M1_OPTICALIGN_{}_TRAMP'.format(sus,dof)] = t_offl
            time.sleep(0.1)

            #Do the offload
            for sus in self.sus_offload_list:
                for dof in ['P','Y']:
                     ezca['SUS-{}_M1_OPTICALIGN_{}_OFFSET'.format(sus,dof)] += offload_vals[sus][dof]

            self.timer['wait'] = t_offl - 9
            self.offload = False
            return

        # return ramp times back to 1 sec
        for sus in self.sus_offload_list:
            for dof in ['P', 'Y']:
                ezca['SUS-{}_M1_OPTICALIGN_{}_TRAMP'.format(sus,dof)] = 1

        return True

        #FIXME: This section of code below isn't actually happening
        # clear corner integrators for OAF CAL
        ezca['OAF-CAL_SUM_IMC_M1_RSET'] = 2
        ezca['OAF-CAL_SUM_IMC_M2_RSET'] = 2
        ezca['OAF-CAL_SUM_MICH_BS_M2_RSET'] = 2
        ezca['OAF-CAL_SUM_PRCL_PRM_M2_RSET'] = 2
        ezca['OAF-CAL_SUM_SRCL_SRM_M2_RSET'] = 2
        ezca['OAF-CAL_SUM_SRCL_SRM_M1_RSET'] = 2

        ezca['CAL-CS_SUM_IMC_CTRL_M1_RSET'] = 2
        ezca['CAL-CS_SUM_IMC_CTRL_M2_RSET'] = 2
        ezca['CAL-CS_SUM_MICH_BS_M2_RSET'] = 2
        ezca['CAL-CS_SUM_PRCL_PRM_M2_RSET'] = 2
        #ezca['CAL-CS_SUM_PRCL_PR2_M2_RSET'] = 2
        ezca['CAL-CS_SUM_SRCL_SRM_M2_RSET'] = 2
        ezca['CAL-CS_SUM_SRCL_SRM_M1_RSET'] = 2
        #ezca['CAL-CS_SUM_SRCL_SR2_M2_RSET'] = 2
        #ezca['CAL-CS_SUM_SRCL_SR2_M1_RSET'] = 2

##################################################
class HEAT_BS(GuardState):
    index = 441
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    def main(self):
        ezca['LSC-CPSFF_GAIN'] = 0

##################################################
class DRMI_LOCKED_ASC(GuardState):
    index = 500
    request = True

    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def main(self):
        self.timer['DRMI alignment checkpoint timer'] = 1

    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def run(self):
        if self.timer['DRMI alignment checkpoint timer']:
            fast_shutter_ready = True
            if ezca['SYS-MOTION_C_SHUTTER_G_TRIGGER_VOLTS'] < 0.005:
                notify('No light on Fast shutter trigger PD (AS-C-QPD), do not proceed')
                fast_shutter_ready = False
            if ezca['SYS-MOTION_C_SHUTTER_G_ERROR_FLAG'] or ezca['SYS-MOTION_C_FASTSHUTTER_A_ERROR_FLAG']:
                notify('Fast shutter has error flag, cannot proceed')
                fast_shutter_ready = False
            if ((ezca['SYS-MOTION_C_FASTSHUTTER_A_READY'] == 0) or (ezca['SYS-PROTECTION_AS_FASTSHUTTERREADY'] == 0) or (ezca['SYS-PROTECTION_AS_LOWPOWERREADY'] == 0)):
                notify('Fast Shutter is not ready, cannot proceed')
                fast_shutter_ready = False
            if not fast_shutter_ready:
                return False
            return True

##################################################
class POWER_UP_2W(GuardState):
    index   = 520
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def main(self):
        # make sure IMC guardian is still managed        
        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed()    #newnodes

        # Turn on TM Optical Lever Damping  
        #for quad in ['ITMX', 'ITMY', 'ETMX', 'ETMY']:              #ajm180214 commented out cause Arnaud told me to
        #    ezca.switch('SUS-' + quad + '_L1_OLDAMP_P','OUTPUT','ON')
        #    ezca['SUS-' + quad + '_L1_OLDAMP_P_TRAMP'] = 5

#        time.sleep(0.1)     
#        for quad in ['ITMX', 'ITMY', 'ETMX', 'ETMY']:
#            ezca['SUS-' + quad + '_L1_OLDAMP_P_GAIN'] = 0# Changed from 1.5 to 1 for new loops
#        time.sleep(0.1)

        #nodes['IMC_LOCK'] = 'LOCKED_4W'
        # set the EPICs channels for the power up
        ezca['PSL-GUARD_POWER_REQUEST'] = 2
        ezca['PSL-GUARD_STEP_REFL_GAIN'] = 0
        ezca['PSL-GUARD_STEP_DC_GAIN'] = 0
        time.sleep(1)
        #nodes['IMC_LOCK'] = 'IFO_POWER'
        #JCB 9/7/2016
        nodes['IMC_LOCK'].set_request('IFO_POWER')

        # set baffle PD gains to not saturate, or at least lowest 20181113 - CB moved from set cal lines state
        for tm in ['ITMX','ITMY','ETMX','ETMY']:
            for nr in ['1','4']:
                ezca['AOS-' + tm + '_BAFFLEPD_' + nr + '_GAINSETTING'] = 0

        for quad in ['ITMX','ITMY','ETMX','ETMY']:
            for dof in ['P','Y']:
                ezca.switch('SUS-' + quad + '_L2_LOCK_'+ dof, 'FM8', 'ON')

        time.sleep(2)
        ezca['OMC-LSC_SERVO_RSET'] = 2 # clear OMC servo for CARM offset reduction

        #ezca['CAM-ETMY_SCATTER_EXP']=10000 #CB20190410 to capture scatter power up images

    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def run(self):
        #return nodes['IMC_LOCK'].arrived
        if not (nodes['IMC_LOCK'].arrived and nodes['IMC_LOCK'].done):
            log('Still powering up')
            return
        return True

##################################################
class ZERO_3F_OFFSETS(GuardState):
    index = 530
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def main(self):

        chans = []
        for dmd in ['27','135']:
            for ph in ['I','Q']:
                chans.append('LSC-REFLAIR_B_RF{}_{}_INMON'.format(dmd,ph))

        time.sleep(3)

        vals = cdsutils.avg(3,chans,stddev=False)
        #vals = ezavg(ezca,3,chans)

        time.sleep(1)

        ii = 0
        for dmd in ['27','135']:
            for ph in ['I','Q']:
                ezca.write('LSC-REFLAIR_B_RF{}_{}_OFFSET'.format(dmd,ph),-1.0*vals[ii])
                ii+=1

        time.sleep(1)

    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def run(self):

        return True

##################################################
class DRMI_TO_3F(GuardState):
    index = 550
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def main(self):
        
        matrix.lsc_input.TRAMP = 2
        matrix.lsc_input['MICH', 'REFL_A_RF45_Q'] = 0
        matrix.lsc_input['MICH', 'REFLAIR_B_RF135_Q'] = 4.5 #ifoconfig.refl_135Q_mich
        matrix.lsc_input['PRCL', 'REFL_A_RF9_I'] = 0
        matrix.lsc_input['PRCL', 'REFLAIR_B_RF27_I'] = 0.6 #ifoconfig.refl_27I_prcl
        matrix.lsc_input['SRCL', 'REFL_A_RF9_I'] = 0
        matrix.lsc_input['SRCL', 'REFL_A_RF45_I'] = 0
        matrix.lsc_input['SRCL', 'REFLAIR_B_RF27_I'] = 0.18 #ifoconfig.refl_27I_srcl
        matrix.lsc_input['SRCL', 'REFLAIR_B_RF135_I'] = 5.2 #ifoconfig.refl_135I_srcl
        matrix.lsc_input.load()
        ezca['LSC-SRCL_TRAMP'] = 3
        ezca['LSC-SRCL_GAIN'] = 1.5 #AE 20230109 reduce SRCL gain, too high on 3f but ok on refl 1f
        time.sleep(2)
        
        # freeze SRM ASC, rdr 20160822 seems to screw up as soon as we move arms towards resonance, its turned on again later
        ezca.switch('ASC-SRC2_P','INPUT','OFF')
        ezca.switch('ASC-SRC2_Y','INPUT','OFF')
        #time.sleep(0.1)
        # reset LSC trigger for DRMI and carm offset reduction stage AE 20200303
        ezca['LSC-IFO_TRIG_THRESH_ON'] = 1.2
        ezca['LSC-IFO_TRIG_THRESH_OFF'] = 0.5

    @assert_mc_lock
    @assert_als_y_lock
    @assert_drmi_lock
    @nodes.checker()
    def run(self):
        if any(matrix.lsc_input.is_ramping()):
            return
        return True

##################################################
class COMM_SLEW(GuardState):
    index = 560
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):

        # Set als guardians back to PDH LOCKED_IDLE, turns off alignment servos and
        # offloads suspensions, and ensures that if ALS drops lock, it won't try to
        # scan again.
        nodes_loose['ALS_XARM'] = 'PDH_LOCKED_IDLE'
        nodes_loose['ALS_YARM'] = 'PDH_LOCKED_IDLE'

        # zero qpd offsets
        xtrb = cdsutils.avg(2,'LSC-TR_X_QPD_B_SUM_INMON')
        ytrb = cdsutils.avg(2,'LSC-TR_Y_QPD_B_SUM_INMON')
        #xtrb = ezavg(ezca,2.0,'LSC-TR_X_QPD_B_SUM_INMON')
        #ytrb = ezavg(ezca,2.0,'LSC-TR_Y_QPD_B_SUM_INMON')
        time.sleep(0.5)
        ezca['LSC-TR_X_QPD_B_SUM_OFFSET'] = -xtrb
        ezca['LSC-TR_Y_QPD_B_SUM_OFFSET'] = -ytrb
        # Reduce COMM offset
        ezca['ALS-C_COMM_PLL_CTRL_TRAMP'] = 2
        ezca.switch('ALS-C_COMM_PLL_CTRL', 'FM10', 'OFF')
        comm_now = ezca['ALS-C_COMM_PLL_CTRL_OFFSET']
        ezca['ALS-C_COMM_PLL_CTRL_OFFSET'] = comm_now - 1423
        self.timer['wait'] = 6
        
        #5-19-22 TJO        
        #Saves SRM controls to text file to add to SRM p and y offset values if lock is lost on inded 400
        SRMfilepath = '/opt/rtcds/userapps/trunk/sus/l1/scripts/pass_SRM_controls_DO_NOT_EDIT.txt'        
        stCalP = 1.875
        stCalY = 2.681
        SRMfile = open(SRMfilepath,'w')
        SRMfile.truncate(0)
        SRMfile.write('DO NOT EDIT THIS FILE (USED in ISC_LOCK see state 400 and 560)' + '\n')
        SRMfile.write('Add to SRM P: ' + str(ezca['SUS-SRM_M1_LOCK_P_OUT16'] / stCalP) + '\n')
        SRMfile.write('Add to SRM Y: ' + str(ezca['SUS-SRM_M1_LOCK_Y_OUT16'] / stCalY) + '\n')
        SRMfile.write('Current P:    ' + str(ezca['SUS-SRM_M1_OPTICALIGN_P_OFFSET']) + '\n')
        SRMfile.write('Current Y:    ' + str(ezca['SUS-SRM_M1_OPTICALIGN_Y_OFFSET']) + '\n')
        SRMfile.close()

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):

        if not nodes_loose['ALS_XARM'].arrived: 
            notify('Waiting on ALS X guardian')
            return
        elif not nodes_loose['ALS_YARM'].arrived:
            notify('Waiting on ALS Y guardian')
            return

        return self.timer['wait']

##################################################
# Transition CARM to the transmited power signals
class MOVE_CARM_TR(GuardState):
    index = 580
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):
        ezca['LSC-MCL_TRAMP'] = 5
        ezca['LSC-XARM_TRAMP'] = 3

        # cross-ramp MCL and XARM gains
        # gain = -24 for IMC input power = .75W, -17 for 1.5W
        ezca['LSC-MCL_GAIN'] = -24
        time.sleep(1)
        ezca.switch('LSC-MCL','FM6','ON')
        time.sleep(1)
        ezca.switch('LSC-MCL','FM8','ON')
        ezca.switch('LSC-XARM','FM6','OFF')
        time.sleep(2)

        # Turn off XARM
        ezca.switch('LSC-XARM','FM9','OFF')
        ezca['LSC-XARM_GAIN'] = 0
        time.sleep(1)

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):
        return True

##################################################    
# CARM is now on transmission signals        
class CARM_ON_TR(GuardState):
    index = 600
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):
        ezca['LSC-TR_CARM_TRAMP'] = 1
        return True

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):
        return True

##################################################
# More CARM offset reduction
class CARM_100PM(GuardState):
    index = 650
    request = False

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def main(self):
        #TODO: 99% sure this section of code is obsolete, but will test before deleting it completely ajm170317
        #########################################################
        #matrix.lsc_pow_norm['YARM','TRY_A_LF'] = 1
        matrix.lsc_input.TRAMP = 0
        matrix.lsc_input.zero(row='YARM')
        #matrix.lsc_input['YARM','ASAIR_A_RF45_Q'] = 1
        #matrix.lsc_input.load()
        #########################################################

        #cstep_ramp = 3     #ajm20191112 radiation pressure kick is violent
        cstep_ramp = 5
        ezca['LSC-TR_CARM_TRAMP'] = cstep_ramp
        time.sleep(0.2)

        self.timer['cstep'] = cstep_ramp
        ezca['LSC-TR_CARM_OFFSET'] = ifoconfig.carm_offset_asq

        #TODO: set up and test diff servo
        #self.diff_servo = cdsutils.Servo(ezca, ALS-C_DIFF_PLL_CTRL_OFFSET, readback='LSC-ASVAC_A_RF45_Q_MON',gain=???)

    @assert_mc_lock
    @assert_als_y_lock
    @nodes.checker()
    def run(self):

        # self.diff_servo.step()

        if not self.timer['cstep']:
            return

        #if not ezca['LSC-ASVAC_A_RF45_Q_MON'] < 1:
        #    return

        return True

##################################################
# Start using AS45Q as the error signal for DARM
class MOVE_DARM_ASQ(GuardState):
    index = 670
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        ezca.switch('LSC-L3_LOCK','FM1','OFF')
        time.sleep(1)

        as45q = abs(ezca['LSC-ASVAC_A_RF45_Q_MON'])

        if as45q<=1000:
            tramp = 1
        elif 1000<as45q<=3000:
            tramp = 2
        elif 3000<as45q<=5000:
            tramp = 3
        else:
            tramp = 5

        #tramp = 1

        matrix.lsc_input_arm['DARM','ASVAC_A_RF45_Q'] = -0.1
        matrix.lsc_input_arm['DARM','ALS_DIFF'] = 0
        matrix.lsc_input_arm.TRAMP = tramp# was 1
        time.sleep(0.1)
        matrix.lsc_input_arm.load()
        time.sleep(tramp) # was 1

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if any(matrix.lsc_input.is_ramping()):
            return
        if any(matrix.lsc_input_arm.is_ramping()):
            return

        # setup power normalization
        ezca['LSC-POW_NORM_TRX_SQRT_ENABLE'] = 1
        matrix.lsc_pow_norm['DARM','TRX_A_LF'] = 0.0786 # 0.0786 for 18 counts tr carm, 0.0707 for 20
        # turn off notches
        ezca.switch('LSC-DARM','FM2','FM6','FM8','OFF','FM1','ON')  #AJM180130 Turn off 200Hz LPF (FM8) to gain more phase

        return True

####################################################
def turn_off_dc_oplevs():
    for sus in ['ETM','ITM']:
        for arm in ['X','Y']:
            for dof in ['P','Y']:
                ezca.switch("SUS-{}{}_L2_OLDAMP_{}".format(sus,arm,dof),"INPUT","OFFSET","OFF")

#class ASC_CARM_OFFSET_REDUCTION(GuardState):
class TURN_ON_DHARD(GuardState):
    index = 690
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # AJM190619 go a little further up the fringe before engaging dhard
        ezca['LSC-TR_CARM_OFFSET'] = -30
        time.sleep(5)

        #---------------------------------------------------------------------------------
        ### Vertex ASC ###

        # freeze DRMI ASC faster to help with CARM offset wiggles (test AE 20181010)
        ezca.switch('SUS-PR2_M1_LOCK_P','INPUT','OFF')
        ezca.switch('SUS-PR2_M1_LOCK_Y','INPUT','OFF')

        # Test to see if turning SRC1 loops off makes CARM offset reduction easier AJM210125
        #ezca.switch('SUS-SR2_M1_LOCK_P','INPUT','OFF')
        #ezca.switch('SUS-SR2_M1_LOCK_Y','INPUT','OFF')

        # disengage PRC2 ASC loops
        ezca.switch('ASC-PRC2_P','INPUT','OFF') #AJM190625 uncommented
        ezca.switch('ASC-PRC2_Y','INPUT','OFF') #AJM190625 uncommented

        time.sleep(0.3)

        #Transition input matrix for PRC2 DOF
        set_asc_pr2_mtrx()

        # If using POPX turn PRC2 loops back on
        if ifoconfig.popx:

            ezca['ASC-PRC2_P_PRE_TRAMP'] = 0
            ezca['ASC-PRC2_Y_PRE_TRAMP'] = 0
            time.sleep(0.3)

            ezca['ASC-PRC2_P_PRE_GAIN'] = 1.0
            ezca['ASC-PRC2_Y_PRE_GAIN'] = 0.5
            time.sleep(0.3)

            # Engage PR2 servos (AJM put back in 20240216)
            ezca.switch('ASC-PRC2_P','INPUT','ON')
            ezca.switch('ASC-PRC2_Y','INPUT','ON')
            ezca.switch('ASC-PRC2_Y','OFFSET','ON') #CB Getting rid of offsets helps through in high useism
            #ezca['ASC-PRC2_Y_TRAMP'] = 5
            #ezca['ASC-PRC2_Y_OFFSET'] = -1200        # This was the offset 17 Feb 2024
            time.sleep(0.3)

        # Turn off INP2 and SRC2 loops
        ezca.switch('ASC-INP2_P','INPUT','OFF')
        ezca.switch('ASC-INP2_Y','INPUT','OFF')
        ezca.switch('ASC-SRC2_P','INPUT','OFF')
        ezca.switch('ASC-SRC2_Y','INPUT','OFF')

        # Test IM4 ADS
        if ifoconfig.dhardIm4Ads:
            bank = '6'
            im4_freq = {'PIT':13.15,'YAW':13.65}
            for dof in ['PIT','YAW']:
                turn_on_dither('IM4',dof,'OSC'+bank,im4_freq[dof],0.3,sin_gain=1,cos_gain=1,tramp=3)
            matrix.asc_ads_input_pit.zero(row='PIT{}'.format(bank))
            matrix.asc_ads_input_yaw.zero(row='YAW{}'.format(bank))
            time.sleep(0.1)
            #TODO restart guardian to import matrices file
            #matrix.asc_ads_input_pit['PIT{}'.format(bank),'REFL_A_LF'] = 1
            #matrix.asc_ads_input_yaw['YAW{}'.format(bank),'REFL_A_LF'] = 1
            ezca['ASC-ADS_PIT_SEN_MTRX_6_7'] = 1
            ezca['ASC-ADS_YAW_SEN_MTRX_6_7'] = 1
            matrix.asc_ads_output_pit['IM4','PIT{}'.format(bank)] = 1
            matrix.asc_ads_output_yaw['IM4','YAW{}'.format(bank)] = 1

            ezca.switch('ASC-ADS_PIT6_DOF','FM1','FM7','OUTPUT','ON')
            ezca.switch('ASC-ADS_YAW6_DOF','FM1','FM7','OUTPUT','ON')
            ezca.switch('ASC-ADS_PIT6_DEMOD_SIG','FM2','ON','FM6','OFF')
            ezca.switch('ASC-ADS_YAW6_DEMOD_SIG','FM2','ON','FM6','OFF')
            time.sleep(0.1)

            ezca['ASC-ADS_PIT6_DOF_GAIN'] = 2
            ezca['ASC-ADS_YAW6_DOF_GAIN'] = 2
            ezca['ASC-ADS_GAIN'] = 3
            ezca.switch('ASC-ADS_PIT6_DOF','INPUT','ON')
            ezca.switch('ASC-ADS_YAW6_DOF','INPUT','ON')

        # Test to see if turning SRC1 loops off makes CARM offset reduction easier AJM210125
        #ezca.switch('ASC-SRC1_P','INPUT','OFF')
        #ezca.switch('ASC-SRC1_Y','INPUT','OFF')

        # Transition input matrix for INP2
        set_asc_inp_mtrx()

        #---------------------------------------------------------------------------------
        ### DHARD ###

        # Turn off any dc oplev to TM loops that are running
        #turn_off_dc_oplevs()

        # set up initial gains with loops off
        # ramp up input gains 
        ezca['ASC-DHARD_P_PRE_TRAMP'] = 0
        ezca['ASC-DHARD_Y_PRE_TRAMP'] = 0
        time.sleep(0.2)

        ezca['ASC-DHARD_P_PRE_GAIN'] = 0.1 # reduced from 2.0 - signals are likey garbage at this point Dec 18, 2019
        ezca['ASC-DHARD_Y_PRE_GAIN'] = 0.1 # same as above 

        ezca['ASC-DHARD_P_TRAMP'] = 0
        ezca['ASC-DHARD_Y_TRAMP'] = 0
        time.sleep(0.2)

        ezca['ASC-DHARD_P_GAIN'] = -2.5
        ezca['ASC-DHARD_Y_GAIN'] = -2.7 

        ezca['ASC-DHARD_P_TRAMP'] = 3
        ezca['ASC-DHARD_Y_TRAMP'] = 3

        # switch the loops ON
        ezca.switch('ASC-DHARD_P','INPUT','ON')
        ezca.switch('ASC-DHARD_Y','INPUT','ON')
        time.sleep(3)
        #---------------------------------------------------------------------------------

        # push up the CARM fringe a little to improve length and angle optical gains
        ezca['LSC-TR_CARM_TRAMP']  = 6
        time.sleep(0.2)

        ezca['LSC-TR_CARM_OFFSET'] = ifoconfig.carm_offset_dhard

        # let arm alignment settle
        self.timer['fringepush'] = 6 # AP used to be 8


    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if self.timer['fringepush']:
            return True

##################################################
# checkpoint to verify arm alignment is good. 
class DARM_ON_ASQ(GuardState):
    index = 700
    request = False

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        return True


##################################################
'''
class TURN_OFF_DRMI_ASC(GuardState):
    index = 720
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # FIXME: Currently this is whole state happens in TURN ON DHARD

        #ezca.switch('SUS-PR2_M1_LOCK_P','INPUT','OFF')
        #ezca.switch('SUS-PR2_M1_LOCK_Y','INPUT','OFF')

        # AJM if leaving on PRC2, need to switch the sensor to POP X 36
        # disengage loops
        #ezca.switch('ASC-PRC2_P','INPUT','OFF')
        #ezca.switch('ASC-PRC2_Y','INPUT','OFF')

        #time.sleep(0.3)

        #TODO: transition to popx before turning off other loops
        #set_asc_pr2_mtrx()

        #if ifoconfig.popx:

        #    ezca['ASC-PRC2_P_PRE_TRAMP'] = 0
        #    ezca['ASC-PRC2_Y_PRE_TRAMP'] = 0
        #    time.sleep(0.3)

        #    ezca['ASC-PRC2_P_PRE_GAIN'] = 1.0
        #    ezca['ASC-PRC2_Y_PRE_GAIN'] = 0.5
        #    time.sleep(0.3)

        #    # Engage PR2 servos
        #    ezca.switch('ASC-PRC2_P','INPUT','ON')
        #    ezca.switch('ASC-PRC2_Y','INPUT','ON')


        ezca.switch('ASC-INP2_P','INPUT','OFF')
        ezca.switch('ASC-INP2_Y','INPUT','OFF')
        ezca.switch('ASC-SRC2_P','INPUT','OFF')
        ezca.switch('ASC-SRC2_Y','INPUT','OFF')

        # Test to see if turning SRC1 loops off makes CARM offset reduction easier AJM210125
        #ezca.switch('ASC-SRC1_P','INPUT','OFF')
        #ezca.switch('ASC-SRC1_Y','INPUT','OFF')

        time.sleep(0.3)

        #switch PRC2 sensor to POP X 36 and turn back on (ajm 181023)
        #matrix.asc_input_pit.zero(row='PRC2')
        #matrix.asc_input_yaw.zero(row='PRC2')
        #matrix.asc_input_pit['PRC2','POP_X_RF36_I'] = 36
        #matrix.asc_input_yaw['PRC2','POP_X_RF36_I'] = -120
        #ezca.switch('ASC-PRC2_P','INPUT','ON')
        #ezca.switch('ASC-PRC2_Y','INPUT','ON')

        #Set ASC INP2 final matrix (defined at the start)
        set_asc_inp_mtrx()


    @assert_mc_lock
    @nodes.checker()
    def run(self):
        return True
'''
##################################################
class CARM_10PM(GuardState):
    index   = 750
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        offset_ramp = 6
        ezca['LSC-TR_CARM_TRAMP'] = offset_ramp
        time.sleep(0.3)
        ezca['LSC-TR_CARM_OFFSET'] = ifoconfig.carm_offset_refl9 
        
        # decrease DHARD WFS gain since they gain it in optical response
        # TODO: Maybe do this in ZERO CARM OFFSET?
        ezca['ASC-DHARD_P_PRE_TRAMP'] = 2
        ezca['ASC-DHARD_Y_PRE_TRAMP'] = 2
        ezca['ASC-DHARD_P_PRE_GAIN'] = 0.1 
        ezca['ASC-DHARD_Y_PRE_GAIN'] = 0.1
        self.timer['wait'] = offset_ramp

    @assert_mc_lock
    @nodes.checker()

    def run(self):
        if not self.timer['wait']:
            return
        notify("CARM at 10pm off resonance")
        return True

##################################################
class MOVE_CARM_REFL9(GuardState):
    index   = 780
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        # lock CARM to REFL 9I (with an offset)
        ezca['LSC-TR_REFL9_TRAMP']  = 0.1
        time.sleep(0.2)
        ezca['LSC-TR_REFL9_OFFSET'] = 0.4
        ezca['LSC-TR_REFL9_TRAMP']  = 1
        time.sleep(0.2)
 
 
        # matrix ramp TR_CARM -> CARM (1->0) and TR_REFL9 -> CARM (0->1)
        matrix.lsc_input['MCL','TR_CARM']  = 0.5
        matrix.lsc_input['MCL','TR_REFL9'] = 1
        matrix.lsc_input.TRAMP = 1
        time.sleep(0.1)
        matrix.lsc_input.load()
        time.sleep(1)

        #self.timer['wait'] = 1

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        #if not self.timer['wait']:
        #    return
        #return not any(matrix.lsc_input.is_ramping())
	    return True

##################################################
# Remove the offset in the TR_REFL9 bank, this is "full lock"
class CARM_ZERO_OFFSET(GuardState):
    index = 800
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        ezca['LSC-TR_CARM_TRAMP'] = 1
        matrix.lsc_input['MCL','TR_CARM']  = 0
        time.sleep(0.2)
        ezca['LSC-TR_CARM_OFFSET'] = ifoconfig.carm_offset_nudge2zero 
        #ezca['LSC-TR_REFL9_TRAMP'] = 1
        #time.sleep(0.5)
        ezca.switch('LSC-TR_REFL9','OFFSET','OFF')
        self.timer['zero offset'] = 1
        #matrix.lsc_input['MCL','TR_REFL9'] = 1
        #time.sleep(0.1)
        matrix.lsc_input.load()

        # DHARD
        ezca["ASC-DHARD_P_PRE_GAIN"] = 0.5  #0.4
        ezca["ASC-DHARD_Y_PRE_GAIN"] = 0.3  #0.4

        # Boost DHARD
        ezca.switch('ASC-DHARD_P','FM2','ON')
        ezca.switch('ASC-DHARD_Y','FM2','ON')

        self.asport_protect = True

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['zero offset']:
            return

        # AS port protection, we have IR resonance
        if self.asport_protect:
            ezca['SYS-PROTECTION_AS_XARM'] = 1
            ezca['SYS-PROTECTION_AS_YARM'] = 1
            self.asport_protect = False

        notify("CARM offset now at zero")

        return True

##################################################
# Engage the common mode servo
class CARM_SERVO_AO(GuardState):
    index = 810
    request = False

    @assert_mc_lock
    def main(self):
        # Turn off MCL boost
        #ezca.switch('LSC-MCL','FM10','OFF')
        #time.sleep(3)        
        # Turn CM gain and compensation off
        ezca['LSC-REFL_SERVO_COMCOMP'] = 0
        ezca['LSC-REFL_SERVO_COMBOOST'] = 0
        # -25 without TCS
        ezca['LSC-REFL_SERVO_IN1GAIN'] = -25    #-31 #AE20230109 added 6dB because REFL reduced by 2 (alog 62255)
        ezca['LSC-REFL_SERVO_FASTGAIN'] = -26 # -32 for 4W

        # Turn down the AO gain, switch it on, then turn it back up
        ezca['IMC-REFL_SERVO_IN2GAIN'] = -32
        time.sleep(0.3)
        ezca['IMC-REFL_SERVO_IN2EN'] = 1
        self.step = cdsutils.Step(ezca,'IMC-REFL_SERVO_IN2GAIN','+2,13',time_step=0.2)

        self.timer['wait'] = 1
        self.wait = True

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return

        if not self.step.step():
            return

        if self.wait:
            self.timer['wait'] = 1
            self.wait = False
            return

        return True

##################################################
class CARM_SERVO_SLOW(GuardState):
    index = 811
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        matrix.lsc_input['MCL','TR_REFL9'] = 0
        matrix.lsc_input['MCL','REFLSERVO_SLOW'] = 0.17 
        matrix.lsc_input.TRAMP = 1
        time.sleep(0.1)
        matrix.lsc_input.load()

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if any(matrix.lsc_input.is_ramping()):
            return
        return True


##################################################

class CARM_SERVO_FAST(GuardState):
    index = 812
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        ezca['LSC-MCL_TRAMP'] = 0.2

        ### Prepare CM servo stepping commands:
        # Oth step brings the fast gain to the right cross-over point
        self.cmstep0 = cdsutils.Step(ezca,'LSC-REFL_SERVO_FASTGAIN','+1,12',time_step=0.2)
        # 1st step increases the CM gain by 8dB. Forget why MCL step is 0.9dB.
        self.cmsteps1 = [
            cdsutils.Step(ezca,'LSC-MCL_GAIN','+0.9dB,8',time_step=0.2),            
            cdsutils.Step(ezca,'LSC-REFL_SERVO_FASTGAIN','+1,8',time_step=0.2),
            ]
        # 2nd step increases the CM gain by another 12dB.
        self.cmsteps2 = [
            cdsutils.Step(ezca,'LSC-MCL_GAIN','+0.9dB,12',time_step=0.2),
            cdsutils.Step(ezca,'LSC-REFL_SERVO_FASTGAIN','+1,12',time_step=0.2),
            ]

        self.turnoffslowfm1 = True

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        # Oth stepping (see description above)        
        if not self.cmstep0.step():
            return

        time.sleep(0.5)

        # 1st stepping
        ret = True
        for step in self.cmsteps1:
            ret &= step.step()
        if not ret:
            return

        if self.turnoffslowfm1:
            ezca.switch('LSC-REFL_SERVO_SLOW','FM1','OFF')
            self.turnoffslowfm1 = False

        # 2nd stepping
        ret = True
        for step in self.cmsteps2:
            ret &= step.step()
        if not ret:
            return

        return True

##################################################
class CARM_SERVO_BOOST(GuardState):
    index = 816
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        self.step = cdsutils.Step(ezca,'LSC-REFL_SERVO_IN1GAIN','+1,21',time_step=0.2) 

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if not self.step.step():
            return

        # BOOST! (Actually, the compensation filter)
        ezca['LSC-REFL_SERVO_COMCOMP'] = 1
        return True

##################################################
class TURN_ON_ARM_WFS(GuardState):
    index = 820
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # turn on the CHARD !
        ezca['ASC-CHARD_P_TRAMP'] = 0
        ezca['ASC-CHARD_Y_TRAMP'] = 0
        time.sleep(0.2)
        # set the gains
        ezca['ASC-CHARD_P_GAIN'] = -0.75 
        ezca['ASC-CHARD_Y_GAIN'] = -0.60   

        ezca['ASC-CHARD_P_PRE_TRAMP'] = 5
        ezca['ASC-CHARD_Y_PRE_TRAMP'] = 5
 
        # inputs ON
        ezca.switch('ASC-CHARD_P','INPUT','ON')
        ezca.switch('ASC-CHARD_Y','INPUT','ON')

        self.timer["wait"] = 1
        self.chard_preG = True
        self.dhard_resg = True
#        self.chard_boost = True

        if self.dhard_resg:
            ezca.switch('ASC-DHARD_P','FM3','ON')
            ezca.switch('ASC-DHARD_Y','FM3','ON')
            self.timer["wait"] = 1
            self.dhard_resg = False
            return

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            return

        # Ramp up input gains
        if self.chard_preG:
            ezca['ASC-CHARD_P_PRE_GAIN'] = 0.4  #0.7    #AJM20230117 lowered because re-phased REFL 9
            ezca['ASC-CHARD_Y_PRE_GAIN'] = 0.4  #0.7
            self.timer["wait"] = 5
            self.chard_preG = False
            return

        return True




##################################################
# Transition DRMI to 1F sensing
class DRMI_TO_1F(GuardState):
    index = 830
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        ezca['LSC-PRCL_GAIN'] = 1.0   # 20171029: UGF measured 
        matrix.lsc_input['PRCL','REFLAIR_B_RF27_I'] = 0
        matrix.lsc_input['PRCL','POP_A_RF9_I'] = 16
        ezca['LSC-POP_A_RF45_PHASE_R'] = 97

        ezca['LSC-MICH_GAIN'] = -0.06 
        matrix.lsc_input['MICH','REFLAIR_B_RF135_Q'] = 0
        matrix.lsc_input['MICH','POP_A_RF45_Q'] = 28
        
        ezca['LSC-SRCL_TRAMP'] = 3
        ezca['LSC-SRCL_GAIN'] = 0.75
        matrix.lsc_input['SRCL','REFLAIR_B_RF135_I'] = 0
        matrix.lsc_input['SRCL','REFLAIR_B_RF27_I'] = 0
        matrix.lsc_input['SRCL','POP_A_RF45_I'] = 25
        matrix.lsc_input['SRCL','POP_A_RF9_I'] = 1 #4 AE 241127 #0.8 AE 230623 #1.4 AE 220513 #-1.6 AE 211130 

        matrix.lsc_input.TRAMP = 1
        time.sleep(0.1)
        matrix.lsc_input.load()

    # clear corner integrators for OAF CAL
        ezca['OAF-CAL_SUM_IMC_M1_RSET'] = 2
        ezca['OAF-CAL_SUM_IMC_M2_RSET'] = 2
        ezca['OAF-CAL_SUM_MICH_BS_M2_RSET'] = 2
        ezca['OAF-CAL_SUM_PRCL_PRM_M2_RSET'] = 2
        ezca['OAF-CAL_SUM_SRCL_SRM_M2_RSET'] = 2
        ezca['OAF-CAL_SUM_SRCL_SRM_M1_RSET'] = 2

        # Some of these channel
        ezca['CAL-CS_SUM_IMC_CTRL_M1_RSET'] = 2
        ezca['CAL-CS_SUM_IMC_CTRL_M2_RSET'] = 2
        ezca['CAL-CS_SUM_MICH_BS_M2_RSET'] = 2
        ezca['CAL-CS_SUM_PRCL_PRM_M2_RSET'] = 2
        #ezca['CAL-CS_SUM_PRCL_PR2_M2_RSET'] = 2
        ezca['CAL-CS_SUM_SRCL_SRM_M2_RSET'] = 2
        ezca['CAL-CS_SUM_SRCL_SRM_M1_RSET'] = 2
        #ezca['CAL-CS_SUM_SRCL_SR2_M2_RSET'] = 2
        #ezca['CAL-CS_SUM_SRCL_SR2_M1_RSET'] = 2



    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if any(matrix.lsc_input.is_ramping()):
            return
        return True

##################################################
class TURN_ON_VERTEX_WFS(GuardState):
    index = 840
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # Part of test (see ) AJM210125
        #ezca.switch('ASC-SRC1_P','INPUT','ON')
        #ezca.switch('ASC-SRC1_Y','INPUT','ON')      
        #ezca.switch('SUS-SR2_M1_LOCK_P','INPUT','ON')
        #ezca.switch('SUS-SR2_M1_LOCK_Y','INPUT','ON')


        #ezca.switch('ASC-PRC2_P','INPUT','OFF')
        #ezca.switch('ASC-PRC2_Y','INPUT','OFF')

        if ifoconfig.popx:
            ezca['ASC-PRC2_P_PRE_GAIN'] = 0.3 # 1 AE20240220 as measured at 2W
            ezca['ASC-PRC2_Y_PRE_GAIN'] = 0.35 # 0.5 AE20240220 
        else:
            ezca['ASC-PRC2_P_PRE_GAIN'] = 1 #0.2 AE20230125 rescaled new matrix   #0.35 #AJM20230117 lowered because we rephased REFL9 WFS
            ezca['ASC-PRC2_Y_PRE_GAIN'] = 1 #0.2   #0.35

        ezca['ASC-INP2_P_PRE_GAIN'] = 1.3   #2.5    #AJM20230117 lowered because we rephased REFL9 WFS
        ezca['ASC-INP2_Y_PRE_GAIN'] = 1.3   #2.5


        #Set ASC inp matrix for full IFO (defined at the start)
        #set_asc_inp_mtrx()
        # Matrix now being set in TURN_OFF_DRMI_ASC state


        # AE new gains, but don't work through DRMI locking (160902 or 160209?)
        #ezca['ASC-PRC2_P_GAIN'] = -0.1     
        #ezca['ASC-PRC2_Y_GAIN'] = -0.1
        #ezca['ASC-INP2_P_GAIN'] =  0.2
        #ezca['ASC-INP2_Y_GAIN'] = -0.1
        ezca['ASC-MICH_P_GAIN'] =  0.05 
        ezca['ASC-MICH_Y_GAIN'] =  0.05 
        #ezca['ASC-SRC1_P_TRAMP'] = 20 
        #ezca['ASC-SRC1_P_GAIN'] = 4000 
        ezca['ASC-SRC1_P_GAIN'] = 3000  
        ezca['ASC-SRC1_Y_GAIN'] = 4000
        time.sleep(0.1)

        ezca['ASC-MICH_Y_PRE_TRAMP'] = 3
        time.sleep(0.1)
        ezca['ASC-MICH_Y_PRE_GAIN'] = 1

        # Increase the gains to match O2 and refl power 30% loss # MFK 180208
        # no longer do PRC2 since we switched to REFL WFS AE 20181011
        ezca['ASC-PRC2_P_PRE_TRAMP'] = 5
        ezca['ASC-PRC2_Y_PRE_TRAMP'] = 5
        ezca['ASC-INP2_P_PRE_TRAMP'] = 5
        ezca['ASC-INP2_Y_PRE_TRAMP'] = 5
        
        # turn off DC centering for POP     
        #ezca.switch('ASC-DC5_P','INPUT','OFF')
        #ezca.switch('ASC-DC5_Y','INPUT','OFF')

        # Engage INP2 servo
        ezca.switch('ASC-INP2_P','INPUT','ON')
        ezca.switch('ASC-INP2_Y','INPUT','ON')

# -------------------------------------------------
        # Engage PR2 servos
        ezca.switch('ASC-PRC2_P','INPUT','ON')
        ezca.switch('ASC-PRC2_Y','INPUT','ON')

        # Turn on M1 integrators
        ezca.switch('SUS-PR2_M1_LOCK_P','INPUT','ON')
        ezca.switch('SUS-PR2_M1_LOCK_Y','INPUT','ON')
# -------------------------------------------------
        ezca['ASC-PRC2_Y_OFFSET'] = 0        # zero offset from turn on DHARD


#        # PRM DC servo (leave commented out )
#        #ezca['ASC-PRC1_P_OFFSET'] = 0.01
#        #ezca['ASC-PRC1_Y_OFFSET'] = -0.18
#        #ezca.switch('ASC-PRC1_P','INPUT','OFFSET','ON')
#        #ezca.switch('ASC-PRC1_Y','INPUT','OFFSET','ON')
        
        #matrix.lsc_input_arm['DARM','ASVAC_A_RF45_Q'] = -0.3 #AE18 bring back up
        #matrix.lsc_input_arm.load()

        self.chard_boost = True

        # CHARD Boosts
        if self.chard_boost:
            ezca.switch('ASC-CHARD_P','FM3','ON') 
            ezca.switch('ASC-CHARD_Y','FM3','ON')
            ezca.switch('ASC-CHARD_P','FM2','ON')
            ezca.switch('ASC-CHARD_Y','FM2','ON')
            self.timer["wait"] = 1
            self.chard_boost = False
  

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        return True

'''
class SPLIT_DARM_DRIVE_AT_RF(GuardState):
    index = 845
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # Start with ETMY L2 Lock Gain set to zero
        ezca["SUS-ETMY_L2_LOCK_L_TRAMP"] = 0
        time.sleep(0.1)
        ezca["SUS-ETMY_L2_LOCK_L_GAIN"] = 0
        time.sleep(0.3)
        for stage in ["L2","L1","M0"]:
            ezca["SUS-ETMY_{}_LOCK_L_RSET".format(stage)] = 2
        time.sleep(0.3)

        ezca["SUS-ETMY_M0_LOCK_L_TRAMP"] = 0
        time.sleep(0.2)
        ezca["SUS-ETMY_L1_LOCK_OUTSW_L"] = 1
        ezca.switch("SUS-ETMY_L1_LOCK_L","FM2","FM3","FM6","FM7","FM8","ON")
        ezca["SUS-ETMY_M0_LOCK_L_GAIN"] = 0.4
        time.sleep(1)


        t_ramp = 30
        # Set tramps
        for sus in ["ETMX","ETMY"]:
            ezca["SUS-{}_L2_LOCK_L_TRAMP".format(sus)] = t_ramp
        time.sleep(0.3)

        # Split the drive
        for sus in ["ETMX","ETMY"]:
            ezca["SUS-{}_L2_LOCK_L_GAIN".format(sus)] = 0.5

        self.timer["wait"] = t_ramp

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            notify("Splitting DARM drive")
            return

        return True
'''

##################################################
class TURN_ON_TIDAL_OFFLOAD(GuardState):
    index = 850
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        log("Darm act is {}".format(ifoconfig.darm_actuator))

        # Make sure that IMC-F path is open
        ezca['IMC-F_TIDAL_SW'] = 1

        if "ETMX" in ifoconfig.darm_actuators:
            comm_tidal = "Y"
            diff_tidal = "X"
        else:
            comm_tidal = "X"
            diff_tidal = "Y"
        #TODO: replace below with above and test.
        #if ifoconfig.darm_actuator == 'ETMX':
        #    comm_tidal = 'Y'
        #if ifoconfig.darm_actuator == 'ETMY': 
        #    comm_tidal = 'X'
        #if ifoconfig.darm_actuator == 'BOTH': #BOTHACTUATORS 20190107
        #    comm_tidal = 'Y'

        # Make sure that the correct filters are on
        ezca.switch('SUS-ETM'+comm_tidal+'_M0_TIDAL_L','FM1','FM2','ON')
        time.sleep(0.3)

##### Commented out for refcav bypass AJM20190205
        # Tidal control goes to ETM M0
        ezca['SUS-ETM'+comm_tidal+'_M0_TIDAL_L_TRAMP'] = 60
        ezca['SUS-ETM'+comm_tidal+'_M0_TIDAL_L_RSET'] = 2
        time.sleep(0.3)

        # Servo gain
        ezca['SUS-ETM'+comm_tidal+'_M0_TIDAL_L_GAIN'] = 1
        ezca.switch('SUS-ETM'+comm_tidal+'_M0_TIDAL_L','INPUT','OUTPUT','ON')
        #AP20190319 changed for more microseism tidal test (Note: currently only set up for ETMY):
        #ezca['SUS-ETM'+comm_tidal+'_M0_TIDAL_L_GAIN'] = 2
        #ezca.switch('SUS-ETM'+comm_tidal+'_M0_TIDAL_L','FM1','FM4','FM5','FM10','ON','FM2','OFF')
        #time.sleep(3)
        #ezca.switch('SUS-ETM'+comm_tidal+'_M0_TIDAL_L','INPUT','OUTPUT','ON')

#####

        time.sleep(1)

        #AJM20240509 for test
        nodes["TIDAL_{}".format(comm_tidal)] = "HPI_SERVO_ON_CTIDAL"
        nodes["TIDAL_{}".format(diff_tidal)] = "HPI_SERVO_ON_DTIDAL"
        if "ETMX" in ifoconfig.darm_actuators and "ETMY" in ifoconfig.darm_actuators:
            ezca['HPI-ETMX_ISCINF_LONG_GAIN'] = 2
        

        #if ifoconfig.darm_actuator == 'ETMX':
        #    nodes['TIDAL_X'] = 'HPI_SERVO_ON_DTIDAL'
        #    nodes['TIDAL_Y'] = 'HPI_SERVO_ON_CTIDAL'   #refcav bypass AJM20190205

        #if ifoconfig.darm_actuator == 'ETMY':
        #    nodes['TIDAL_X'] = 'HPI_SERVO_ON_CTIDAL'   #refcav bypass AJM20190205
        #    nodes['TIDAL_Y'] = 'HPI_SERVO_ON_DTIDAL'

        #if ifoconfig.darm_actuator == 'BOTH':  # Make both like X CB #BOTHACTUATORS 20190107
        #    nodes['TIDAL_X'] = 'HPI_SERVO_ON_DTIDAL'
        #    nodes['TIDAL_Y'] = 'HPI_SERVO_ON_CTIDAL'   #refcav bypass AJM20190205
        #    ezca['HPI-ETMX_ISCINF_LONG_GAIN'] = 2

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        return True

##################################################
class TURN_ON_BS_STAGE2(GuardState):
    index = 855
    request = False

    @assert_mc_lock
    @assert_drmi_lock
    @nodes.checker()
    def main(self):
        nodes_loose['SEI_BS'] = 'FULLY_ISOLATED'

    def run(self):
#        return nodes_loose['SEI_BS'].arrived % not sure we need to wait until this is done before moving on in the script. AP December 09th 2016
        return True
##################################################
class SET_CALIBRATION(GuardState):
    index = 860
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # clear integrators for OAF CAL
        ezca['OAF-CAL_SUM_DARM_L1_RSET'] = 2
        ezca['OAF-CAL_SUM_YARM_L1_RSET'] = 2
        ezca['OAF-CAL_SUM_CARM_M2_RSET'] = 2
        ezca['OAF-CAL_SUM_CARM_M1_RSET'] = 2
        ezca['OAF-CAL_SUM_DARM_ERR_RSET'] = 2
        ezca['OAF-CAL_SUM_PRCL_ERR_RSET'] = 2
        ezca['OAF-CAL_SUM_SRCL_ERR_RSET'] = 2
        ezca['OAF-CAL_SUM_MICH_ERR_RSET'] = 2
        ezca['OAF-CAL_SUM_CARM_AO_RSET'] = 2
        ezca['OAF-RANGE_RHP_RSET'] = 2 # added AE for DARM BLRMS
        ezca.switch('OAF-RANGE_RHP','OUTPUT','ON')

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        return True

##################################################
class SWITCH_AS_CENTERING_MTRX(GuardState):
    index = 880
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # Switch off DC4 inputs
        ezca.switch('ASC-DC4_P','INPUT','OFF')
        ezca.switch('ASC-DC4_Y','INPUT','OFF')

        # Stepping parameters
        nStep = 30
        tRamp = 10.0
        tStep = tRamp/nStep

        # Dictionaries for defining matrix elements
        #self.dofs ={'DC3': '14',
        #       'DC4': '15',
        #      }

        #self.oms ={'OM1': '18',
        #      'OM2': '19',
        #      'OM3': '20',
        #     }

        # Matrices that we need to set DC3 and DC4 to
        #self.out_matrices = {'P':[[0.7712,0],[-1.3071,0],[1.9816,0]],
        #                'Y':[[0.849,0],[1.3104,0],[1.7251,0]]
        #               }

        self.dofs = asdc_centering_matrix.dofs
        self.oms = asdc_centering_matrix.oms
        self.asdc_omtrx_vals = asdc_centering_matrix.asdc_omtrx_vals

        self.matrixSteps = []

        for rot in ['P','Y']:
            mtrx = self.asdc_omtrx_vals['FINAL_'+rot]

            ii=0
            for dof in ['DC3','DC4']:
                ii+=1
                jj=0
                for om in ['OM1','OM2','OM3']:
                    jj+=1
                    channel = 'ASC-OUTMATRIX_'+rot+'_'+str(self.oms[om])+'_'+str(self.dofs[dof])
                    current_val = ezca[channel]

                    #final_val = mtrx[jj-1][ii-1]
                    final_val = mtrx.item(jj-1,ii-1)
                    log('final value is '+str(final_val))

                    diff = final_val - current_val
                    log('diff is '+str(diff))
                    # If no steps required move to next iteration
                    if diff == 0:
                        continue

                    dStep = diff/nStep

                    cmd_string = str(dStep) + ',' + str(nStep)
                    log(cmd_string)
                    # Make an array for stepping the matrix elements
                    self.matrixSteps.append(cdsutils.Step(ezca,channel,cmd_string,time_step=tStep))


        self.do_once = True
        self.clear_hist = True

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        done = True

        for i in range(len(self.matrixSteps)):
            stepDone = self.matrixSteps[i].step()
            done = done and stepDone
        if not done:
            return


        # Make sure that we set the final matrix
        #asdc_centering_matrix.switch_to_mtrx('FINAL')

        if self.do_once:
            for rot in ['P','Y']:
#                mtrx = self.out_matrices[rot]
                mtrx = self.asdc_omtrx_vals['FINAL_'+rot]
                ii=0
                for dof in ['DC3','DC4']:
                    ii+=1
                    jj=0
                    for om in ['OM1','OM2','OM3']:
                        jj+=1
                        channel = 'ASC-OUTMATRIX_'+rot+'_'+str(self.oms[om])+'_'+str(self.dofs[dof])
                        current_val = ezca[channel]

                        #final_val = mtrx[jj-1][ii-1]
                        final_val = mtrx.item(jj-1,ii-1)

                        #diff = final_val - current_val
                        #if diff == 0:
                        #    continue
                        ezca[channel] = final_val
            self.do_once = False

        if self.clear_hist:
            for dof in ["P","Y"]:
                ezca.write("ASC-DC4_{}_RSET".format(dof),2)
            self.clear_hist = False

        return True



##################################################
class FINALIZE_RF_LOCK(GuardState):
    index = 890
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        ezca.switch('LSC-DARM','FM1','FM7','ON','FM8','OFF')

        # Boost DARM, turn off BS BR damping
        ezca['LSC-DARM_TRAMP'] = 1
        ezca.switch('LSC-DARM','FM4','ON')
        time.sleep(0.1)
        ezca['SUS-BS_M2_VRDAMP_P_GAIN'] = 0
        ezca['SUS-BS_M2_VRDAMP_Y_GAIN'] = 0
        time.sleep(0.1)

        # Re-engage SRM alignment
        ezca.switch('ASC-SRC2_P','INPUT','ON')
        ezca.switch('ASC-SRC2_Y','INPUT','ON')
        time.sleep(0.1)

        # ISS second loop gain
        ezca['PSL-ISS_SECONDLOOP_GAIN'] = 30 # 2 W

        # Hold output for normalization
        ezca.switch('LSC-TR_X_NORM','HOLD','ON')
        ezca.switch('LSC-TR_Y_NORM','HOLD','ON')
        time.sleep(1)

        # Switch PD to QPD
        ezca['LSC-TR_DC_OR_QPD_SUM'] = 1

        # Turn off ALS Corner PLLs and Offset to 10V
        for dof in ["COMM","DIFF"]:
            ezca["ALS-C_{}_PLL_INEN".format(dof)] = 0
            ezca["ALS-C_{}_PLL_COMP1".format(dof)] = 0
            ezca["ALS-C_{}_VCO_TUNEOFS".format(dof)] = 10

        # Set ITM Angular TRAMPs
        for itm in ["X","Y"]:
            for dof in ["P","Y"]:
                ezca["SUS-ITM{0}_M0_OPTICALIGN_{1}_TRAMP".format(itm,dof)] = 3

        # Change whitening gain for X_TR_QPD (reduced from 15dB to 3dB)
        ezca['ASC-X_TR_A_WHITEN_GAIN'] = 3
        ezca['ASC-X_TR_B_WHITEN_GAIN'] = 3
        ezca['ASC-Y_TR_A_WHITEN_GAIN'] = 3
        ezca['ASC-Y_TR_B_WHITEN_GAIN'] = 3
        time.sleep(1)

        # re-enable violin damping outputs (they're shut off near the
        # top above)  Moved from DOWN state to prevent accidental ring ups. CB 20181019
        for sus in ["ITMX","ITMY","ETMX","ETMY"]:
            ezca["SUS-{}_L2_DAMP_OUT_SW".format(sus)] = 'ON'

        # set trigger for full lock AE 20200303
        ezca['LSC-IFO_TRIG_THRESH_ON'] = 32
        ezca['LSC-IFO_TRIG_THRESH_OFF'] = 30

        #for optic in ['ETMX','ETMY']:
        #    ezca['CAM-'+optic+'_EXP'] = 200

        #ezca['ASC-PRC2_P_PRE_GAIN'] = 0.35
        #ezca['ASC-PRC2_Y_PRE_GAIN'] = 0.35

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        return True


##################################################
# RF LOCKED checkpoint
class RF_LOCKED(GuardState):
    index = 900

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        if ifoconfig.run_tcs:
            nodes_loose['TCS_RH'].set_request('FINAL_HIGHPOWER')

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        # Turn on RH for high power - trial 19 Feb 2024 CB
        #if ezca['GRD-ISC_LOCK_REQUEST_N'] >= 1194:

        return True

##################################################
def turn_on_dither(optic,dof,osc,freq,clk_gain,sin_gain=0,cos_gain=0,tramp=0):

    if dof == 'PIT':
        matrix.asc_ads_lo_pit.zero(col=osc)
        time.sleep(0.1)
        matrix.asc_ads_lo_pit[optic,osc] = 1
    elif dof == 'YAW':
        matrix.asc_ads_lo_yaw.zero(col=osc)
        time.sleep(0.1)
        matrix.asc_ads_lo_yaw[optic,osc] = 1

    ezca['ASC-ADS_{}{}_OSC_FREQ'.format(dof,osc[-1])] = freq
    ezca['ASC-ADS_{}{}_OSC_TRAMP'.format(dof,osc[-1])] = tramp
    time.sleep(0.1)
    ezca['ASC-ADS_{}{}_OSC_CLKGAIN'.format(dof,osc[-1])] = clk_gain
    ezca['ASC-ADS_{}{}_OSC_SINGAIN'.format(dof,osc[-1])] = sin_gain
    ezca['ASC-ADS_{}{}_OSC_COSGAIN'.format(dof,osc[-1])] = cos_gain

##################################################
# Dither align PRM   # experimental state needs a manual run through to set gains and thresholds CB 20190223
class DITHER_PRM(GuardState):
    index = 910
    request = False
    def main(self):

        #osc = 'OSC5'

        # Lower limit on PRG
        self.prg_ll = 41  #Was 51 in O3

        # If PRG above limit by more than 2, skip this state
        prg = ezavg(ezca,3,'LSC-PRC_GAIN_MON')
        if prg > self.prg_ll + 2:
            log('Skipping the dither')
            return True

        for dof in ['PIT5','YAW5']:
            #turn_on_dither('PRM',dof,osc,ifoconfig.prm_ads_freq[dof],\
                                            #ifoconfig.prm_clk_gain,\
                                            #sin_gain=ifoconfig.prm_sin_gain,\
                                            #cos_gain=ifoconfig.prm_cos_gain,tramp=5)
            # Engage oscilators for these degrees of freedom
            ezca['ASC-ADS_' + dof + '_OSC_FREQ'] = ifoconfig.prm_ads_freq[dof[:-1]]
            ezca['ASC-ADS_' + dof + '_OSC_CLKGAIN'] = ifoconfig.prm_clk_gain
            ezca['ASC-ADS_' + dof + '_OSC_SINGAIN'] = ifoconfig.prm_sin_gain
            ezca['ASC-ADS_' + dof + '_OSC_COSGAIN'] = ifoconfig.prm_cos_gain
            # Turn filter inputs and outputs on 
            ezca.switch('ASC-ADS_' + dof + '_DOF', 'INPUT', 'ON')
            ezca.switch('ASC-ADS_' + dof + '_DOF', 'OUTPUT', 'ON')


        ezca['ASC-ADS_GAIN'] = 0.05
        # return True
        self.timer['prm_dither'] = 20
        self.timer['prm_dither_limit'] = 100  # CB20190411 timers to make PRM dither more

        self.do_once = True
        #self.check_spots = True

        #self.prg_avg = EzAvg(ezca,5,'LSC-PRC_GAIN_MON')

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['prm_dither']:
            return

#        if self.timer['prm_dither_limit']:
#            self.check_spots = False

        #[done,prg] = self.prg_avg.ezAvg(stddev=True)
        #if not done:
        #    return

        #prmspotp = cdsutils.avg(-5,'ASC-ADS_PIT5_DEMOD_I_OUTPUT',stddev=True)
        #prmspoty = cdsutils.avg(-5,'ASC-ADS_YAW5_DEMOD_I_OUTPUT',stddev=True)
        prg = cdsutils.avg(5,'LSC-PRC_GAIN_MON',stddev=True)
        #if prmspotp < 0.1 and prmspoty < 0.1:

#        if self.check_spots:
#            if prg[0] < 50.3 or prg[1] > 0.15:
#                log("PRG not high enough or not settled")
#                return
#            else:
#                log('PRG high enough, turning off dither loop and moving on')
#                ezca['ASC-ADS_GAIN'] = 0.0
#                for dof in ['PIT5','YAW5']:
#                    ezca.switch('ASC-ADS_' + dof + '_DOF', 'INPUT', 'OFF')
#                    ezca['ASC-ADS_' + dof + '_OSC_CLKGAIN'] = 0
#                self.check_spots = False

#        return True

        if (self.timer['prm_dither'] and prg[0] > self.prg_ll and prg[1] < 0.15) or self.timer['prm_dither_limit']:  #prg[0] > 49.5 and prg[1] < 0.3
            if self.do_once:
                ezca['ASC-ADS_GAIN'] = 0.0
                for dof in ['PIT5','YAW5']:
                    ezca.switch('ASC-ADS_' + dof + '_DOF', 'INPUT', 'OFF')
                    ezca['ASC-ADS_' + dof + '_OSC_CLKGAIN'] = 0
                self.do_once = False
            return True
        return


##################################################
class SHUTTER_ALS(GuardState):
    index = 930
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        # Close green beam shutters     #ajm 170830, modified for HWS measurements. Shuttered is the nominal state UNLOCK_PDH is the HWS state #ap 170903 reverted
        #nodes['ALS_XARM'] = 'SHUTTERED'
        #nodes['ALS_YARM'] = 'SHUTTERED'
        nodes_loose['ALS_XARM'] = 'SHUTTERED'  #newnodes
        nodes_loose['ALS_YARM'] = 'SHUTTERED'
        #nodes['ALS_XARM'] = 'UNLOCK_PDH'
        #nodes['ALS_YARM'] = 'UNLOCK_PDH'
        ##nodes_loose['ALS_XARM'] = 'UNLOCK_PDH'    #newnodes
        ##nodes_loose['ALS_YARM'] = 'UNLOCK_PDH'
        ezca['CAM-ETMX_EXP']=100000
        ezca['CAM-ETMY_EXP']=100000
        ezca['CAM-BS_EXP']=50000
 
        # Save the time to a file for offloading
        pathname = '/opt/rtcds/userapps/trunk/asc/l1/scripts/'
        filename = 'aligned_rf_times.txt'
        with open (pathname + filename, "a") as f:
            f.write(str(int(gpstime.gpsnow())) + '\n')

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        #if not self.timer['wait']:
        #    return

        return True
        #return nodes.arrived
        
##################################################
class DARM_NORM_FIX(GuardState):
    index = 940
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        ezca['LSC-TR_X_NORM_TRAMP'] = 0
        time.sleep(0.1)
        trx_mon = ezca['LSC-TR_X_NORM_OUTMON']
        trx_out = ezca['LSC-TR_X_NORM_OUTPUT']
        trx_gain = trx_out/trx_mon
        ezca['LSC-TR_X_NORM_GAIN'] = trx_gain
        time.sleep(0.1)
        ezca.switch('LSC-TR_X_NORM','HOLD','OFF')
        ezca['LSC-TR_X_NORM_TRAMP'] = 10
        time.sleep(0.1)
        ezca['LSC-TR_X_NORM_GAIN'] = 4  #2  #AJM20230119 compensate for TR PD gain reduced by 12dB for power increase.
        self.timer['wait'] = 10
        # turn cal lines on early to monitor optical gain # and cavity pole 20181107 CB
        # JCB 11/15/2018, added pcalx/y option
        if ifoconfig.use_pcaly:
            ezca['CAL-CS_TDEP_PCAL_REF_END_SW'] = 1
            ezca['CAL-PCALY_PCALOSC2_OSC_TRAMP'] = 15
            ezca['CAL-PCALY_PCALOSC3_OSC_TRAMP'] = 15 # no TRAMP in turn on calibration lines state?
            ezca['CAL-PCALX_RX_PD_GAIN'] = 1 # Re-correct gain if we had switched over to PcalX previously
            ezca['CAL-PCALY_PCALOSC2_OSC_FREQ'] = ifoconfig.pcaly_line2_freq
            ezca['CAL-PCALY_PCALOSC2_OSC_SINGAIN'] = ifoconfig.pcaly_line2_amp
            ezca['CAL-PCALY_PCALOSC3_OSC_FREQ'] = ifoconfig.pcaly_line3_freq
            ezca['CAL-PCALY_PCALOSC3_OSC_SINGAIN'] = ifoconfig.pcaly_line3_amp
            ezca['CAL-PCALY_OSC_SUM_TRAMP'] = 15
            ezca['CAL-PCALX_OSC_SUM_TRAMP'] = 15
            time.sleep(0.1)
            ezca['CAL-PCALY_OSC_SUM_ON'] = 1
        else:
            ezca['CAL-CS_TDEP_PCAL_REF_END_SW'] = 0
            ezca['CAL-PCALX_PCALOSC2_OSC_TRAMP'] = 15
            ezca['CAL-PCALX_PCALOSC3_OSC_TRAMP'] = 15 # no TRAMP in turn on calibration lines state?
            ezca['CAL-PCALX_RX_PD_GAIN'] = -1 # Flip polarity to match what Y arm would be
            #ezca['CAL-PCALY_PCALOSC3_OSC_TRAMP'] = 15
            ezca['CAL-PCALX_PCALOSC2_OSC_FREQ'] = ifoconfig.pcaly_line2_freq
            ezca['CAL-PCALX_PCALOSC2_OSC_SINGAIN'] = ifoconfig.pcaly_line2_amp
            ezca['CAL-PCALX_PCALOSC3_OSC_FREQ'] = ifoconfig.pcaly_line3_freq
            ezca['CAL-PCALX_PCALOSC3_OSC_SINGAIN'] = ifoconfig.pcaly_line3_amp
            ezca['CAL-PCALY_OSC_SUM_TRAMP'] = 15
            ezca['CAL-PCALX_OSC_SUM_TRAMP'] = 15
            time.sleep(0.1)
            ezca['CAL-PCALX_OSC_SUM_ON'] = 1
            #ezca['CAL-PCALY_PCALOSC3_OSC_FREQ'] = ifoconfig.pcaly_line3_freq+2.0
            #ezca['CAL-PCALY_PCALOSC3_OSC_SINGAIN'] = ifoconfig.pcaly_line3_amp
            #ezca['CAL-PCALY_OSC_SUM_ON'] = 1

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if not self.timer['wait']:
            return
        ezca.switch('LSC-TR_X_NORM','HOLD','ON')
        return True

##################################################
# TODO: Make state that offloads gain from TRX normalization of DARM, 
# so it's no longer needed.
#class REMOVE_DARM_POWER_NORM(GuardState):
#    index = 941
#    request = False
#
#    @assert_mc_lock
#    @nodes.checker()
#    def main(self):
#
#        ezca['LSC-TR_X_NORM_GAIN'] = 4
#        trx = 4*ezca['LSC-TR_X_NORM_INMON']
#        g_trx = 1.0/(ezca['LSC-POW_NORM_MTRX_1_17']*math.sqrt(trx))

#        # DARM is divided by sqrt(trx) multiplied by the matrix element,
#        # if we ramp to zero, DARM gain will go to infinity, need to set
#        # to unity first
#        g_settounity = 1.0/math.sqrt(trx))


#        dnorm_mtrx_chan = "LSC-POW_NORM_MTRX_1_17"
#        curr_dnorm_gain = ezca[dnorm_mtrx_chan]
#        delta_dnorm_gain = curr_dnorm_gain - g_settounity
#        tramp = 10.0
#        n_steps = 40
#        tstep = tramp/n_steps
#        step_size = delta_dnorm_gain/n_steps
#        cmd_str = "-{},{}".format(step_size,n_steps)
#        self.reduce_pnorm_step = cdsutils.Step(ezca,dnorm_mtrx_chan,cmd_str,time_step=tstep) 


#        self.clean_up = True
#        self.dnorm_mtrx_chan = dnorm_mtrx_chan


#        matrix.lsc_input_arm['DARM','ASVAC_A_RF45_Q'] = g_trx*matrix.lsc_input_arm['DARM','ASVAC_A_RF45_Q']
#        matrix.lsc_input_arm.tramp = tramp
#        time.sleep(0.3)
#        matrix.lsc_input_arm.load()

#    @assert_mc_lock
#    @nodes.checker()
#    def run(self):

#        if not self.reduce_pnorm_step.step():
#            return
#
#        if self.clean_up:
#            ezca[self.dnorm_mtrx_chan] = 0
#            self.clean_up = False
#
#        return True

        

##################################################
class TURN_ON_SOFT_DAMPING(GuardState):
    index = 945
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # Turn off ADS
        ezca['ASC-ADS_GAIN'] = 0

        # Ensure TMS lock filters are on
        for end in ['X','Y']:
            for dof in ['P','Y']:
                ezca.switch('SUS-TMS{}_M1_LOCK_{}'.format(end,dof),'FM1','FM2','ON')
        time.sleep(1)

        # Turn ON DC centering for TRANSMON QPDs MK 20181106
        ezca.switch('ASC-DC6_P','INPUT','ON')
        ezca.switch('ASC-DC6_Y','INPUT','ON')
        ezca.switch('ASC-DC7_P','INPUT','ON')
        ezca.switch('ASC-DC7_Y','INPUT','ON')

        # Turn on SRC2 loop
        #ezca['ASC-SRCL2_P_PRE_TRAMP'] = 3
        #ezca['ASC-SRCL2_Y_PRE_TRAMP'] = 3
        #time.sleep(0.2)
        #ezca['ASC-SRCL2_P_PRE_GAIN'] = 0.3
        #ezca['ASC-SRCL2_Y_PRE_GAIN'] = 0.3
        ####

        ezca['ASC-SRCL1_P_PRE_TRAMP'] = 3
        ezca['ASC-SRCL1_Y_PRE_TRAMP'] = 3      
        time.sleep(0.2)
        ezca['ASC-SRCL1_P_PRE_GAIN'] = 1.
        ezca['ASC-SRCL1_Y_PRE_GAIN'] = 1.

        #for quad in ['ETMX','ETMY','ITMX','ITMY']:
        #    for dof in ['P','Y']:
        #        ezca.write('SUS-{}_L1_LOCK_{}_TRAMP'.format(quad,dof),10)
        #time.sleep(1)

        #for quad in ['ETMX','ETMY','ITMX','ITMY']:
        #    for dof in ['P','Y']:
        #        ezca.write('SUS-{}_L1_LOCK_{}_GAIN'.format(quad,dof),-7)

        self.chans = []
        for end in ['X','Y']:
            for dof in ['PIT','YAW']:
                self.chans.append('ASC-{}_TR_B_{}_OUTPUT'.format(end,dof))
        self.threshold = 0.2

        self.turn_on_soft_loops = True
        self.timer['wait'] = 1

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return

        vals = cdsutils.avg(5,self.chans)

        if any(abs(val) > self.threshold for val in vals):
            notify('Waiting for TR B QPDs to center')
            return

        if self.turn_on_soft_loops:
            ezca.switch('ASC-DSOFT_P','INPUT','ON')
            ezca.switch('ASC-CSOFT_P','INPUT','ON')
            ezca.switch('ASC-DSOFT_Y','INPUT','ON')
            ezca.switch('ASC-CSOFT_Y','INPUT','ON')
            ezca['ASC-DSOFT_P_GAIN'] = 0.6 # 
            ezca['ASC-CSOFT_P_GAIN'] = 0.2 #
            ezca['ASC-DSOFT_Y_GAIN'] = 0.3 #
            ezca['ASC-CSOFT_Y_GAIN'] = 0.1 #0.2 20241029 AE/VF #0.5 20240221 
            self.turn_on_soft_loops = False
            self.timer['wait'] = 3
            return

        return True


##################################################
class POWER_UP_10W(GuardState):
    index = 950
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

	    # make sure IMC guardian is still managed        
        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed()    #newnode


        #nodes['IMC_LOCK'] = 'LOCKED_10W'
        # set the EPICS channels for the power up
        ezca['PSL-GUARD_POWER_REQUEST']  = 13   #10
        ezca['PSL-GUARD_STEP_REFL_GAIN'] = 1
        ezca['PSL-GUARD_STEP_DC_GAIN']   = 0
        time.sleep(1)

        nodes['IMC_LOCK'].set_request('IFO_POWER')

        #for optic in ['ETMX','ETMY']:
        #    ezca['CAM-'+optic+'_EXP'] = 100

        # reduce exposure of AS and TM cameras smoothly edit CB 20181004
                                             #TARGET EXPOSURE
        ascamexpstr = str(-(ezca['CAM-AS_EXP'] -       600      )/100) +',100'
        etmxcamexpstr = str(-(ezca['CAM-ETMX_EXP'] -  100000      )/100) +',100'
        etmycamexpstr = str(-(ezca['CAM-ETMY_EXP'] -  100000      )/100) +',100'
        itmxcamexpstr = str(-(ezca['CAM-ITMX_EXP'] -  30000      )/100) +',100'
        itmycamexpstr = str(-(ezca['CAM-ITMY_EXP'] -  30000      )/100) +',100'
        self.ASCAMstep = cdsutils.Step(ezca,'CAM-AS_EXP',ascamexpstr,time_step=0.1)
        #self.ETMXCAMstep = cdsutils.Step(ezca,'CAM-ETMX_EXP',etmxcamexpstr,time_step=0.1)
        #self.ETMYCAMstep = cdsutils.Step(ezca,'CAM-ETMY_EXP',etmycamexpstr,time_step=0.1)
        self.ITMXCAMstep = cdsutils.Step(ezca,'CAM-ITMX_EXP',itmxcamexpstr,time_step=0.1)
        self.ITMYCAMstep = cdsutils.Step(ezca,'CAM-ITMY_EXP',itmycamexpstr,time_step=0.1)
 
        for loo in ['D','C']:
            for DOF in ['P','Y']:
                ezca['ASC-' + loo + 'HARD_' + DOF + '_TRAMP'] = 3

        #ezca['CAM-ETMY_SCATTER_EXP']=2000 #CB20190410 to capture scatter power up images


    @assert_mc_lock
    @nodes.checker()
    def run(self):
        #if not (nodes['IMC_LOCK'].arrived and nodes['IMC_LOCK'].done):
        # reduce exposure of AS and TM cameras smoothly edit CB 20181004
        as_fin = self.ASCAMstep.step()
        ix_fin = self.ITMXCAMstep.step()
        iy_fin = self.ITMYCAMstep.step()
        #ex_fin = self.ETMXCAMstep.step()
        #ey_fin = self.ETMYCAMstep.step()
        if not (nodes['IMC_LOCK'].arrived and nodes['IMC_LOCK'].done and (ezca['IMC-IM4_TRANS_SUM_OUTPUT'] > 8.9) and as_fin and ix_fin and iy_fin):     # cdb 181004
            log('Still powering up')
            return
        return True

##################################################
class ASC_HARD_10W(GuardState):
    index = 960
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

#        ezca['CAM-ETMY_EXP']=10000

        self.t_ramp = 2

        #prep for transition
        for dof1 in ["CHARD","DHARD"]:
            for dof2 in ["P","Y"]:
                 ezca.write("ASC-{}_{}_B_TRAMP".format(dof1,dof2),0)
                 time.sleep(0.2)
                 ezca.write("ASC-{}_{}_B_GAIN".format(dof1,dof2),0)
                 time.sleep(0.2)
                 ezca.switch("ASC-{}_{}_B".format(dof1,dof2),"INPUT","OUTPUT","FM1","ON")
                 #Set tramps for transition
                 ezca.write("ASC-{}_{}_A_TRAMP".format(dof1,dof2),self.t_ramp)
                 ezca.write("ASC-{}_{}_B_TRAMP".format(dof1,dof2),self.t_ramp)


        self.dofs = ["CHARD_P","CHARD_Y","DHARD_P","DHARD_Y"]
        #self.dofs = ["CHARD_P","DHARD_P","DHARD_Y"]


        # Turn off 0.45 resonant gains for yaw (consider doing this for pitch as well)
        ezca.switch('ASC-CHARD_Y','FM3','OFF')
        ezca.switch('ASC-DHARD_Y','FM3','OFF')


        self.switch_to_B = True
        self.switch_A_to_10W = True
        self.switch_to_A = True
        self.increase_chard_yaw = True
        self.ii = 0

        self.timer["wait"] = 1

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            return

        if self.ii<len(self.dofs) and self.switch_to_B:
            notify("Switching HARDs over to B")
            ezca.write("ASC-{}_A_GAIN".format(self.dofs[self.ii]), 0)
            ezca.write("ASC-{}_B_GAIN".format(self.dofs[self.ii]), 1)
            self.timer["wait"] = self.t_ramp
            self.ii+=1
            return

        self.switch_to_B = False

        if self.switch_A_to_10W:
            notify("Switching A bank from 2 to 10 W")
            for dof in self.dofs:
                ezca.switch("ASC-{}_A".format(dof),"FM1","OFF","FM2","ON")
            self.switch_A_to_10W = False
            self.timer["wait"] = 2
            return

        if self.switch_to_A:
            notify("Switching back to A banks")
            for dof in self.dofs:
                ezca.write("ASC-{}_A_GAIN".format(dof), 1)
                ezca.write("ASC-{}_B_GAIN".format(dof), 0)
            self.switch_to_A = False
            self.timer["wait"] = self.t_ramp
            return

        if self.increase_chard_yaw:
            ezca.write("ASC-CHARD_Y_PRE_GAIN", 0.6)
            self.increase_chard_yaw = False
            return

        return True

####################################################################################
class CENTER_SPOTS_WITH_CAMERAS(GuardState):
    index = 970
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        #Increase exposures
        log('Increasing camera exposures')
        ezca['CAM-ETMX_EXP']=100000
        ezca['CAM-ETMY_EXP']=100000
        ezca['CAM-BS_EXP']=50000

        # Zero ITM camera columns
        matrix.asc_ads_output_pit.zero(col='PIT11')
        matrix.asc_ads_output_pit.zero(col='PIT12')
        matrix.asc_ads_output_yaw.zero(col='YAW11')
        matrix.asc_ads_output_yaw.zero(col='YAW12')

        log('Setting output matrices for camera servos')
        #ezca['ASC-ADS_OUT_PIT_MTRX_7_13']=1
        matrix.asc_ads_output_pit['ITMX','PIT13']=1
        #ezca['ASC-ADS_OUT_PIT_MTRX_9_13']=0.78
        matrix.asc_ads_output_pit['ETMX','PIT13']=0.78
        #ezca['ASC-ADS_OUT_PIT_MTRX_8_14']=1
        matrix.asc_ads_output_pit['ITMY','PIT14']=1
        #ezca['ASC-ADS_OUT_PIT_MTRX_10_14']=0.78
        matrix.asc_ads_output_pit['ETMY','PIT14']=0.78
        #ezca['ASC-ADS_OUT_PIT_MTRX_3_15']=1
        matrix.asc_ads_output_pit['PRM','PIT15']=1

        #ezca['ASC-ADS_OUT_YAW_MTRX_7_13']=1
        matrix.asc_ads_output_yaw['ITMX','YAW13']=1
        #ezca['ASC-ADS_OUT_YAW_MTRX_9_13']=-0.78
        matrix.asc_ads_output_yaw['ETMX','YAW13']=-0.78
        #ezca['ASC-ADS_OUT_YAW_MTRX_8_14']=1
        matrix.asc_ads_output_yaw['ITMY','YAW14']=1
        #ezca['ASC-ADS_OUT_YAW_MTRX_10_14']=-0.78
        matrix.asc_ads_output_yaw['ETMY','YAW14']=-0.78
        #ezca['ASC-ADS_OUT_YAW_MTRX_3_15']=1
        matrix.asc_ads_output_yaw['PRM','YAW15']=1

        # Set gains
        log('Setting gains for camera servos')
        ezca['ASC-ADS_PIT13_DOF_GAIN']=-150
        ezca['ASC-ADS_PIT14_DOF_GAIN']=-150
        ezca['ASC-ADS_PIT15_DOF_GAIN']=0.01

        ezca['ASC-ADS_YAW13_DOF_GAIN']=150
        ezca['ASC-ADS_YAW14_DOF_GAIN']=150
        ezca['ASC-ADS_YAW15_DOF_GAIN']=0.01

        # Set offsets
        log('Setting offsets for camera servos')
        # ETMX
        ezca['ASC-ADS_YAW13_DOF_OFFSET']= -6 #-2 changed ae 241101 # -3 ae 240827 ; 0 cb 170324
        ezca['ASC-ADS_PIT13_DOF_OFFSET']= 2 #1 #VB20240904 changed -1->1 #-6 cb 090324 # -3

        #ETMY
        ezca['ASC-ADS_YAW14_DOF_OFFSET']= 13 # 10
        ezca['ASC-ADS_PIT14_DOF_OFFSET']= 1 #0 cb 090324 #-9 #-12

        # BS
        ezca['ASC-ADS_YAW15_DOF_OFFSET']=-340.0
        ezca['ASC-ADS_PIT15_DOF_OFFSET']=-222.2

        # Turn on servos
        log('Turning on camera servos')
        for dof1 in ['PIT','YAW']:
            for dof2 in ['13','14','15']:
                ezca.switch('ASC-ADS_{}{}_DOF'.format(dof1,dof2),'OFFSET','FM1','ON')
                ezca.switch('ASC-ADS_{}{}_DOF'.format(dof1,dof2),'INPUT','OUTPUT','ON')

        # Turn on ads dither lines for monitoring purposes
        for dof1 in ['PIT','YAW']:
            for dof2 in ['1','3','4']:
                ezca['ASC-ADS_{}{}_OSC_CLKGAIN'.format(dof1,dof2)] = ifoconfig.quad_clk_gain

        self.chans = ['LSC-PRC_GAIN_MON']
        for dof in ['PIT13','PIT14','PIT15','YAW13','YAW14','YAW15']:
            self.chans.append('ASC-ADS_{}_DOF_OUTPUT'.format(dof))

        self.prg_threshold = 39

        self.timer['wait'] = 30

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return

        #avgs = cdsutils.avg(10,self.chans,stddev=True)
        #prg = avgs[0]
        #xspotp = avgs[1]
        #yspotp = avgs[2]
        #bspotp = avgs[3]
        #xspoty = avgs[4]
        #yspoty = avgs[5]
        #bspoty = avgs[6]

        #if (prg[0] < self.prg_threshold or prg[1] > 0.1):
        #    notify("Waiting for PRG to settle higher than {}".format(self.prg_threshold))
        #    return

        return True

##################################################
# Switch actuators to low noise state
class SWITCH_ACTUATORS(GuardState):
    index = 980
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # Set RM tramps
        for opt in ['PRM','SRM','PR2','SR2']:
            ezca['SUS-' + opt + '_M3_DRIVEALIGN_L2L_TRAMP'] = 3

        self.switch_bs = True
        self.switch_triples = [True,True,True,True,True]
        self.tune_mich = True
        self.switch_etmx = True
        self.switch_quads = True

        self.timer['wait'] = 0


    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return

        if not ((nodes_optics['SUS_BS'].arrived and nodes_optics['SUS_BS'].done) \
            and (nodes_optics['SUS_ETMX'].arrived and nodes_optics['SUS_ETMX'].done) \
            and (nodes_optics['SUS_ETMY'].arrived and nodes_optics['SUS_ETMY'].done) \
            and (nodes_optics['SUS_ITMX'].arrived and nodes_optics['SUS_ITMX'].done) \
            and (nodes_optics['SUS_ITMY'].arrived and nodes_optics['SUS_ITMY'].done)):
            return

        # BS switch (TODO: make SUS BS work!!!)
        if self.switch_bs:
            log('Switching BS coils to low noise')
            nodes_optics['SUS_BS'] = 'COILDRIVER_LOWNOISE_1'
            self.timer['wait'] = 2
            self.switch_bs = False
            return
    
        # RM switch
        if self.switch_triples[0]:
            log('Sending control signals to PR2 and SRM')
            # Control on PR2
            ezca['SUS-PRM_M3_DRIVEALIGN_L2L_GAIN'] = 0
            ezca['SUS-PR2_M3_DRIVEALIGN_L2L_GAIN'] = 1.5
            # Control on SRM
            ezca['SUS-SRM_M3_DRIVEALIGN_L2L_GAIN'] = 3
            ezca['SUS-SR2_M3_DRIVEALIGN_L2L_GAIN'] = 0

            self.timer['wait'] = 3.5
            self.switch_triples[0] = False
            return

        if self.switch_triples[1]:
            # Switch states (individually now)
            for coil in ['UL','UR','LL','LR']:
                ezca['SUS-PRM_BIO_M3_' + coil + '_STATEREQ'] = 3
                ezca['SUS-SR2_BIO_M3_' + coil + '_STATEREQ'] = 3 
            self.timer['wait'] = 2
            self.switch_triples[1] = False
            return

        if self.switch_triples[2]:
            log('Sending control signals to PRM and SR2')
            # Control back to PRM
            ezca['SUS-PRM_M3_DRIVEALIGN_L2L_GAIN'] = 3
            ezca['SUS-PR2_M3_DRIVEALIGN_L2L_GAIN'] = 0
            # Control back to SR2
            ezca['SUS-SRM_M3_DRIVEALIGN_L2L_GAIN'] = 0
            ezca['SUS-SR2_M3_DRIVEALIGN_L2L_GAIN'] = 1.5
            self.timer['wait'] = 3.5
            self.switch_triples[2] = False
            return

        if self.switch_triples[3]:
            log('Switching PR2 and SRM to low noise')
            # Switch Low Pass ON
            for coil in ['UL','UR','LL','LR']:
                ezca['SUS-PR2_BIO_M3_' + coil + '_STATEREQ'] = 3
                ezca['SUS-SRM_BIO_M3_' + coil + '_STATEREQ'] = 3
            self.timer['wait'] = 2
            self.switch_triples[3] = False
            return

        if self.switch_triples[4]:
            log('Sharing control between PRM and PR2')
            # Share control between PR2 and PRM
            ezca['SUS-PRM_M3_DRIVEALIGN_L2L_GAIN'] = 1
            ezca['SUS-PR2_M3_DRIVEALIGN_L2L_GAIN'] = 1
            self.switch_triples[4] = False
            self.timer['wait'] = 3
            return

        if self.tune_mich:
            # now that the coils are switched, tune up the Michelson loop a little
            ezca.switch('SUS-BS_M2_LOCK_L','FM2','FM9','OFF')
            time.sleep(1)
            ezca.switch('LSC-MICH','FM4','OFF','FM3','ON')
            ezca.switch('SUS-BS_M2_LOCK_L','FM3','ON')      #Moved from DRMI FF to here AJM210121
            self.tune_mich = False
            return

        if self.switch_etmx:
            nodes_optics['SUS_ETMX'] = 'COILDRIVER_LOWNOISE_2'
            self.switch_etmx = False
            return

        if self.switch_quads:
            log('Switching QUADs to low noise')
            nodes_optics['SUS_ETMX'] = 'COILDRIVER_LOWNOISE_1'
            nodes_optics['SUS_ETMY'] = 'COILDRIVER_LOWNOISE_1'
            nodes_optics['SUS_ITMX'] = 'COILDRIVER_LOWNOISE_1'
            nodes_optics['SUS_ITMY'] = 'COILDRIVER_LOWNOISE_1'
            self.timer['wait'] = 2
            self.switch_quads = False
            return


        return True

'''
##################################################
# Switch actuators to low noise state
class SWITCH_ACTUATORS(GuardState):
    index = 980 #920
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        #ezca['SUS-ETMX_BIO_L1_STATEREQ'] = 3
        time.sleep(0.5)
        #ezca['SUS-ETMY_BIO_L1_STATEREQ'] = 3
        #time.sleep(0.5)

        # increase DC3 gain AE 220525 (moved here from later, we need it)
        #ezca['ASC-DC3_P_PRE_TRAMP'] = 3
        #time.sleep(0.1)
        #ezca['ASC-DC3_P_PRE_GAIN'] = 3
        #ezca['ASC-DC3_Y_PRE_TRAMP'] = 3
        #time.sleep(0.1)
        #ezca['ASC-DC3_Y_PRE_GAIN'] = 3        

        bs_m2_actuator.switch_lownoise_all()

        for opt in ['PRM','SRM','PR2','SR2']:
            ezca['SUS-' + opt + '_M3_DRIVEALIGN_L2L_TRAMP'] = 3

        time.sleep(0.1)

        # Control on PR2
        ezca['SUS-PRM_M3_DRIVEALIGN_L2L_GAIN'] = 0
        ezca['SUS-PR2_M3_DRIVEALIGN_L2L_GAIN'] = 1.5
        # Control on SRM
        ezca['SUS-SRM_M3_DRIVEALIGN_L2L_GAIN'] = 3
        ezca['SUS-SR2_M3_DRIVEALIGN_L2L_GAIN'] = 0
        # Control on SR2 (ajm20211122)
        #ezca['SUS-SRM_M3_DRIVEALIGN_L2L_GAIN'] = 0
        #ezca['SUS-SR2_M3_DRIVEALIGN_L2L_GAIN'] = 1.5

        time.sleep(3.5)

        # Switch states (individually now)
        for coil in ['UL','UR','LL','LR']:
            ezca['SUS-PRM_BIO_M3_' + coil + '_STATEREQ'] = 3
            ezca['SUS-SR2_BIO_M3_' + coil + '_STATEREQ'] = 3   #ajm20211122
            #ezca['SUS-SRM_BIO_M3_' + coil + '_STATEREQ'] = 3
        time.sleep(1)

        # Control back to PRM
        ezca['SUS-PRM_M3_DRIVEALIGN_L2L_GAIN'] = 3
        ezca['SUS-PR2_M3_DRIVEALIGN_L2L_GAIN'] = 0
        # Control back to SR2
        ezca['SUS-SRM_M3_DRIVEALIGN_L2L_GAIN'] = 0
        ezca['SUS-SR2_M3_DRIVEALIGN_L2L_GAIN'] = 1.5
        # Control back to SRM (ajm20211122)
        #ezca['SUS-SRM_M3_DRIVEALIGN_L2L_GAIN'] = 3
        #ezca['SUS-SR2_M3_DRIVEALIGN_L2L_GAIN'] = 0
        time.sleep(3.5)

#        # Switch Acquire mode to OFF
#        for coil in ['UL','UR','LL','LR']:
#            ezca['SUS-PR2_BIO_M3_' + coil + '_STATEREQ'] = 1
#            ezca['SUS-SRM_BIO_M3_' + coil + '_STATEREQ'] = 1
#       time.sleep(2)

        # Switch Low Pass ON
        for coil in ['UL','UR','LL','LR']:
            ezca['SUS-PR2_BIO_M3_' + coil + '_STATEREQ'] = 3
            ezca['SUS-SRM_BIO_M3_' + coil + '_STATEREQ'] = 3   #ajm20211122
            #ezca['SUS-SR2_BIO_M3_' + coil + '_STATEREQ'] = 3
        time.sleep(2)

        # Share control between PR2 and PRM
        ezca['SUS-PRM_M3_DRIVEALIGN_L2L_GAIN'] = 1
        ezca['SUS-PR2_M3_DRIVEALIGN_L2L_GAIN'] = 1

        # Switch to SR2 M2
#        ezca['SUS-SR2_M2_DRIVEALIGN_L2L_RSET'] = 2
#        ezca['SUS-SRM_M2_DRIVEALIGN_L2L_TRAMP'] = 10
#        ezca['SUS-SR2_M2_DRIVEALIGN_L2L_TRAMP'] = 10
#        time.sleep(1)
        #ezca['SUS-SR2_M2_DRIVEALIGN_L2L_GAIN'] = 0.5
        #ezca['SUS-SRM_M2_DRIVEALIGN_L2L_GAIN'] = 0
        #time.sleep(10)

        # now that the coils are switched, tune up the Michelson loop a little
        ezca.switch('SUS-BS_M2_LOCK_L','FM2','FM9','OFF')
        time.sleep(1)
        ezca.switch('LSC-MICH','FM4','OFF','FM3','ON')
        ezca.switch('SUS-BS_M2_LOCK_L','FM3','ON')      #Moved from DRMI FF to here AJM210121

        #JCB 2022-05-12, modify susconst.py in /opt/rtcds/userapps/release/sus/l1/guardian/ to change which state is engaged
        #JCB 20240905 Modified to run L1 first on ETMX, then L2s on all quads
        nodes_optics['SUS_ETMX'] = 'COILDRIVER_LOWNOISE_2'
        self.first_pass = True

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if nodes_optics['SUS_ETMX'].arrived and nodes_optics['SUS_ETMX'].done and nodes_optics['SUS_ETMY'].arrived and nodes_optics['SUS_ETMY'].done \
            and nodes_optics['SUS_ITMX'].arrived and nodes_optics['SUS_ITMX'].done and nodes_optics['SUS_ITMY'].arrived and nodes_optics['SUS_ITMY'].done:
            if self.first_pass:
                nodes_optics['SUS_ETMX'] = 'COILDRIVER_LOWNOISE_1'
                nodes_optics['SUS_ETMY'] = 'COILDRIVER_LOWNOISE_1'
                nodes_optics['SUS_ITMX'] = 'COILDRIVER_LOWNOISE_1'
                nodes_optics['SUS_ITMY'] = 'COILDRIVER_LOWNOISE_1'
                
                self.first_pass = False
                time.sleep(0.5)
                return
            return True
        else:
            return False
'''

##################################################
class SWITCH_ESD_TO_LOW_NOISE(GuardState):
    index = 990
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        t_handoff = 20
        handoff = False

        #TODO: Work out current configuration from the drive
        if 'ETMX' in ifoconfig.locking_esds and 'ETMY' in ifoconfig.locking_esds:
            handoff = True

        #etms = ['ETMX','ETMY']

        #if ifoconfig.lnlv_switch_on_current_esd:
        #    self.goto_esd = ifoconfig.lownoise_esd
        #    self.other_esd = other etm in etms #FIXME
        #else:
        #    self.other_esd = ifoconfig.lownoise_esd
        #    self.goto_esd = other etm in etms #FIXME

        # which esd do we want to be on here?
        if ifoconfig.lnlv_switch_on_current_esd:
            self.goto_esd = ifoconfig.lownoise_esd
            if ifoconfig.lownoise_esd == 'ETMX':
                self.other_esd = 'ETMY'
            elif ifoconfig.lownoise_esd == 'ETMY':
                self.other_esd = 'ETMX'
        else:
            if ifoconfig.lownoise_esd == 'ETMX':
                self.goto_esd = 'ETMY'
                self.other_esd = 'ETMX'
            elif ifoconfig.lownoise_esd == 'ETMY':
                self.goto_esd = 'ETMX'
                self.other_esd = 'ETMY'

        if handoff:
            for quad in ["ETMX","ETMY"]:
                ezca["SUS-{}_L3_DRIVEALIGN_L2L_TRAMP".format(quad)] = t_handoff
            time.sleep(0.1)

            # put all drive one esd
            if self.goto_esd == 'ETMX':
                ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_GAIN'] *= 2
                ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN'] = 0
            elif self.goto_esd == 'ETMY':
                ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN'] *= 2
                ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_GAIN'] = 0

            self.timer["wait"] = t_handoff + 1
            self.ramp_down_bias = True
            self.switch_other_esd_to_low_noise = True
        else:
            self.timer["wait"] = 1
            self.ramp_down_bias = False
            self.switch_other_esd_to_low_noise = False

        #JCB 20240904 Temporary test with ETMY bias ramped up
        self.ramp_up_bias = True
        #self.ramp_up_bias = not ifoconfig.lnlv_switch_on_current_esd

        self.lownoise_biases = {'ETMX':0,'ETMY':100}

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            return

        # After the drive is on the other ESD, ramp down bias
        if self.ramp_down_bias:
            #ezca['SUS-ETMY_L3_LOCK_BIAS_TRAMP'] = 20
            ezca['SUS-{}_L3_LOCK_BIAS_TRAMP'.format(self.other_esd)] = 20
            time.sleep(0.1)
            #ezca['SUS-ETMY_L3_LOCK_BIAS_GAIN'] = 0
            ezca['SUS-{}_L3_LOCK_BIAS_GAIN'.format(self.other_esd)] = 0
            self.ramp_down_bias = False
            self.timer["wait"] = 21
            return

        # Switch other esd to low noise.
        if self.switch_other_esd_to_low_noise:
            #ezca['SUS-ETMY_L3_LOCK_BIAS_TRAMP'] = 0
            ezca['SUS-{}_L3_LOCK_BIAS_TRAMP'.format(self.other_esd)] = 0
            time.sleep(0.1)
            #ezca['SUS-ETMY_L3_LOCK_BIAS_OFFSET'] = 0
            ezca['SUS-{}_L3_LOCK_BIAS_OFFSET'.format(self.other_esd)] = 0
            time.sleep(0.1)
            #ezca['SUS-ETMY_L3_LOCK_BIAS_RSET'] = 2
            ezca['SUS-{}_L3_LOCK_BIAS_RSET'.format(self.other_esd)] = 2
            time.sleep(0.1)
            #ezca['SUS-ETMY_L3_LOCK_BIAS_GAIN'] = 320
            ezca['SUS-{}_L3_LOCK_BIAS_GAIN'.format(self.other_esd)] = 320
            #set_esd_state("ETMY","LV")
            set_esd_state(self.other_esd,"LV")
            self.switch_other_esd_to_low_noise = False
            self.timer["wait"] = 1
            return

        # Ramp up the bias of the other esd, ready for transition
        if self.ramp_up_bias:
            ezca['SUS-{}_L3_LOCK_BIAS_TRAMP'.format(self.other_esd)] = 30 #10 JCB 20240904
            time.sleep(0.1)
            ezca['SUS-{}_L3_LOCK_BIAS_OFFSET'.format(self.other_esd)] = 45 #ifoconfig.esdy_sign_flip*self.lownoise_biases[self.other_esd] JCB 20240904
            self.timer["wait"] = 30 #5
            self.ramp_up_bias = False
            return

        #JCB removed test 20230908
        #JCB 20230905 Quick change to make ETMY ESD 100V early on instead of -30 later
        #if self.set_etmy_bias:
        #    ezca['SUS-ETMY_L3_LOCK_BIAS_TRAMP'] = 30
        #    time.sleep(0.2)
        #    ezca['SUS-ETMY_L3_LOCK_BIAS_OFFSET'] = -100
        #    self.set_etmy_bias = False
        #    self.timer["wait"] = 30
        #    return

        return True  


##################################################
class RF_LOCKED_10W(GuardState):
    index = 1000

    #@assert_mc_lock
    #@nodes.checker()
    #def main(self):

        # Turn on RH for 45W if this state or higher is requested  # No longer used CB20190111
        #if ezca['GRD-ISC_LOCK_REQUEST_N'] >= 1194:
        #    nodes['TCS_RH'].set_request('READY_HIGHPOWER')

    @assert_mc_lock
    def run(self):

        for quad in ifoconfig.violin_list.keys():
            for mode in ifoconfig.violin_list[quad]:
                if ezca['SUS-{}_L2_DAMP_MODE{}_RMSLP_LOG10_OUTMON'.format(quad,str(mode))] > -15.5:
                    notify('Violins too high! Please damp')
                    log('Violin modes are too high, need to damp before proceeding.')
                    return False
        return True

###################################################

class LOCKING_OMC(GuardState):
    index = 1020
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        
        #self.autolock = False
        ezca["LSC-DARM_OFFSET"] = ifoconfig.fringe_side * ifoconfig.darm_offset

        #ezca.switch('LSC-DARM','OFFSET','ON')
        nodes.set_managed() #or nodes['OMC_LOCK'].set_managed()

        #if self.autolock:
        #    nodes['OMC_LOCK'].set_request('MANUAL_LOCKING')
        #    while nodes['OMC_LOCK'].state != 'MANUAL_LOCKING':
        #        log('Waiting for OMC_LOCK to reach MANUAL_LOCKING state') 

        nodes['OMC_LOCK'].set_request('READY_FOR_HANDOFF')
    
    @nodes.checker()
    @assert_mc_lock
    def run(self):

        # OMC_LOCKED will stall if it jumps, so ensure it's constantly revived
        for node in nodes.get_stalled_nodes():
            log("reviving node: %s" % node.name)
            node.revive()

        if nodes['OMC_LOCK'].state == 'MANUAL_LOCKING':
            notify('OMC needs manual locking')
            return

        if not (nodes['OMC_LOCK'].state == 'READY_FOR_HANDOFF' and 
                nodes['OMC_LOCK'].arrived and nodes['OMC_LOCK'].done):
            notify('Waiting on OMC_LOCK to be READY_FOR_HANDOFF')
            return

        return True

#        # increased from 1 to 5 seconds averaging AP 10/14/2016
#	    omc_dcpd = cdsutils.avg(5, 'OMC-DCPD_SUM_OUTPUT', stddev=False)
#        
#	    if (ezca.LIGOFilter('LSC-DARM').is_engaged('OFFSET') and
#            ezca.LIGOFilter('OMC-LSC_SERVO').is_input_on() and
#            ezca.LIGOFilter('OMC-LSC_SERVO').is_engaged('FM1') and
#            ezca.LIGOFilter('OMC-LSC_SERVO').is_engaged('FM2') and
#            ezca['OMC-ASC_MASTERGAIN'] == 1 and
#            ezca.LIGOFilter('OMC-ASC_POS_X').is_engaged('FM2') and
#            ezca.LIGOFilter('OMC-ASC_POS_Y').is_engaged('FM2') and
#            ezca.LIGOFilter('OMC-ASC_ANG_X').is_engaged('FM2') and
#            ezca.LIGOFilter('OMC-ASC_ANG_Y').is_engaged('FM2') and
#            omc_dcpd < 13 and omc_dcpd > 3.00 ):
#            return True
#        else:
#            notify('OMC NOT LOCKED ON CARRIER')

class MANUAL_OMC(GuardState):
    index = 1025
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        ezca["LSC-DARM_OFFSET"] = ifoconfig.fringe_side * ifoconfig.darm_offset #Nominally 5
        ezca.switch('LSC-DARM','OFFSET','ON')

    @nodes.checker()
    @assert_mc_lock
    def run(self):


        if not ezca.LIGOFilter('LSC-DARM').is_engaged('OFFSET'):
            notify("DARM Offset is off")
            return

        if not ezca.LIGOFilter('OMC-LSC_SERVO').is_input_on():
            notify("OMC LSC servo is off")
            return

        if not (ezca.LIGOFilter('OMC-LSC_SERVO').is_engaged('FM1') and
                ezca.LIGOFilter('OMC-LSC_SERVO').is_engaged('FM2')):
            notify("OMC LSC Boost and/or Int are off")
            return

        if not ezca['OMC-ASC_MASTERGAIN'] == 1:
            notify("OMC ASC Master Gain not set to 1")
            return

        if not (ezca.LIGOFilter('OMC-ASC_POS_X').is_engaged('FM2') and
                ezca.LIGOFilter('OMC-ASC_POS_Y').is_engaged('FM2') and
                ezca.LIGOFilter('OMC-ASC_ANG_X').is_engaged('FM2') and
                ezca.LIGOFilter('OMC-ASC_ANG_Y').is_engaged('FM2')):
            notify("OMC ASC integrators are off")
            return

        # increased from 1 to 5 seconds averaging AP 10/14/2016
        omc_dcpd = cdsutils.avg(3, 'OMC-DCPD_SUM_OUTPUT', stddev=False)   

        if not (3.0 < omc_dcpd < 13.00):
            notify("OMC DCPD current is {} and not correct for carrier".format(omc_dcpd))
            return
        return True

##################################################
class RF_TO_DC_READOUT(GuardState):
    index = 1030
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        # Set violin damping to off for transition VB20230320
        for bit,val in zip(['TRAMP','GAIN'],[3,0]):
            for quad in ifoconfig.violin_list.keys():
                for mode in ifoconfig.violin_list[quad]:
                    ezca['SUS-{}_L2_DAMP_MODE{}_{}'.format(quad,mode,bit)] = val
            time.sleep(0.1)
        time.sleep(2.9)
        ezca['LSC-OMC_DC_GAIN'] = 1
        ezca['LSC-DARM_TRAMP'] = 5
        #JCB 20230120, scaling omc matrix by input power
        im4_trans_power = cdsutils.avg(1,'IMC-IM4_TRANS_SUM_OUTPUT',stddev=False)
        #matrix.lsc_input_arm["DARM", "OMC_DC"] = -0.12*10.0/im4_trans_power  
        matrix.lsc_input_arm["DARM", "OMC_DC"] = ifoconfig.fringe_side * 0.12*10.0/im4_trans_power
        #matrix.lsc_input_arm['DARM', 'ASAIR_A_RF45_Q'] = 0
        #ezca['LSC-ARM_INPUT_MTRX_SETTING_1_7'] = 0
        matrix.lsc_input_arm['DARM', 'ASVAC_A_RF45_Q'] = 0
        matrix.lsc_input_arm.TRAMP = 5  #10
        # make sure future power steps account for OMC gain as well
        ezca['PSL-GUARD_STEP_DC_GAIN'] = 1
        time.sleep(0.1)
        matrix.lsc_input_arm.load()
        self.timer['wait'] = 1
        
    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if not self.timer['wait']:
            return
        return not any(matrix.lsc_input_arm.is_ramping())

##################################################
#TODO: Test new state AJM20201130

class OFFLOAD_OMS(GuardState):
    index = 1040
    request = False

#    @assert_mc_lock
#    @nodes.checker()
    def main(self):

        # Make channel list of control signals
        chans = []
        for sus in ["OM1","OM2","OM3"]:
            for dof in ["P","Y"]:
                chans.append("SUS-{0}_M1_DRIVEALIGN_{1}_OUTMON".format(sus,dof))
        om_ctrl = cdsutils.avg(5, chans, stddev=False) 

        tramp = 10
        threshold = 30
        if any(val > threshold for val in om_ctrl):

            log("At least one control signal is greater than {} counts, offloading OMs".format(threshold))

            for sus in ["OM1","OM2","OM3"]:
                for dof in ["P","Y"]:
                    ezca["SUS-{0}_M1_OPTICALIGN_{1}_TRAMP".format(sus,dof)] = tramp

            ii = 0
            for sus in ["OM1","OM2","OM3"]:
                for dof in ["P","Y"]:
                    ezca["SUS-{0}_M1_OPTICALIGN_{1}_OFFSET".format(sus,dof)] += om_ctrl[ii]
                    ii+=1

            self.timer["offloading"] = tramp
            self.reset_tramp = True

        else:
            log("Control signals are small, no need to offload")
            self.timer["offloading"] = 0
            self.reset_tramp = False

#    @assert_mc_lock
#    @nodes.checker()
    def run(self):

        if not self.timer["offloading"]:
            notify("Offloading OMs")
            return

        if self.reset_tramp:
            # Change offsets back to 1 sec
            for sus in ["OM1","OM2","OM3"]:
                for dof in ["P","Y"]:
                    ezca["SUS-{0}_M1_OPTICALIGN_{1}_TRAMP".format(sus,dof)] = 1
            self.reset_tramp = False

        return True

##################################################
class RELOCATE_DARM_OFFSET(GuardState):
    index = 1050
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        tramp = 5.0
        ezca['OMC-READOUT_OFFSET_TRAMP'] = tramp
        ezca['LSC-DARM_TRAMP'] = tramp
        matrix.lsc_input_arm.TRAMP = tramp

        #Initial dcpd current setting.
        ezca['OMC-READOUT_OFFSET_OFFSET'] = 20.0
        # This mtrx element is reduced by half at the end of this state
        self.im4_trans_power = cdsutils.avg(1,'IMC-IM4_TRANS_SUM_OUTPUT',stddev=False)
        matrix.lsc_input_arm["DARM", "OMC_DC"] = ifoconfig.fringe_side * 0.088*math.sqrt(10.0/self.im4_trans_power)
        time.sleep(1)

        # Relocate DARM and load the new matrix number to compensate
        ezca.switch('LSC-DARM','OFFSET','OFF')
        ezca.switch('OMC-READOUT_OFFSET','OFFSET','ON')
        matrix.lsc_input_arm.load()
        self.timer['wait'] = tramp

        #If want to increase current past 20mA, set below to true. Compensates in the SIMPLE filter
        self.increase_current_again = True
        self.halve_omc_matrix = True
        self.dcpd_current = 50.0
        self.tramp2 = 5.0

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if any(matrix.lsc_input_arm.is_ramping()) or not self.timer['wait']:
            return

        if self.increase_current_again:
            ezca['OMC-READOUT_OFFSET_TRAMP'] = self.tramp2
            ezca['OMC-READOUT_SIMPLE_TRAMP'] = self.tramp2

            time.sleep(1)
            ezca['OMC-READOUT_OFFSET_OFFSET'] = self.dcpd_current
            ezca['OMC-READOUT_SIMPLE_GAIN'] = math.sqrt(20.0/self.dcpd_current) #Is 1.0 at 20mA
            #matrix.lsc_input_arm.load()
            self.timer['wait'] = self.tramp2 + 1
            self.increase_current_again = False
            return

        if self.halve_omc_matrix:
            matrix.lsc_input_arm["DARM", "OMC_DC"] = ifoconfig.fringe_side * 0.044*math.sqrt(10.0/self.im4_trans_power)
            time.sleep(0.3)
            matrix.lsc_input_arm.load()
            #JCB 20240507, added sleep to ensure we don't move on before ramp finishes
            time.sleep(0.3)
            self.halve_omc_matrix = False
            return

        return True

##################################################
class DC_READOUT(GuardState):
    index = 1100

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        ezca['OMC-ASC_MASTERGAIN']=0.555
        #Store the known good pzt offset
        ezca['OMC-STORE_PZT2_OFFSET'] = ezca['OMC-PZT2_OFFSET'] 

        # Turn on RH for 45W if this state or higher is requested
        #if ezca['GRD-ISC_LOCK_REQUEST_N'] >= 1194:
        #    nodes['TCS_RH'].set_request('READY_HIGHPOWER')

        # Engage DC-4 (AS-B) Centerring for squeezer later
        #TODO: Reload matrices file, uncomment below
        #matrix.asc_output_pit.zero(col='DC4')
        #matrix.asc_output_yaw.zero(col='DC4')
        #matrix.asc_output_pit['OMC_SUS_P','DC4'] = 1
        #matrix.asc_output_yaw['OMC_SUS_Y','DC4'] = 1

        ezca['ASC-OUTMATRIX_P_18_15']=0
        ezca['ASC-OUTMATRIX_P_19_15']=0 
        ezca['ASC-OUTMATRIX_P_20_15']=0 
        ezca['ASC-OUTMATRIX_P_26_15']=1 
        ezca['ASC-OUTMATRIX_Y_18_15']=0 
        ezca['ASC-OUTMATRIX_Y_19_15']=0 
        ezca['ASC-OUTMATRIX_Y_20_15']=0 
        ezca['ASC-OUTMATRIX_Y_26_15']=1

        ezca['ASC-DC4_P_GAIN'] =  3
        ezca['ASC-DC4_Y_GAIN'] =  -3
        ezca['ASC-DC4_P_TRAMP'] = 3
        ezca['ASC-DC4_Y_TRAMP'] = 3
        ezca['ASC-DC4_P_RSET'] = 2
        ezca['ASC-DC4_Y_RSET'] = 2

        ezca.switch('ASC-DC4_P','IN','ON') 
        ezca.switch('ASC-DC4_Y','IN','ON')

        

        if not sqzconfig.nosqz:
            nodes_loose['SQZ_MANAGER'] = 'FC_LOCKED'

        # Turn on Violin damping
        nodes_loose['SUS_1STVIOLIN'] = 'DAMP_1ST'
        #nodes_loose['SUS_2NDVIOLIN'] = 'DAMP_2ND' # VB20240423 should be in, but not sure if settings are all good

        # AJM20240320 new states for CO2 guardians
        if ifoconfig.run_co2:
            nodes_loose['TCS_ITMX_CO2'].set_request('SET_INTERMEDIATE_POWER')
            nodes_loose['TCS_ITMY_CO2'].set_request('SET_INTERMEDIATE_POWER')

    @assert_mc_lock
    def run(self):
        return True


##################################################
class DARM_TO_L2(GuardState):
    index = 1160
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # freeze SRC2 since we don't know why it oscillates AE 220516
        ezca.switch('ASC-SRC2_P','INPUT','OFF')
        ezca.switch('ASC-SRC2_Y','INPUT','OFF')

        tramp = 5.0
        matrix.lsc_input_arm.TRAMP = tramp
        ezca["LSC-DARM_TRAMP"] = tramp
        time.sleep(0.2)

        ezca["LSC-DARM_GAIN"] = -0.4    #
        time.sleep(tramp)

        #TODO: Move DARM TO L2 transition to run part of state

        #-----------------------------------------------------
        # Make sure the L2 stage that you want it turned on and that the other is turned off
        #for actuator in ["ETMX","ETMY"]:
        #    ezca['SUS-'+actuator+'_L2_LOCK_OUTSW_L'] = int(actuator in ifoconfig.darm_actuators)
        #TODO: Replace below with above
        #-----------------------------------------------------

        for esd in ['ETMX','ETMY']:
            ezca['SUS-' + esd + '_L3_DRIVEALIGN_L2L_TRAMP'] = 2
        #ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_TRAMP'] = 5
        time.sleep(0.2)

        #Boost L3 gain by 10% to help get through crossover (was 30% once upon a time, what changed?):
        for esd in ['ETMX','ETMY']:
            ezca["SUS-{}_L3_DRIVEALIGN_L2L_GAIN".format(esd)] *= 1.1
        time.sleep(3)

        #FIXME: If using both ESDs, gain can't be set this high
#        if ifoconfig.locking_esd == 'ETMX':
#            ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_GAIN'] = 1.3    #
#            time.sleep(3)

#        ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN'] = 1.4  #1.2 #AEPCAL, increased to get through
#        time.sleep(5)

        # Bring in L2
        ezca['SUS-'+ifoconfig.darm_actuator+'_L2_DRIVEALIGN_L2L_TRAMP'] = 10
        time.sleep(0.2)
        ezca.write("SUS-{}_L2_LOCK_OUTSW_L".format(ifoconfig.darm_actuator),1)
        ezca['SUS-'+ifoconfig.darm_actuator+'_L2_DRIVEALIGN_L2L_GAIN'] = -2.4
        #time.sleep(6)
        time.sleep(4) #AJM20220505 sped this up as high frequency noise was saturating L2.
        for actuator in ['ETMX','ETMY']:
            ezca.switch('SUS-{}_L2_LOCK_L'.format(actuator),'FM10','ON')    #This filter takes 3 seconds to turn on.
        ezca.switch('LSC-L2_LOCK','FM10','ON')

        notify("Bringing in L2")
        time.sleep(5)
        notify("Finished ramping, turning on FM8 and FM9")

        # Move integrator and boost from DARM filter to L2
        ezca.switch('LSC-DARM','FM10','FM4','OFF')
        
        # Switched to FM7 instead of FM8 (no integrator!)
        for actuator in ['ETMX','ETMY']:
            ezca.switch('SUS-{}_L2_LOCK_L'.format(actuator),'FM7','FM9','ON')
        ezca.switch('LSC-L2_LOCK','FM7','FM9','ON')

        # Calibration stuff (mirror the sus model to reduce spectral leackage at low frequencies SK)
        ezca['CAL-CS_DARM_FE_ETMY_L2_DRIVEALIGN_L2L_GAIN'] = -2.4
        ezca.switch('CAL-CS_DARM_FE_ETMY_L2_LOCK_L','FM10','ON') 
        ezca.switch('CAL-CS_DARM_FE_ETMY_L2_LOCK_L','FM7','FM9','ON')

        # set DARM bandwidth to 55 Hz
        ezca['LSC-DARM_TRAMP'] = 5
        time.sleep(0.1)
        ezca['LSC-DARM_GAIN'] = -0.6
        self.timer['wait'] = 5

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if not self.timer['wait']:
            return
        #clear SRC2 histories
        #ezca['ASC-SRC2_P_RSET'] = 2
        #ezca['ASC-SRC2_Y_RSET'] = 2
        return True

##################################################
class SPLIT_DARM_L2_DRIVE(GuardState):
    index = 1165
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        ezca.switch("SUS-ETMY_L2_LOCK_L","INPUT","OFF")
        ezca["SUS-ETMY_L2_LOCK_L_TRAMP"] = 0
        ezca["SUS-ETMY_L2_DRIVEALIGN_L2L_TRAMP"] = 0
        time.sleep(0.1)
        ezca["SUS-ETMY_L2_LOCK_L_GAIN"] = 0
        time.sleep(0.1)
        ezca["SUS-ETMY_L2_LOCK_L_RSET"] = 2
        time.sleep(0.1)
        ezca["SUS-ETMY_L1_LOCK_L_RSET"] = 2
        time.sleep(0.1)

        ezca.switch("SUS-ETMY_L2_LOCK_L","FM8","OFF","FM7","FM9","FM10","ON")
        ezca.switch("SUS-ETMY_L1_LOCK_L","FM8","OFF","FM2","FM9","ON")
        ezca["SUS-ETMY_L1_LOCK_L_GAIN"] = ifoconfig.l1_lock_l_gains['ETMY'] #0.9
        time.sleep(3)

        # Set initial turn on tramp, should be longer than cut-off (100s)
        t_ramp = 10 #150
        ezca["SUS-ETMY_L2_LOCK_L_TRAMP"] = t_ramp

        # Ensure switches and drive align are on
        ezca["SUS-ETMY_L2_LOCK_OUTSW_L"] = 1
        ezca["SUS-ETMY_L1_LOCK_OUTSW_L"] = 1
        ezca["SUS-ETMY_L2_DRIVEALIGN_L2L_GAIN"] = -2.4 
        time.sleep(0.1)

        self.split_darm = True
        #self.split_darm = False

        # Turn on with small gain first
        notify("Turning ETMY on with small gain")
        ezca.switch("SUS-ETMY_L2_LOCK_L","INPUT","ON")
        ezca["SUS-ETMY_L2_LOCK_L_GAIN"] = 0.01

        # Wait (in run) for gain to ramp up (and transient to settle)
        self.timer["wait"] = t_ramp + 1


    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            return

        if self.split_darm:
            # Set tramps
            t_ramp = 10
            for etm in ["X","Y"]:
                ezca["SUS-ETM{}_L2_LOCK_L_TRAMP".format(etm)] = t_ramp
            ezca["SUS-ETMX_M0_LOCK_L_TRAMP"] = t_ramp
            time.sleep(0.1)
            # Make the transition
            notify("Making the transition now")
            for etm in ["X","Y"]:
                ezca["SUS-ETM{}_L2_LOCK_L_GAIN".format(etm)] = 0.5
            # Double ETMX M0 gain to compensate for halved ETMX L1 gain
            ezca["SUS-ETMX_M0_LOCK_L_GAIN"] = 0.8
            self.timer["wait"] = t_ramp
            self.split_darm = False
            return

        return True

'''
class SPLIT_DARM_L2_DRIVE(GuardState):
    index = 1165
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        ezca['SUS-ETMY_L2_DRIVEALIGN_L2L_TRAMP'] = 0
        time.sleep(0.1)
        ezca['SUS-ETMY_L2_DRIVEALIGN_L2L_GAIN'] = -2.4 

        self.split_darm_via_lsc = True

        self.timer['wait'] = 1

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return

        if self.split_darm_via_lsc:
            t_ramp = 10
            matrix.lsc_l2_lock.TRAMP = t_ramp
            matrix.lsc_l2_lock['ETMY','DARM'] = -0.5
            matrix.lsc_l2_lock['ETMX','DARM'] = 0.5
            time.sleep(0.1)
            matrix.lsc_l2_lock.load()
            self.split_darm_via_lsc = False
            self.timer['wait'] = t_ramp
            return

        #if any(matrix.lsc_l2_lock.is_ramping()):
        #    notify('Still ramping')
        #    return

        return True

'''

##################################################
class SPLIT_DARM_L1_DRIVE(GuardState):
    index = 1170
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        ezca['SUS-ETMY_L1_LOCK_L_TRAMP'] = 0
        time.sleep(0.1)
        ezca['SUS-ETMY_L1_LOCK_L_GAIN'] = 0
        ezca["SUS-ETMY_L1_LOCK_L_RSET"] = 2
        time.sleep(2)

        # Turn on L1 ouput switch
        ezca['SUS-ETMY_L1_LOCK_OUTSW_L'] = 1

        self.split_darm_old_school = False
        self.split_darm_via_lsc = True

        # Wait
        self.timer['wait'] = 5


    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return

        if self.split_darm_old_school:
            # Set tramps
            t_ramp = 10
            for etm in ['X','Y']:
                ezca['SUS-ETM{}_L1_LOCK_L_TRAMP'.format(etm)] = t_ramp
            ezca['SUS-ETMX_M0_LOCK_L_TRAMP'] = t_ramp
            time.sleep(0.1)
            # Make the transition
            notify('Making the transition now')
            for etm in ['X','Y']:
                ezca['SUS-ETM{}_L1_LOCK_L_GAIN'.format(etm)] = 0.5*ifoconfig.l1_lock_l_gains['ETM{}'.format(etm)]
            # Double ETMX M0 gain to compensate for halved ETMX L1 gain
            ezca['SUS-ETMX_M0_LOCK_L_GAIN'] *= 2
            self.timer['wait'] = t_ramp
            self.split_darm_old_school = False
            return

        if self.split_darm_via_lsc:
            t_ramp = 10
            matrix.lsc_l1_lock.TRAMP = t_ramp
            matrix.lsc_l1_lock['ETMY','DARM'] = -0.5
            matrix.lsc_l1_lock['ETMX','DARM'] = 0.5
            time.sleep(0.1)
            matrix.lsc_l1_lock.load()
            self.split_darm_via_lsc = False
            self.timer['wait'] = t_ramp
            return

        #if any(matrix.lsc_l1_lock.is_ramping()):
        #    notify('Still ramping')
        #    return

        return True


##################################################
#class WAIT_FOR_RH(GuardState):
#    index = 1194
#    request = False
#    @assert_mc_lock
#    @nodes.checker()
#    # state disabled AE 20181003
#   def main(self):
#        if ezca['GRD-TCS_RH_REQUEST_N'] != 15:
#            nodes['TCS_RH'].set_request('READY_HIGHPOWER')        
#    def run(self):
#         return True
#        if not (nodes['TCS_RH'].arrived): # Wait for PI dynamic thermal compensation #CB 20180214
#            log('Waiting for ring heater for parametric instability mitigation')
#            return
#        nodes['TCS_RH'].set_request('FINAL_HIGHPOWER')
##################################################

def srasc_juggle(high_sr,tramp=5):

    for dof1 in ['1','2']:
        for dof2 in ['P','Y']:
            ezca.write("ASC-SRCL{}_{}_PRE_TRAMP".format(dof1,dof2),tramp)
    time.sleep(0.5)

    if high_sr == 'srm':
        ezca['ASC-SRCL1_P_PRE_GAIN'] = 0.05
        ezca['ASC-SRCL1_Y_PRE_GAIN'] = 0.05
        ezca['ASC-SRCL2_P_PRE_GAIN'] = 1.0
        ezca['ASC-SRCL2_Y_PRE_GAIN'] = 1.0

    if high_sr == 'sr2':
        ezca['ASC-SRCL1_P_PRE_GAIN'] = 1.0
        ezca['ASC-SRCL1_Y_PRE_GAIN'] = 1.0
        ezca['ASC-SRCL2_P_PRE_GAIN'] = 0.3  
        ezca['ASC-SRCL2_Y_PRE_GAIN'] = 0.3


###############################


class SWITCH_SRC2_SENSOR(GuardState):
    index = 1190
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # Switch SRM alignment to 72MHz signals
        ezca.switch('ASC-SRC2_P','INPUT','OFF')
        ezca.switch('ASC-SRC2_Y','INPUT','OFF')
        matrix.asc_input_pit['SRC2','AS_A_RF36_I'] = 0 # will be reset in down, HY, CB 20181114
        matrix.asc_input_yaw['SRC2','AS_A_RF36_I'] = 0 
        matrix.asc_input_pit['SRC2','AS_A_RF72_Q'] = -70 # Has a 119 sec time constant
        matrix.asc_input_yaw['SRC2','AS_A_RF72_Q'] = -70 # Has a 38 sec time constant
        time.sleep(2)
        ezca.switch('ASC-SRC2_P','INPUT','ON')
        ezca.switch('ASC-SRC2_Y','INPUT','ON')
        # Set SRC Alignment offsets (to optimise coupled cavity pole)
        #ezca['ASC-SRC2_P_OFFSET'] = 0 #10
        #ezca['ASC-SRC2_Y_OFFSET'] = 0

        # Lower SR2 ASC gains, increases SRM ASC gains    
        srasc_juggle('srm',tramp=3)


        self.timer["wait"] = 5

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            return

        return True

##################################################

class POWER_UP_25W(GuardState):
    index = 1195
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # make sure IMC guardian is still managed        
        #nodes.set_managed(MANAGED_NODES)
        nodes.set_managed()        #newnodes

        #Turn off Spot Centering Loops
        ezca['ASC-ADS_GAIN'] = 0

        # Increase CSOFT gain to control the 0.5 Hz oscillation MK 20181102
        ezca['ASC-CSOFT_P_GAIN'] = 0.65 #0.52 

        # Increase Fast Shutter Trigger from 1V to 1.5V
        ezca['SYS-MOTION_C_SHUTTER_G_THRESHOLD'] = 1.5

        # Redistribute MC servo gains, and boost CM AO gain by 3dB
        gain_offload = 12
        gain_boost = 3

        self.step_gains = [
                    cdsutils.Step(ezca, 'IMC-REFL_SERVO_IN1GAIN', '-1.0,{}'.format(gain_offload+gain_boost), time_step=0.5),
                    cdsutils.Step(ezca, 'IMC-REFL_SERVO_FASTGAIN', '+1.0,{}'.format(gain_offload+gain_boost), time_step=0.5),
                    cdsutils.Step(ezca, 'IMC-REFL_SERVO_IN2GAIN', '-1.0,{}'.format(gain_offload), time_step=0.5),
                    ]

        # Power up 25 W
        self.power25 = 28   #25
        ezca['PSL-GUARD_POWER_REQUEST']  = self.power25
        ezca['PSL-GUARD_STEP_REFL_GAIN'] = 1
        ezca['PSL-GUARD_STEP_DC_GAIN']   = 1
        time.sleep(0.5)
        #nodes['IMC_LOCK'].set_request('IFO_POWER')
        #time.sleep(2)

        self.boost_lscmcl = True
        self.power_up = True
        self.set_iss = True

        self.change_sr2m2g = False
        self.sr2m2g = 1.3

        #ezca['CAM-ETMY_SCATTER_EXP']=800 #CB20190410 to capture scatter power up images

        self.timer['wait'] = 1


    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return

        # Redistribute MC servo gains, and boost CM AO gain by 3dB
        done_stepping = True
        for step in self.step_gains:
            done_stepping &= step.step()
        if not done_stepping:
            time.sleep(0.5)
            return

        # Boosts LSC-MCL gain by 2
        if self.boost_lscmcl:
            matrix.lsc_input['MCL','REFLSERVO_SLOW'] *= 2
            matrix.lsc_input.TRAMP = 5
            time.sleep(0.1)
            matrix.lsc_input.load()
            self.boost_lscmcl = False
            self.timer['wait'] = 5
            return


        if self.power_up:
            nodes['IMC_LOCK'].set_request('IFO_POWER')
            time.sleep(2)
            self.power_up = False

        #if not (nodes['IMC_LOCK'].arrived and nodes['IMC_LOCK'].done):
        if not (nodes['IMC_LOCK'].arrived and nodes['IMC_LOCK'].done and (ezca['IMC-IM4_TRANS_SUM_OUTPUT'] > (0.9*self.power25))):
            notify('Still powering up')
            return

        # Set ISS 2nd loop gain (30dB for 25W)
        if self.set_iss:
            ezca['PSL-ISS_SECONDLOOP_GAIN'] = 30    #30 #28 ajm20190820 increased by 2dB to crush high frequency darm more.
            self.set_iss = False

        # Lower SR2 crossover
        if self.change_sr2m2g:
            ezca['SUS-SR2_M2_LOCK_L_GAIN'] = self.sr2m2g
            self.change_sr2m2g = False

        #AJM20230110 cdsutils can no longer look in the past
        prg = cdsutils.avg(10,'LSC-PRC_GAIN_MON',stddev=True)
        if prg[1] > 0.2:
            notify('Waiting for PRG to settle')
            return

        #ezca['ASC-ADS_GAIN'] = 0.1

        return True


####################################################################################
# Obsolete state
'''
class CENTER_SPOTS_WITH_DITHERS(GuardState):
    index = 1197
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):


        # Turn on SRC2 ASC (SRM) loop, lower SRC1
        #srasc_juggle('srm',tramp=5)

        #ezca['SUS-ETMY_L2_DRIVEALIGN_P2L_GAIN'] = 1.0

        ezca['CAM-ETMX_EXP']=100000
        ezca['CAM-ETMY_EXP']=100000
        ezca['CAM-BS_EXP']=50000

        # Engage oscilators for these degrees of freedom
        for dof in ['PIT1','PIT3','PIT4','YAW1','YAW3','YAW4']:      
            #ezca['ASC-ADS_' + dof + '_OSC_TRAMP'] = 3  # Leave TRAMPs to SDF
            #time.sleep(0.3)
            ezca['ASC-ADS_' + dof + '_OSC_CLKGAIN'] = ifoconfig.quad_clk_gain
            #ezca['ASC-ADS_' + dof + '_OSC_SINGAIN'] = ifoconfig.quad_sin_gain
            #ezca['ASC-ADS_' + dof + '_OSC_COSGAIN'] = ifoconfig.quad_cos_gain

        # Turn filter inputs and outputs on 
        for dof in ['PIT1','PIT3','PIT4','YAW1','YAW3','YAW4']:
            ezca.switch('ASC-ADS_' + dof + '_DOF', 'INPUT', 'ON')
            ezca.switch('ASC-ADS_' + dof + '_DOF', 'OUTPUT', 'ON')

        ezca['ASC-ADS_GAIN'] = 0.04 
        self.timer['ads'] = 40

        #Thresholds
        #self.prg_threshold = 45
        self.prg_threshold = 40
        self.spot_threshold = 0.3

        #Channels to check
        self.chans = ['LSC-PRC_GAIN_MON']
        for dof in ['PIT3','PIT4','YAW3','YAW4']:
            self.chans.append("ASC-ADS_{}_DEMOD_I_OUTPUT".format(dof))

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        avgs = cdsutils.avg(10,self.chans,stddev=True)
        prg = avgs[0]
        xspotp = avgs[1]
        yspotp = avgs[2]
        xspoty = avgs[3]
        yspoty = avgs[4]

        #time.sleep(10)
#        prg = cdsutils.avg(-10,'LSC-PRC_GAIN_MON',stddev=True)
#        xspotp = cdsutils.avg(-10,'ASC-ADS_PIT3_DEMOD_I_OUTPUT',stddev=True)
#        yspotp = cdsutils.avg(-10,'ASC-ADS_PIT4_DEMOD_I_OUTPUT',stddev=True)
#        xspoty = cdsutils.avg(-10,'ASC-ADS_YAW3_DEMOD_I_OUTPUT',stddev=True)
#        yspoty = cdsutils.avg(-10,'ASC-ADS_YAW4_DEMOD_I_OUTPUT',stddev=True)

        if (prg[0] < self.prg_threshold or prg[1] > 0.2 or
        abs(xspotp[0]) > self.spot_threshold or
        abs(yspotp[0]) > self.spot_threshold or
        abs(xspoty[0]) > self.spot_threshold or
        abs(yspoty[0]) > self.spot_threshold):
            notify("Waiting for spots to center within {0} and PRG to settle higher than {1}".format(self.spot_threshold,self.prg_threshold))
            return
        return True
'''



##################################################
#TODO: This state spends too long in the main state, move code to run state
class ASC_HARD_25W(GuardState):
    index = 1198
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # If spot centerring is on, turn it off CB 2019 01 30
        ezca['ASC-ADS_GAIN'] = 0.0
        time.sleep(1)
        
        #Increase SRC1 gain to stabilise cavity pole #CTMOS
        #ezca['ASC-SRC1_P_GAIN'] = 10000

        #ezca['ASC-SRCL1_P_PRE_GAIN'] = 2.5  #AJM20190715 think we should change pregains at these points. SRC1_P used to be increased from 4000 to 10000

        ezca['ASC-CHARD_P_PRE_TRAMP'] = 2
        ezca['ASC-DHARD_P_PRE_TRAMP'] = 2
        time.sleep(1)
        # Reduce CHARD P GAIN (set UGF to 2.7 Hz)
        ezca['ASC-CHARD_P_PRE_GAIN'] = 0.3 #0.5    #AJM20230117 lowered because we rephased REFL9 WFS
        ezca['ASC-CHARD_Y_PRE_GAIN'] = 0.2     #Moved from Power Up Final
        ezca['ASC-DHARD_P_PRE_GAIN'] = 0.4       
        ezca['ASC-DHARD_Y_PRE_GAIN'] = 0.4       
        time.sleep(2)
        #self.timer["wait"] = 2

        self.t_ramp = 2

        #prep for transition
        for dof1 in ["CHARD","DHARD"]:
            for dof2 in ["P","Y"]:
                 ezca.switch("ASC-{}_{}_B".format(dof1,dof2),"FM1","OFF","FM2","ON")
                 #Set tramps for transition
                 ezca.write("ASC-{}_{}_A_TRAMP".format(dof1,dof2),self.t_ramp)
                 ezca.write("ASC-{}_{}_B_TRAMP".format(dof1,dof2),self.t_ramp)

        self.dofs = ['CHARD_Y','DHARD_Y','CHARD_P','DHARD_P']

        self.switch_hard = True    #Set to true to do the filter transition in the run()
        self.switch_resg_off = True
        self.switch_A_to_25W = True
        self.switch_to_A = True
        self.finalize_asc = True
        self.ii = 0

        if ifoconfig.cam_spot_centering:
            self.wait_for_cams = True   #Set this to false if you don't want to wait for camera centering

            self.chans = []
            for dof in ['PIT','YAW']:
                for n in ['13','14','15']:
                    self.chans.append('ASC-ADS_{}{}_DOF_OUTPUT'.format(dof,n))

            self.thresholds = {'PIT13':100, 'PIT14':200, 'PIT15':6e-3,
                               'YAW13':100, 'YAW14':200, 'YAW15':6e-3}

            #self.cam_spots = EzAvg(ezca,20,self.chans)

        else:
            self.wait_for_cams = False


        self.timer["wait"] = 1

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            return

        if self.ii<len(self.dofs) and self.switch_hard:
            if self.switch_resg_off:
                ezca.switch('ASC-'+self.dofs[self.ii], 'FM3','OFF')
                self.timer["wait"] = 3
                self.switch_resg_off = False
                self.switch_25W_on = True
                return

            #if self.switch_25W_on:
            #    #ezca.switch('ASC-'+self.dofs[self.ii], 'FM8','ON')
            #    self.timer["wait"] = 3
            #    self.switch_resg_off = True
            #    self.switch_25W_on = False
            #    self.ii+=1    
            #    return

            if self.switch_25W_on:
                ezca.write("ASC-{}_A_GAIN".format(self.dofs[self.ii]), 0)
                ezca.write("ASC-{}_B_GAIN".format(self.dofs[self.ii]), 1)
                self.timer["wait"] = self.t_ramp
                self.switch_resg_off = True
                self.switch_25W_on = False
                self.ii+=1    
                return


        if self.switch_A_to_25W:
            notify("Switching A bank from 10 to 25 W")
            for dof in self.dofs:
                ezca.switch("ASC-{}_A".format(dof),"FM1","FM2","OFF","FM3","ON")
            self.switch_A_to_25W = False
            self.timer["wait"] = 2
            return

        if self.switch_to_A:
            notify("Switching back to A banks")
            for dof in self.dofs:
                ezca.write("ASC-{}_A_GAIN".format(dof), 1)
                ezca.write("ASC-{}_B_GAIN".format(dof), 0)
            self.switch_to_A = False
            self.timer["wait"] = self.t_ramp
            return

        # Make final ASC changes here (except for cut-offs)
        if self.finalize_asc:

            # Setting SOFT gains to the values found when measuring loops at 28W AJM20181220
            ezca['ASC-CSOFT_P_GAIN'] = 0.65  
            ezca['ASC-DSOFT_P_GAIN'] = 0.9 

            self.finalize_asc = False
            return

        if self.wait_for_cams:
            #[done,vals] = self.cam_spots.ezAvg()
            #if not done:
            #    return
            vals = cdsutils.avg(20,self.chans) #AJM240222 increased averaging time to 20s
            jj = 0
            for dof in ['PIT','YAW']:
                for n in ['13','14','15']:
                    if abs(vals[jj])>self.thresholds['{}{}'.format(dof,n)]:
                        notify('Waiting on spots to center')
                        return
                    jj+=1
            log('Spots centered well enough, move on to power up')
            self.wait_for_cams = False

        return True

##################################################
#New state to allow for power up past 50W later on

class POWER_UP_45W(GuardState):
    index = 1200
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        ezca['CAM-ETMX_EXP']=80000
        ezca['CAM-ETMY_EXP']=80000
        ezca['CAM-BS_EXP']=40000

        ezca.switch('ASC-SRC2_P','INPUT','OFF')
        ezca.switch('ASC-SRC2_Y','INPUT','OFF')

        # Power up 
        ezca['PSL-GUARD_POWER_REQUEST']  = 45
        ezca['PSL-GUARD_STEP_REFL_GAIN'] = 1
        ezca['PSL-GUARD_STEP_DC_GAIN']   = 1
        time.sleep(0.5)

        self.power_up = True

        self.turn_on_src2asc = True

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if self.power_up:
            self.power_up = False
            nodes['IMC_LOCK'].set_request('IFO_POWER')
            time.sleep(2)
            return

        if not (nodes['IMC_LOCK'].arrived and nodes['IMC_LOCK'].done):
            notify('Powering up')
            return

        if self.turn_on_src2asc:
            ezca.switch('ASC-SRC2_P','INPUT','ON')
            ezca.switch('ASC-SRC2_Y','INPUT','ON')
            self.turn_on_src2asc = False
            return

        return True

##################################################
class ASC_HIGH_POWER(GuardState):
    index = 1290
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # Turn off INP2
        ezca.switch('ASC-INP2_P','INPUT','OFF')
        ezca.switch('ASC-INP2_Y','INPUT','OFF')
        time.sleep(0.1)

        ezca['ASC-INP2_P_PRE_GAIN'] = 1.0
        ezca['ASC-INP2_P_PRE_GAIN'] = 1.0

        # Set matrix to send POP A QPDs to INP2 (IM4)
        matrix.asc_input_pit.zero(row='INP2')
        matrix.asc_input_yaw.zero(row='INP2')
        time.sleep(0.1)
        #matrix.asc_input_pit['INP2','POP_A_DC'] = -100
        #matrix.asc_input_yaw['INP2','POP_A_DC'] = -100

        # Set POP A QPD offsets
        pop_offsets = {'PIT':0.201,'YAW':0.175}
        for dof in ['PIT','YAW']:
            ezca['ASC-POP_A_{}_OFFSET'.format(dof)] = pop_offsets[dof]
            ezca.switch('ASC-POP_A_{}'.format(dof),'OFFSET','ON')
        time.sleep(0.1)
        # Raise DC3 yaw gain for 2.7 oscillation AE240811
        ezca['ASC-DC3_Y_PRE_GAIN'] = 1.25

        # Turn INP2 servos back on
        #ezca.switch('ASC-INP2_P','INPUT','ON')
        #ezca.switch('ASC-INP2_Y','INPUT','ON')

        self.turn_on_im4_ads = True

        #------------------------------------------------------
        ### Transition hard yaw from 100k to 200k

        # DOFs to transition
        self.dofs = ['CHARD_Y','DHARD_Y']
        # Transition time
        self.tramp_hard = 3

        #Prep filter banks
        for dof in self.dofs:
            ezca.switch("ASC-{}_B".format(dof),"FMALL","OFF")
            ezca.switch("ASC-{}_B".format(dof),"FM3","ON")
            #Set tramps for transition
            ezca.write("ASC-{}_A_TRAMP".format(dof),self.tramp_hard)
            ezca.write("ASC-{}_B_TRAMP".format(dof),self.tramp_hard)

        # Set this flag to False if don't want to transition
        self.switch_hard_to_200k = True

        self.ii = 0

        # Wait for spots to center better?
        if ifoconfig.cam_spot_centering:
            self.wait_for_cams = True   #Set this to false if you don't want to wait for camera centering

            self.cam_chans = []
            for dof in ['PIT','YAW']:
                for n in ['13','14','15']:
                    self.cam_chans.append('ASC-ADS_{}{}_DOF_OUTPUT'.format(dof,n))

            self.thresholds = {'PIT13':100, 'PIT14':100, 'PIT15':1.5e-2,
                               'YAW13':100, 'YAW14':100, 'YAW15':1.5e-2}

            self.cam_spots = EzAvg(ezca,20,self.cam_chans)

        else:
            self.wait_for_cams = False


        #Instantiate timer
        self.timer['wait'] = 0


    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return


        if self.switch_hard_to_200k and self.ii<len(self.dofs):

            notify("Switching {} A to B".format(self.dofs[self.ii]))
            ezca.write("ASC-{}_A_GAIN".format(self.dofs[self.ii]), 0)
            ezca.write("ASC-{}_B_GAIN".format(self.dofs[self.ii]), 1)
            ezca['ASC-CHARD_Y_PRE_GAIN'] = 0.03
            ezca['ASC-DHARD_Y_PRE_GAIN'] = 0.2
            ezca['ASC-CSOFT_Y_GAIN'] = 0.2 #0.4 20241029 AE/VF #0.8 20240221
            self.timer['wait'] = self.tramp_hard
            self.ii += 1
            return


        if self.turn_on_im4_ads:
            bank = '6'
            im4_freq = {'PIT':5.4,'YAW':6.8}
            for dof in ['PIT','YAW']:
                turn_on_dither('IM4',dof,'OSC'+bank,im4_freq[dof],0.3,sin_gain=1,cos_gain=1,tramp=3)
            matrix.asc_ads_input_pit.zero(row='PIT{}'.format(bank))
            matrix.asc_ads_input_yaw.zero(row='YAW{}'.format(bank))
            time.sleep(0.1)
            #TODO restart guardian to import matrices file
            #matrix.asc_ads_input_pit['PIT{}'.format(bank),'REFL_A_LF'] = 1
            #matrix.asc_ads_input_yaw['YAW{}'.format(bank),'REFL_A_LF'] = 1
            ezca['ASC-ADS_PIT_SEN_MTRX_6_7'] = 1
            ezca['ASC-ADS_YAW_SEN_MTRX_6_7'] = 1
            matrix.asc_ads_output_pit['IM4','PIT{}'.format(bank)] = 1
            matrix.asc_ads_output_yaw['IM4','YAW{}'.format(bank)] = 1
            #ezca.switch('ASC-ADS_PIT6_DEMOD_SIG','FM2','OFF') #CB added bank 6 being tested TURN_ON_DHARD 
            #ezca.switch('ASC-ADS_PIT6_DEMOD_SIG','FM6','ON')
            #ezca.switch('ASC-ADS_YAW6_DEMOD_SIG','FM2','OFF')
            #ezca.switch('ASC-ADS_YAW6_DEMOD_SIG','FM6','ON')

            ezca.switch('ASC-ADS_PIT6_DOF','FM1','FM6','ON')
            ezca.switch('ASC-ADS_YAW6_DOF','FM1','FM6','ON')
            ezca.switch('ASC-ADS_PIT6_DOF','INPUT','OUTPUT','ON')
            ezca.switch('ASC-ADS_YAW6_DOF','INPUT','OUTPUT','ON')
            ezca['ASC-ADS_PIT6_DOF_GAIN'] = 2
            ezca['ASC-ADS_YAW6_DOF_GAIN'] = 2
            ezca['ASC-ADS_GAIN'] = 3
            self.turn_on_im4_ads = False


        if self.wait_for_cams:
            [done,vals] = self.cam_spots.ezAvg()
            if not done:
                return

            jj = 0
            for dof in ['PIT','YAW']:
                for n in ['13','14','15']:
                    if abs(vals[jj])>self.thresholds['{}{}'.format(dof,n)]:
                        notify('Waiting on spots to center')
                        return
                    jj+=1
            log('Spots centered well enough, move on to power up')
            self.wait_for_cams = False

        return True


##################################################
class POWER_UP_FINAL(GuardState):
    index = 1300
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        ezca.switch('ASC-SRC2_P','INPUT','OFF')
        ezca.switch('ASC-SRC2_Y','INPUT','OFF')

        # Power up
        #self.powers = [45]
        self.powers = [56.7] 
        #self.powers = [63.7] 
        #self.powers = [71.5] 
        #self.powers = [56.7, 63.7]
        #self.powers = [56.7, 63.7, 71.5]

        ezca['PSL-GUARD_STEP_REFL_GAIN'] = 1
        ezca['PSL-GUARD_STEP_DC_GAIN']   = 1
        time.sleep(0.5)

        self.set_power = True
        self.power_up = True
        self.wait = True
        self.ii = 0

        self.turn_on_src2asc = True
        self.center_spots_again = False

        self.finalize_co2 = ifoconfig.run_co2

        #------------------------------------------------------------------------------------------------
        ## AJM240802 - New code for testing, also below in run loop
        # Wait for spots to center better?
        if ifoconfig.cam_spot_centering:
            self.wait_for_cams = True   #Set this to false if you don't want to wait for camera centering
        
            self.cam_chans = []
            for dof in ['PIT','YAW']:
                for n in ['13','14','15']:
                    self.cam_chans.append('ASC-ADS_{}{}_DOF_OUTPUT'.format(dof,n))
        
            self.thresholds = {'PIT13':100, 'PIT14':100, 'PIT15':1.5e-2,
                               'YAW13':100, 'YAW14':100, 'YAW15':1.5e-2}

            #self.thresholds = {'PIT13':50, 'PIT14':50, 'PIT15':0.75e-2,
            #                   'YAW13':50, 'YAW14':50, 'YAW15':0.75e-2}
        
            self.cam_spots = EzAvg(ezca,20,self.cam_chans)
        
        else:
            self.wait_for_cams = False
        #------------------------------------------------------------------------------------------------

        self.timer['wait'] = 5

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return

        if self.set_power:
            ezca['PSL-GUARD_POWER_REQUEST']  = self.powers[self.ii]
            log('Setting power to {}'.format(self.powers[self.ii]))
            self.set_power = False
            self.power_up = True
            self.timer['wait'] = 1
            return

        if self.power_up:
            #Turn off ASC SRC2 off first
            ezca.switch('ASC-SRC2_P','INPUT','OFF')
            ezca.switch('ASC-SRC2_Y','INPUT','OFF')
            #Tell IMC Guard to Power up
            nodes['IMC_LOCK'].set_request('FINAL_IFO_POWER')
            log('Power up step {}'.format(self.ii+1))
            self.power_up = False
            #self.wait = True   #AJM240802 see below
            if ifoconfig.cam_spot_centering:
                self.wait_for_cams = True
            self.switch_src2_on = True
            self.timer['wait'] = 2
            return

        if not (nodes['IMC_LOCK'].arrived and nodes['IMC_LOCK'].done):
            notify('Powering up')
            return

        if self.switch_src2_on:
            ezca.switch('ASC-SRC2_P','INPUT','ON')
            ezca.switch('ASC-SRC2_Y','INPUT','ON')
            self.switch_src2_on = False

        #------------------------------------------------------------------
        ## AJM240802 - New code for testing
        if self.wait_for_cams:
            [done,vals] = self.cam_spots.ezAvg()
            if not done:
                return

            jj = 0
            for dof in ['PIT','YAW']:
                for n in ['13','14','15']:
                    if abs(vals[jj])>self.thresholds['{}{}'.format(dof,n)]:
                        notify('Waiting on spots to center')
                        return
                    jj+=1
            log('Spots centered well enough, move on')
            self.cam_spots.clear_buff()
            self.wait_for_cams = False

        if self.ii < len(self.powers)-1:
            self.set_power = True
            self.ii+=1
            return
        #------------------------------------------------------------------
        ## AJM240802 - Incompatable with new code above
        #if self.wait:
        #    # reset the timer to wait before next power up
        #    if self.ii < len(self.powers)-1:
        #        self.timer['wait'] = 15 #JCB20240510 decreased to 15 after PSL guardian change #30, Increased VB20240427.
        #        self.set_power = True
        #        self.ii+=1
        #    self.wait = False
        #    return

        #-----------------------------------------------------------------

        #ezca.switch('ASC-SRC2_P','INPUT','ON')
        #ezca.switch('ASC-SRC2_Y','INPUT','ON')

        if self.center_spots_again:
            ezca['ASC-ADS_GAIN'] = 0.04
            self.center_spots_again = False

        if self.finalize_co2:
            # Set CO2 lasers to final power
            nodes_loose['TCS_ITMX_CO2'].set_request('SET_FINAL_POWER')
            nodes_loose['TCS_ITMY_CO2'].set_request('SET_FINAL_POWER')
            self.finalize_co2 = False

        return True

##################################################
class CLOSE_CORNER_DIVERTERS(GuardState):
    index = 1400
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        #if ifoconfig.run_tcs:
        #    nodes_loose['TCS_RH'].set_request('FINAL_HIGHPOWER')

        ezca['CAM-ETMX_EXP']=60000
        ezca['CAM-ETMY_EXP']=60000
        ezca['CAM-BS_EXP']=30000

        # We trigger DRMI on SPOP18, hold it, changed AE since we don't close POP anymore 20230413
        #ezca.switch('LSC-POPAIR_B_RF18_I','HOLD','ON')
        #ezca.switch('LSC-POPAIR_B_RF18_Q','HOLD','ON')
        # Instead of holding SPOP18, we can change the trigger level below zero. AJM 20170822
        ezca['LSC-MICH_TRIG_THRESH_ON'] = -1000
        ezca['LSC-PRCL_TRIG_THRESH_ON'] = -1000
        ezca['LSC-SRCL_TRIG_THRESH_ON'] = -1000
        time.sleep(2)

#        # turn on BounceRoll notches in L2 of TMs since they ring up when we step the power
#        ezca.switch('SUS-ETMX_L2_LOCK_P','FM8','ON')
#        ezca.switch('SUS-ETMY_L2_LOCK_P','FM8','ON')
#        ezca.switch('SUS-ITMX_L2_LOCK_P','FM8','ON')
#        ezca.switch('SUS-ITMY_L2_LOCK_P','FM8','ON')
#        ezca.switch('SUS-ETMX_L2_LOCK_Y','FM8','ON')
#        ezca.switch('SUS-ETMY_L2_LOCK_Y','FM8','ON')
#        ezca.switch('SUS-ITMX_L2_LOCK_Y','FM8','ON')
#        ezca.switch('SUS-ITMY_L2_LOCK_Y','FM8','ON')

        # Close POP, REFL, AS beam diverters
        #ezca['SYS-MOTION_C_BDIV_A_CLOSE'] = 1 # Pop diverter !!DONT FORGET to uncomment above the hold of POP18
        ezca['SYS-MOTION_C_BDIV_B_CLOSE'] = 1 # Refl diverter
        #ezca['SYS-MOTION_C_BDIV_D_CLOSE'] = 1 # AS diverter
        time.sleep(2)

        ezca['LSC-DARM_GAIN'] = -0.8

        if ifoconfig.cam_spot_centering:
            for dof in ['PIT1','PIT3','PIT4','YAW1','YAW3','YAW4']:
                ezca['ASC-ADS_'+ dof +'_OSC_CLKGAIN'] = 0

        if not ifoconfig.cam_spot_centering:
            ezca['ASC-ADS_GAIN'] = 0.04 
        #time.sleep(5)

        ezca.switch('ASC-SRC2_P','INPUT','ON')
        ezca.switch('ASC-SRC2_Y','INPUT','ON')

        self.spot_threshold = 0.35

    @assert_mc_lock
    @nodes.checker()
    def run(self):  
 
        return True

        # This code isn't running right now due to return True above
        time.sleep(5)
        xspotp = cdsutils.avg(-10,'ASC-ADS_PIT3_DEMOD_I_OUTPUT',stddev=True)
        yspotp = cdsutils.avg(-10,'ASC-ADS_PIT4_DEMOD_I_OUTPUT',stddev=True)
        xspoty = cdsutils.avg(-10,'ASC-ADS_YAW3_DEMOD_I_OUTPUT',stddev=True)
        yspoty = cdsutils.avg(-10,'ASC-ADS_YAW4_DEMOD_I_OUTPUT',stddev=True)
        if (abs(xspotp[0]) > self.spot_threshold or
        abs(yspotp[0]) > self.spot_threshold or
        abs(xspoty[0]) > self.spot_threshold or
        abs(yspoty[0]) > self.spot_threshold):
            notify("Waiting for spots to center within {0}".format(self.spot_threshold))
            return
        return True


##################################################
class SWITCH_TO_LOWNOISE_ESD(GuardState):
    index = 1500
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        t_transition = 30
        self.error = False

        #ezca['SUS-ETMY_L3_LOCK_BIAS_TRAMP'] = 10
        ezca['SUS-{}_L3_LOCK_BIAS_TRAMP'.format(ifoconfig.lownoise_esd)] = 10
        #ezca['SUS-ETMY_L3_LOCK_BIAS_OFFSET'] = ifoconfig.esdy_sign_flip * 100
        ezca['SUS-{}_L3_LOCK_BIAS_OFFSET'.format(ifoconfig.lownoise_esd)] = ifoconfig.esdy_sign_flip * 100
        time.sleep(10.3)

        # Set TM ramp times
        for quad in ['ETMX','ETMY']:
            ezca['SUS-{}_L3_DRIVEALIGN_L2L_TRAMP'.format(quad)] = t_transition
            ezca['SUS-{}_L3_LOCK_BIAS_TRAMP'.format(quad)] = 30
        time.sleep(0.3)

        # Make the transition
        if ifoconfig.lownoise_esd == 'ETMY':
            ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN'] = ifoconfig.esdy_sign_flip * ifoconfig.esd_lownoise_l2lgain['ETMY']
            ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_GAIN'] = 0
        elif ifoconfig.lownoise_esd == 'ETMX':
            ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_GAIN'] = ifoconfig.esdy_sign_flip * ifoconfig.esd_lownoise_l2lgain['ETMX']
            ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN'] = 0
        else:
            self.error = True

        ezca['CAL-PCALX_DARM_GAIN'] = 0
        ezca['CAL-PCALY_DARM_GAIN'] = 0
        #time.sleep(10)
        self.timer['esd switch'] = t_transition + 2

#        # AEPCAL (comment below and uncomment above)
#        # set tramps
#        ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_TRAMP'] = 30
#        ezca['CAL-PCALX_DARM_TRAMP'] = 30

#        # Increase L2 gain
#        ezca['SUS-ETMY_L2_LOCK_L_GAIN'] = 1.25
#        time.sleep(2)
#        ezca.switch('SUS-ETMY_L2_LOCK_L','FM7','ON')
#        time.sleep(5)
#        ezca.switch('SUS-ETMY_L2_LOCK_L','FM3','ON')
#        time.sleep(3)

#        # Transition to PCAL
#        ezca['CAL-PCALX_DARM_GAIN'] = -4.8  #-3.77 #for low noise?
#        ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN'] = 0     
#        time.sleep(31)
#        
#        # Transition ETMY HV to LV
#        # ramp down bias, wait for long time for filters to settle
#        ezca['SUS-ETMY_L3_LOCK_BIAS_TRAMP'] = 30
#        ezca['SUS-ETMY_L3_LOCK_BIAS_OFFSET'] = -100
#        time.sleep(40)
#        # set quadrants to 0 to avoid having to go to zero bias
#        # just to turn off linearization
#        ezca['SUS-ETMY_L3_ESDOUTF_UL_GAIN'] = 0
#        ezca['SUS-ETMY_L3_ESDOUTF_UR_GAIN'] = 0
#        ezca['SUS-ETMY_L3_ESDOUTF_LL_GAIN'] = 0
#        ezca['SUS-ETMY_L3_ESDOUTF_LR_GAIN'] = 0
#        time.sleep(11)

#        # linearization off
#        ezca['SUS-ETMY_L3_ESDOUTF_LIN_BYPASS_SW'] = 1
#        time.sleep(1)
#        # low volts
#        ezca['SUS-ETMY_BIO_L3_LL_VOLTAGE_SW'] = 0
#        ezca['SUS-ETMY_BIO_L3_LR_VOLTAGE_SW'] = 0
#        ezca['SUS-ETMY_BIO_L3_UL_VOLTAGE_SW'] = 0
#        ezca['SUS-ETMY_BIO_L3_UR_VOLTAGE_SW'] = 0

#        # low pass
#        ezca['SUS-ETMY_BIO_L3_UL_STATEREQ'] = 2
#        ezca['SUS-ETMY_BIO_L3_UR_STATEREQ'] = 2
#        ezca['SUS-ETMY_BIO_L3_LL_STATEREQ'] = 2
#        ezca['SUS-ETMY_BIO_L3_LR_STATEREQ'] = 2
#        # HV disconnect
#        ezca['SUS-ETMY_BIO_L3_UL_HVDISCONNECT_SW'] = 0
#        ezca['SUS-ETMY_BIO_L3_UR_HVDISCONNECT_SW'] = 0
#        ezca['SUS-ETMY_BIO_L3_LL_HVDISCONNECT_SW'] = 0
#        ezca['SUS-ETMY_BIO_L3_LR_HVDISCONNECT_SW'] = 0
#        time.sleep(2)
#        ezca['SUS-ETMY_L3_ESDOUTF_UL_GAIN'] = 1
#        ezca['SUS-ETMY_L3_ESDOUTF_UR_GAIN'] = 1
#        ezca['SUS-ETMY_L3_ESDOUTF_LL_GAIN'] = 1
#        ezca['SUS-ETMY_L3_ESDOUTF_LR_GAIN'] = 1
#        time.sleep(11)

#        # transition back to ETMY ESD
#        ezca['CAL-PCALX_DARM_GAIN'] = 0
#        ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN'] = -520
#        time.sleep(31)

#        # Reduce L2 gain to normal again
#        ezca.switch('SUS-ETMY_L2_LOCK_L','FM7','OFF')
#        time.sleep(5)
#        ezca.switch('SUS-ETMY_L2_LOCK_L','FM3','OFF')
#        time.sleep(3)

### AJM20200220 moved to RAMP_DOWN_ETMX_BIAS
#        if ifoconfig.darm_actuator=='BOTH':  #BOTHACTUATORS 20190323 add correct gain for BOTH setting
#            ezca['SUS-ETMY_L2_LOCK_L_GAIN'] = 1.07/2
#            ezca['SUS-ETMX_L2_LOCK_L_GAIN'] = 1.10/2
#        else:
#            ezca['SUS-ETMY_L2_LOCK_L_GAIN'] = 1.07
#            ezca['SUS-ETMX_L2_LOCK_L_GAIN'] = 1.10

        #time.sleep(2)
#        # self.timer['esd switch'] = 31

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if self.error:
            notify('Error: no lownoise ESD selected')
            return

        if self.timer['esd switch']:
            return True

#####################################################################
class SWITCH_CURRENT_ESD_TO_LOWNOISE(GuardState):
    index = 1530
    request = False
 
    @assert_mc_lock
    @nodes.checker()
    def main(self):

        # Which ESD are we switching, i.e. which one are we using?
        if abs(ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_GAIN'])>0.1:
            self.current_esd = 'ETMX'
        elif abs(ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN'])>0.1:
            self.current_esd = 'ETMY'
        #self.current_esd = 'ETMX'

        #Ramp Bias to to 100 (Why do we do this again?)
        ezca['SUS-'+self.current_esd+'_L3_LOCK_BIAS_TRAMP'] = 20
        time.sleep(0.3)
        self.bias_sign = math.copysign(1,ezca['SUS-'+self.current_esd+'_L3_LOCK_BIAS_OFFSET'])
        ezca['SUS-'+self.current_esd+'_L3_LOCK_BIAS_OFFSET'] = self.bias_sign * 100
        log('Reducing ESD Bias to 100V')

        # Which two quadrants do we want to switch first? Ensure other 
        # two quadrants are defined below for 2nd iteration of switching.
        self.quadrants = ['UL','LR']

        # Initial eul2esd Ramping settings
        self.nStep = 40
        self.tRamp = 20.0

        # Set up some flags to sequence everything in the run state
        self.do_switch = True
        self.reset1 = True
        self.reset2 = True
        self.redistribute = True


        # Define a timer that we can reset when needed, initially set for bias ramp
        self.timer['random'] = 1#23


    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['random']:
            return

        if self.do_switch:
            self.do_switch = False
            self.flag1 = True
            self.flag2 = True
            self.flag3 = True

        ###############################################
        ### This section of code ramps the eul2esd matrix (necessary because of linearization).

        # Prepare the array of cdsutils.step commands

        if self.flag1:
            self.flag1 = False
            self.enforce = True	#enforces final values in code below
            ezca['SUS-ETMX_L3_EUL2ESD_TRAMP'] = 20
            if self.quadrants[0] == 'UL':
                ezca['SUS-ETMX_L3_EUL2ESD_SETTING_1_1'] = 0
                ezca['SUS-ETMX_L3_EUL2ESD_SETTING_2_1'] = 0.5
                ezca['SUS-ETMX_L3_EUL2ESD_SETTING_3_1'] = 0.5
                ezca['SUS-ETMX_L3_EUL2ESD_SETTING_4_1'] = 0
            elif self.quadrants[0] == 'UR':
                ezca['SUS-ETMX_L3_EUL2ESD_SETTING_1_1'] = 0.5
                ezca['SUS-ETMX_L3_EUL2ESD_SETTING_2_1'] = 0
                ezca['SUS-ETMX_L3_EUL2ESD_SETTING_3_1'] = 0
                ezca['SUS-ETMX_L3_EUL2ESD_SETTING_4_1'] = 0.5
            else:
                ezca['SUS-ETMX_L3_EUL2ESD_SETTING_1_1'] = 0.25
                ezca['SUS-ETMX_L3_EUL2ESD_SETTING_2_1'] = 0.25
                ezca['SUS-ETMX_L3_EUL2ESD_SETTING_3_1'] = 0.25
                ezca['SUS-ETMX_L3_EUL2ESD_SETTING_4_1'] = 0.25
            time.sleep(0.3)
            ezca['SUS-ETMX_L3_EUL2ESD_LOAD_MATRIX'] = 1
            self.timer['random'] = 20
            return

        #####################################
        # Switch Quadrants to Low Noise

        if self.flag2:
            if self.flag3:
                self.flag3 = False
                # Ramp chosen quadrants ESDOUT to zero
                quad_esd_switch.ramp_quadrant_output(self.current_esd,self.quadrants,0,10)
                log("Ramping down the {0} and {1} ESDOUTF gains".format(self.quadrants[0],self.quadrants[1]))
                self.timer['random']=13
                return
            self.flag2 = False
            # Switch selected quadrants to low noise
            log("Switching the {0} and {1} ESDs to LNLV state".format(self.quadrants[0],self.quadrants[1]))
            quad_esd_switch.switch_quadrants_to_lownoise(self.current_esd,self.quadrants)
            # Calculate the gain required for ESDOUT for low volts
            #FIXME: set gains without having to reload
            #esdratio = quad_esd_switch.esd_lownoise_l2lgain[self.current_esd]/ezca['SUS-'+self.current_esd+'_L3_DRIVEALIGN_L2L_GAIN']
            esdratio = self.bias_sign * ifoconfig.esd_lownoise_l2lgain[self.current_esd]/ezca['SUS-'+self.current_esd+'_L3_DRIVEALIGN_L2L_GAIN']
            # Set the ESDOUT gains for low volts
            log("Setting {0} and {1} ESDOUTF gains to {2}".format(self.quadrants[0],self.quadrants[1],esdratio))
            quad_esd_switch.ramp_quadrant_output(self.current_esd,self.quadrants,esdratio,0)

        #####################################
        # Now redefine which quadrants we want to switch and repeat above
        if self.reset1:
            self.reset1 = False
            self.quadrants = ['UR','LL']
            self.nStep = 2*self.nStep
            self.tRamp = 2*self.tRamp
            self.do_switch = True   #Invokes the ramping of quadrant eul2esd mtrx and switching again (with different quadrants)
            return

        # Now all quadrants should be switched so return eul2esd matrix
        if self.reset2:
            self.reset2 = False
            self.quadrants = ['NONE']
            self.nStep = self.nStep/2
            self.tRamp = self.tRamp/2
            self.flag1 = True	#Invokes the ramping of quadrant eul2esd again (back to default)
            return

        # Finally ramp the l2l gain to final value and esdouts back to 1
        #if self.redistribute:
        #    self.redistribute = False
        #    tramp = 10
        #    for esd in ['UL','LL','UR','LR']:
        #        ezca['SUS-'+self.current_esd+'_L3_ESDOUTF_'+esd+'_TRAMP'] = tramp
        #    ezca['SUS-'+self.current_esd+'_L3_DRIVEALIGN_L2L_TRAMP'] = tramp
        #    time.sleep(0.3)
        #    ezca['SUS-'+self.current_esd+'_L3_DRIVEALIGN_L2L_GAIN'] = self.bias_sign * ifoconfig.esd_lownoise_l2lgain[self.current_esd]
        #    for esd in ['UL','LL','UR','LR']:
        #        ezca['SUS-'+self.current_esd+'_L3_ESDOUTF_'+esd+'_GAIN'] = 1.0
        #    log('Redistributing gain to L2L')
        #    # reset the timer
        #    self.timer['random'] = tramp
        #    return

        return True

##################################################
class SWITCH_OFF_OTHER_ESD(GuardState):
    index = 1570
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        #if ifoconfig.lownoise_esd == 'ETMY':
        #    self.other_esd = 'ETMX'
        #else:
        #    self.other_esd = 'ETMY'

        if abs(ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_GAIN'])>0.1:
            self.other_esd = 'ETMY'
        elif abs(ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN'])>0.1:
            self.other_esd = 'ETMX'

        #---------------------------------------------------------
        # Code for other stages (not esd related)

        # FIXME: Obsolete with LSC DARM splitting
        # Fine tune L2 gains for calibration and cross-overs
        if ifoconfig.split_darm_stage == 'L2':
            ezca['SUS-ETMY_L2_LOCK_L_GAIN'] = 1.07/2
            ezca['SUS-ETMX_L2_LOCK_L_GAIN'] = 1.10/2
        else:
            ezca['SUS-ETMY_L2_LOCK_L_GAIN'] = 1.07
            ezca['SUS-ETMX_L2_LOCK_L_GAIN'] = 1.10


        ezca['LSC-L2_LOCK_TRAMP'] = 5
        matrix.lsc_l1_lock.TRAMP = 5
        matrix.lsc_l1_lock['ETMY','DARM'] *= 1.07/1.1

        time.sleep(0.1)
        ezca['LSC-L2_LOCK_GAIN'] = 1.1
        matrix.lsc_l1_lock.load()
        #---------------------------------------------------------
        #JCB added timer wait default 20240905
        self.timer['wait'] = 1

        # Turn bias off
        #JCB commenting turning bias off out, since we are running 50V, 20240905
        #if not (ezca['SUS-'+self.other_esd+'_L3_LOCK_BIAS_OUTPUT'] == 0):
        #    ezca['SUS-'+self.other_esd+'_L3_LOCK_BIAS_TRAMP'] = 20
        #    time.sleep(0.1)
        #    ezca['SUS-'+self.other_esd+'_L3_LOCK_BIAS_GAIN'] = 0
        #    self.timer['wait'] = 35
        #else:
        #    ezca['SUS-'+self.other_esd+'_L3_LOCK_BIAS_TRAMP'] = 1
        #    time.sleep(0.1)
        #    ezca['SUS-'+self.other_esd+'_L3_LOCK_BIAS_GAIN'] = 0
        #    self.timer['wait'] = 1



        self.switch_to_ln = True

        if self.other_esd == 'ETMY':
            self.set_bias_non_zero = False #AE20240306 lose lock when ramping bias, after vent so don't know new good value
        else:
            self.set_bias_non_zero = False

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer['wait']:
            return

        # If locking esd is the same as low noise esd, the other esd should already be "off"
        #if ifoconfig.locking_esd==ifoconfig.lownoise_esd:
        #    return True

        if self.switch_to_ln:
            # ETMX HV disengage (it makes noise, even with no bias/no control VF/GT/rdr 20161114)
            etm_hv_state = 'OFF'
            for quadrant in ['UL','UR','LL','LR']:
                ezca['SUS-'+self.other_esd+'_BIO_L3_'+quadrant+'_VOLTAGE_SW'] = etm_hv_state
                ezca['SUS-'+self.other_esd+'_BIO_L3_'+quadrant+'_HVDISCONNECT_SW'] = 0

            # No reason not to engage ESD LP on unused optics -AWB 20190225
            etm_statereq = 2
            for quad in ['UL','UR','LL','LR']:
                ezca['SUS-'+self.other_esd+'_BIO_L3_' + quad +'_STATEREQ'] = etm_statereq
            #ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN'] = ifoconfig.esdy_sign_flip * 455; #AEPCAL
            self.switch_to_ln = False
            self.timer['wait'] = 1
            return

        if self.set_bias_non_zero:
            # Glitch rate is less with -100V bias, further minimized by electrical inj to -30 
            ezca['SUS-'+self.other_esd+'_L3_LOCK_BIAS_OFFSET'] = -30
            ezca['SUS-'+self.other_esd+'_L3_LOCK_BIAS_TRAMP'] = 20
            time.sleep(0.1)
            ezca['SUS-'+self.other_esd+'_L3_LOCK_BIAS_GAIN'] = 320
            self.timer['wait'] = 20
            self.set_bias_non_zero = False
            return

        return True


##################################################
class TURN_ON_DAC_DITHERS(GuardState):
    index = 1580
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):
        # Turn on monitorring of PI. Moved here to capture final bias settings of IFO. VB 29082023 # VB09072024 moved again because it takes too long
        nodes_loose['SUS_PI'] = 'MONITOR_LOCK'
        
        #JCB 2021/12/17: Set matrices for pringle mode
        for sus in ["ITMX","ITMY","ETMX","ETMY","BS"]:
            if sus == "BS":
                stage = "M2"
            else:
                stage = "L2"
            ezca.write("SUS-{}_{}_LKIN2OSEM_1_1".format(sus,stage),1)
            ezca.write("SUS-{}_{}_LKIN2OSEM_2_1".format(sus,stage),-1)
            ezca.write("SUS-{}_{}_LKIN2OSEM_3_1".format(sus,stage),-1)
            ezca.write("SUS-{}_{}_LKIN2OSEM_4_1".format(sus,stage),1)

        for sus in ["ETMX","ETMY"]:
            stage = "L3"
            ezca.write("SUS-{}_{}_LKIN2ESD_1_2".format(sus,stage),1)
            ezca.write("SUS-{}_{}_LKIN2ESD_2_2".format(sus,stage),-1)
            ezca.write("SUS-{}_{}_LKIN2ESD_3_2".format(sus,stage),-1)
            ezca.write("SUS-{}_{}_LKIN2ESD_4_2".format(sus,stage),1)

        time.sleep(0.1)

        

        #JCB 2021/12/17: Turn on switches   
        for sus in ["ITMX","ITMY","ETMX","ETMY"]:
            ezca.write("SUS-{}_L2_LKIN_EXC_SW".format(sus),1)
        ezca.write("SUS-BS_M2_LKIN_EXC_SW",1)
        ezca.write("SUS-ETMX_L3_LKIN_EXC_SW",1)
        ezca.write("SUS-ETMY_L3_LKIN_EXC_SW",1)
        time.sleep(0.1)

        # Set tramps
        for sus in ["ITMX","ITMY","ETMX","ETMY","BS"]:
            ezca.write("SUS-{}_LKIN_P_OSC_TRAMP".format(sus),3)

        for sus in ["ETMX","ETMY"]:
            ezca.write("SUS-{}_LKIN_Y_OSC_TRAMP".format(sus),3)
        time.sleep(0.1)

        #JCB 2021-12-14, added 20-bit dac to ITMX for L2 stage and ESD
        #changed ITMX to 60000 from 40000
        #ezca.write("SUS-ITMX_LKIN_P_OSC_CLKGAIN",40000)

        for quad in ["ITMX","ITMY","ETMX","ETMY","BS"]:
            ezca.write("SUS-{}_LKIN_P_OSC_FREQ".format(quad),7000.1)
            ezca.write("SUS-{}_LKIN_P_OSC_CLKGAIN".format(quad),60000)
        
        for quad in ["ETMX","ETMY"]:
            ezca.write("SUS-{}_LKIN_Y_OSC_FREQ".format(quad),7000.2)
            ezca.write("SUS-{}_LKIN_Y_OSC_CLKGAIN".format(quad),0.3)

        self.timer["wait"] = 3

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            return

        return True    


##################################################
class TURN_ON_CALIBRATION_LINES(GuardState):
    index = 1600
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        #kappa_c = 1.0
        #invkappa_c = 1/kappa_c
        #ezca['CAL-CS_DARM_ERR_GAIN'] = invkappa_c
        #ezca['CAL-CS_DARM_ERR_NULL_GAIN'] = invkappa_c
        #ezca['CAL-CS_DARM_DELTAL_RESIDUAL_WHITEN_GAIN'] = kappa_c

        #Test 
        #ezca['SUS-ETMY_M0_TEST_L_TRAMP'] = 120
        #time.sleep(0.5)
        #tidal_offset = cdsutils.avg(-10,'SUS-ETMY_M0_TIDAL_L_OUTPUT')
        #ezca['SUS-ETMY_M0_TEST_L_OFFSET'] = -tidal_offset
        #ezca.switch('SUS-ETMY_M0_TEST_L','OFFSET','ON')

        #JCB 20230426 Switching to CALCS L1 driven line from SUS L1 line
        #ezca['SUS-ETMX_L1_CAL_LINE_FREQ'] = ifoconfig.calibration_line1_freq
        #ezca['SUS-ETMX_L1_CAL_LINE_CLKGAIN'] = ifoconfig.calibration_line1_clkgain
        ezca['CAL-CS_LINE_1_OSC_FREQ'] = ifoconfig.calibration_line1_freq
        ezca['CAL-CS_LINE_1_OSC_CLKGAIN'] = ifoconfig.calibration_line1_clkgain
        #JCB 20240227 Switching to CALCS L2 driven line from SUS L2 line
        #ezca['SUS-ETMX_L2_CAL_LINE_FREQ'] = ifoconfig.calibration_line2_freq
        #ezca['SUS-ETMX_L2_CAL_LINE_CLKGAIN'] = ifoconfig.calibration_line2_clkgain
        ezca['CAL-CS_LINE_2_OSC_FREQ'] = ifoconfig.calibration_line2_freq
        ezca['CAL-CS_LINE_2_OSC_CLKGAIN'] = ifoconfig.calibration_line2_clkgain


        #This could probably be moved to the TURN_ON_CALIBRATION_LINE step
        #Turn on ETMY calibration line
        #JCB 20240227 Switch to CALCS L3 driven line from SUS L3 lines - only need one since it gets sent to both
        #ezca['SUS-ETMY_L3_CAL_LINE_FREQ'] = ifoconfig.calibration_line3_freq
        #ezca['SUS-ETMY_L3_CAL_LINE_CLKGAIN'] = ifoconfig.calibration_line3_clkgain
        #ezca['SUS-ETMX_L3_CAL_LINE_FREQ'] = ifoconfig.calibration_line3_freq
        #ezca['SUS-ETMX_L3_CAL_LINE_CLKGAIN'] = ifoconfig.calibration_line3_clkgain #Opposite sign for proper phase -AWB 20190216
        ezca['CAL-CS_LINE_3_OSC_FREQ'] = ifoconfig.calibration_line3_freq
        ezca['CAL-CS_LINE_3_OSC_CLKGAIN'] = ifoconfig.calibration_line3_clkgain
        
        for osc in [1,2,3,4,5,6,7,8,9]:
            ezca[f'CAL-PCALX_PCALOSC{osc}_OSC_TRAMP'] = 15
            ezca[f'CAL-PCALY_PCALOSC{osc}_OSC_TRAMP'] = 15
        

        # PCAL LINES
        if ifoconfig.use_pcaly:
            # ezca['CAL-CS_TDEP_PCAL_REF_END_SW'] = 1 # Done earlier
            ezca['CAL-PCALY_OSC_SUM_MATRIX_1_1'] = 1
            ezca['CAL-PCALY_OSC_SUM_MATRIX_1_2'] = 1
            ezca['CAL-PCALY_OSC_SUM_MATRIX_1_3'] = 1
            ezca['CAL-PCALY_OSC_SUM_MATRIX_1_4'] = 1
            if ifoconfig.pcalx_good:
                ezca['CAL-PCALX_OSC_SUM_MATRIX_1_1'] = 1
                ezca['CAL-PCALX_OSC_SUM_MATRIX_1_5'] = 1
                ezca['CAL-PCALX_OSC_SUM_MATRIX_1_6'] = 1
                ezca['CAL-PCALX_OSC_SUM_MATRIX_1_7'] = 1
                ezca['CAL-PCALX_OSC_SUM_MATRIX_1_8'] = 1
                ezca['CAL-PCALX_OSC_SUM_MATRIX_1_9'] = 1
            
            ezca['CAL-PCALY_PCALOSC4_OSC_FREQ'] = ifoconfig.pcaly_line4_freq
            ezca['CAL-PCALY_PCALOSC3_OSC_FREQ'] = ifoconfig.pcaly_line3_freq
            ezca['CAL-PCALY_PCALOSC2_OSC_FREQ'] = ifoconfig.pcaly_line2_freq
            ezca['CAL-PCALY_PCALOSC1_OSC_FREQ'] = ifoconfig.pcaly_line1_freq
            if ifoconfig.pcalx_good:
                ezca['CAL-PCALX_PCALOSC5_OSC_FREQ'] = ifoconfig.pcalx_line5_freq
                ezca['CAL-PCALX_PCALOSC6_OSC_FREQ'] = ifoconfig.pcalx_line6_freq
                ezca['CAL-PCALX_PCALOSC7_OSC_FREQ'] = ifoconfig.pcalx_line7_freq
                ezca['CAL-PCALX_PCALOSC8_OSC_FREQ'] = ifoconfig.pcalx_line8_freq
                ezca['CAL-PCALX_PCALOSC9_OSC_FREQ'] = ifoconfig.pcalx_line9_freq
                #ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] = ifoconfig.pcalx_line1_freq   # for running HF lines, SK 170421
            time.sleep(0.1)
            if ifoconfig.pcalx_good:
                ezca['CAL-PCALX_OSC_SUM_ON'] = 1
            else:
                ezca['CAL-PCALX_OSC_SUM_ON'] = 0
            ezca['CAL-PCALY_OSC_SUM_ON'] = 1
            
        else:
            # ezca['CAL-CS_TDEP_PCAL_REF_END_SW'] = 0 # Done earlier
            ezca['CAL-PCALX_OSC_SUM_MATRIX_1_1'] = 1
            ezca['CAL-PCALX_OSC_SUM_MATRIX_1_2'] = 1
            ezca['CAL-PCALX_OSC_SUM_MATRIX_1_3'] = 1
            ezca['CAL-PCALX_PCALOSC3_OSC_FREQ'] = ifoconfig.pcaly_line3_freq
            ezca['CAL-PCALX_PCALOSC2_OSC_FREQ'] = ifoconfig.pcaly_line2_freq
            ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] = ifoconfig.pcaly_line1_freq
            #ezca['CAL-PCALY_PCALOSC3_OSC_FREQ'] = ifoconfig.pcaly_line3_freq+2.0
            #ezca['CAL-PCALY_PCALOSC3_OSC_SINGAIN'] = ifoconfig.pcaly_line3_amp
            #ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] = ifoconfig.pcalx_line1_freq   # for running HF lines, SK 170421
            time.sleep(0.1)
            ezca['CAL-PCALX_OSC_SUM_ON'] = 1
            ezca['CAL-PCALY_OSC_SUM_ON'] = 0
        
        # Make sure the HW injection line is on. IFO_CALIB guardian can turn it off, and in edge cases it may not get turned back on.

        ###TEMPORARY TURN ON OF HIGH FREQUENCY PCALX LINES  #FOR ER10 only
        #ezca['CAL-PCALX_OSC_SUM_ON'] = 1
        #ezca['CAL-PCALX_OSC_SUM_MATRIX_1_1'] = 1
        #ezca['CAL-PCALX_PCALOSC1_OSC_FREQ'] = 2001.3
        #ezca['CAL-PCALX_PCALOSC1_OSC_TRAMP'] = 10
        #ezca['CAL-PCALX_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcalx_line1_amp
        #time.sleep(0.1)
        #ezca['CAL-PCALX_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcalx_line1_amp # for running HF lines, SK 170421
        # to accomodate two pcal actuation the lines are starte here
        if ifoconfig.use_pcaly:
            ezca['CAL-PCALY_PCALOSC4_OSC_SINGAIN'] = ifoconfig.pcaly_line4_amp
            ezca['CAL-PCALY_PCALOSC3_OSC_SINGAIN'] = ifoconfig.pcaly_line3_amp
            ezca['CAL-PCALY_PCALOSC2_OSC_SINGAIN'] = ifoconfig.pcaly_line2_amp 
            ezca['CAL-PCALY_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcaly_line1_amp
            if ifoconfig.pcalx_good:
                ezca['CAL-INJ_MASTER_SW'] = 1
                ezca['CAL-PCALX_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcalx_line1_amp #HFLine actually managed by HF LINE GUARDIAN
                ezca['CAL-PCALX_PCALOSC5_OSC_SINGAIN'] = ifoconfig.pcalx_line5_amp
                ezca['CAL-PCALX_PCALOSC6_OSC_SINGAIN'] = ifoconfig.pcalx_line6_amp
                ezca['CAL-PCALX_PCALOSC7_OSC_SINGAIN'] = ifoconfig.pcalx_line7_amp
                ezca['CAL-PCALX_PCALOSC8_OSC_SINGAIN'] = ifoconfig.pcalx_line8_amp
                ezca['CAL-PCALX_PCALOSC9_OSC_SINGAIN'] = ifoconfig.pcalx_line9_amp
            else:
                ezca['CAL-INJ_MASTER_SW'] = 0
        else:
            ezca['CAL-INJ_MASTER_SW'] = 0
            ezca['CAL-PCALX_PCALOSC3_OSC_SINGAIN'] = ifoconfig.pcaly_line3_amp
            ezca['CAL-PCALX_PCALOSC2_OSC_SINGAIN'] = ifoconfig.pcaly_line2_amp 
            ezca['CAL-PCALX_PCALOSC1_OSC_SINGAIN'] = ifoconfig.pcaly_line1_amp
        
        # Disable subtraction overrides, now that we have a signal
        self.timer['wait'] = 20
        self.first_time_through = True
 
       # AE20200324 spot check ETMY, temporary setting for powerup, putting back to nominal
        #ezca['SUS-ETMY_L2_DRIVEALIGN_P2L_TRAMP']=300
        #ezca['SUS-ETMY_L2_DRIVEALIGN_Y2L_TRAMP']=300
        #time.sleep(0.2)
        #ezca['SUS-ETMY_L2_DRIVEALIGN_P2L_GAIN']=-3
        #ezca['SUS-ETMY_L2_DRIVEALIGN_Y2L_GAIN']=-7

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if self.timer['wait']:
            #if self.first_time_through:
            #    ezca['CAL-CS_TDEP_A_IMAG_RSET'] = 2
            #    ezca['CAL-CS_TDEP_A_REAL_RSET'] = 2
            #   ezca['CAL-CS_TDEP_KAPPA_PU_REAL_RSET'] = 2
            #    ezca['CAL-CS_TDEP_KAPPA_PU_IMAG_RSET'] = 2
            for osc in ['1','2','3']:
                ezca['CAL-CS_TDEP_SUS_LINE'+ osc +'_GRM_REAL_OVERRIDE'] = 0
                ezca['CAL-CS_TDEP_SUS_LINE'+ osc +'_GRM_IMAG_OVERRIDE'] = 0
            for osc in ['1','2','3','4','5','6','7','8','9']:
                ezca['CAL-CS_TDEP_PCAL_LINE'+ osc +'_GRM_REAL_OVERRIDE'] = 0
                ezca['CAL-CS_TDEP_PCAL_LINE'+ osc +'_GRM_IMAG_OVERRIDE'] = 0
            return True

##################################################
class SET_IMC_FINAL(GuardState):
    index = 1700
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        #vcoFreq = cdsutils.avg(-20,'SYS-TIMING_C_FO_A_PORT_11_NODE_CFC_FREQUENCY_5',stddev=False)
        #vcoOffset = ezca['IMC-VCO_TUNEOFS']

        #dVoltMaxPlus = 10-vcoOffset
        #dVoltMaxMinus = -10-vcoOffset

        #dFreq = 79200000 - vcoFreq # TH per alog 41424 #79600000
        #dVolt = dFreq*7.8e-6

        #if dVolt > dVoltMaxPlus:
        #    dVolt = dVoltMaxPlus
        #elif dVolt < dVoltMaxMinus:
        #    dVolt = dVoltMaxMinus

        #if dVolt < 0:
        #    vStep = -0.02
        #else:
        #    vStep = +0.02

        #nStep = int(math.ceil(abs(dVolt/vStep)))
        #if vcoFreq < 78e6 or vcoFreq > 80e6:
        #    notify('VCO frequency out of range, check comparator')
        #    nStep = 1

        #tStep = 0.2
    
        #cmd_string = str(vStep) + ',' + str(nStep)

        #self.vcostep = cdsutils.Step(ezca, 'IMC-VCO_TUNEOFS', cmd_string, time_step=tStep)

        # Put IMC LOCK in it's final state, where it checks and sets the FSS.
        nodes['IMC_LOCK'].set_request('PSL_TRACKING')

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not (nodes['IMC_LOCK'].arrived and nodes['IMC_LOCK'].done):
            log('Waiting on IMC_LOCK')
            return
        #if self.step_check:
        #    return True
        #return self.vcostep.step() #commented out this and above for rogue vco test ajm20190227
        #return self.fss_step.step()
        return True

##################################################
class SET_DCPD_CURRENT(GuardState):
    index = 1750
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        dcpd_current = 50.0 #40.0

        tramp = 1
        ezca['OMC-READOUT_OFFSET_TRAMP'] = tramp
        ezca['OMC-READOUT_SIMPLE_TRAMP'] = tramp

        time.sleep(1)
        ezca['OMC-READOUT_OFFSET_OFFSET'] = dcpd_current
        ezca['OMC-READOUT_SIMPLE_GAIN'] = math.sqrt(20.0/dcpd_current) #Is 1.0 at 20mA
        #matrix.lsc_input_arm.load()
        self.timer['wait'] = tramp + 1

    @assert_mc_lock
    @nodes.checker()
    def run(self):
        if not self.timer['wait']:
            return
        return True


##################################################
class DRMI_FF(GuardState):
    index = 1780
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):


        # Prep FF filters and matrices
        ezca['LSC-MICHFF_TRAMP'] = 0
        ezca['LSC-SRCLFF_TRAMP'] = 0
        time.sleep(0.3)
        ezca['LSC-MICHFF_GAIN'] = 0
        ezca['LSC-SRCLFF_GAIN'] = 0

        ezca.switch('LSC-MICHFF','FMALL','OFF')
        #ezca.switch('LSC-MICHFF','FM3','ON') # VF tweak Jan 10 2020
        #ezca.switch('LSC-MICHFF','FM5','ON') # Updated for new FF filter 20190312 -AWB
        ezca.switch('LSC-MICHFF','FM1','ON') # VB new FF filter 20230906
        ezca.switch('LSC-SRCLFF','FMALL','OFF')
        ezca.switch('LSC-SRCLFF','FM1','FM2','ON')  # VB new filter 11 Aug 2023 
        #ezca.switch('LSC-SRCLFF','FM3','FM4','FM10','ON')
        ezca.switch('SUS-ITMX_L2_LOCK_L','FM6','ON')
        matrix.lsc_output['ITMY', 'MICHFF'] = -2
        matrix.lsc_output['ITMY', 'SRCLFF'] = -2
        time.sleep(3)
        # Turn on feedforward
        ezca['LSC-MICHFF_TRAMP'] = 3
        ezca['LSC-SRCLFF_TRAMP'] = 3
        ezca['LSC-MICHFF_GAIN'] = 1.025 # VF 3 Mar 2025
        ezca['LSC-SRCLFF_GAIN'] = 0.94 # for 56W #1.05 for 71W   #1.0 for 63.7W 50mA | VB new filter 11 Aug 2023 
        #2.8 #63.7W 50mA | 2.5 #63.7W 40mA | #2.45 #56.7W 40mA |1.95 #50W 30mA | 2.2 #56.7W 30mA | 1.95 #40W 30mA
        ezca['LSC-SRCL_GAIN'] = 0.4 
        ezca['LSC-PRCL_GAIN'] = 0.88
        time.sleep(3)
        # Tuned 30 LP to reduce SRCL coherence with DARM at 30Hz (Den Oct 8 2016)
        ezca.switch('LSC-SRCL','FM1','FM9','ON')
        # Turn on PRCL Low Pass
        ezca.switch('LSC-PRCL','FM7','ON')
        #ezca.switch('SUS-BS_M2_LOCK_L','FM3','ON')     #Moved to SWITCH ACTUATORS ajm210121

        # Turn on OAF-CAL L1
        ezca.switch('OAF-CAL_SUM_DARM_L1','OUTPUT','ON')

        self.timer["wait"] = 0.3

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            return

        return True


##################################################
class FINALIZE_LOW_NOISE(GuardState):
    index = 1800
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        ezca["LSC-DARM_GAIN"] = -0.6

        ezca['OMC-ASC_MASTERGAIN']=1

        # Turn on SRCL OFFSET
        ezca['LSC-SRCL_TRAMP'] = 1
        time.sleep(0.5)
        ezca.switch('LSC-SRCL','OFFSET','ON')
        ezca['LSC-SRCL_OFFSET'] = -2500 #BK 20240826 changed from -3400 #JCB 20240627 changed from -3000

        if not sqzconfig.nosqz:
            #ezca['SQZ-FC_ASC_GAIN'] = 0
            time.sleep(1)
            #nodes_loose['SQZ_MANAGER'] = 'SINGLE_CLF_ADS'

        ### ISS 2nd loop (keep on the lower side)
        final_iss_gain = math.floor(ifoconfig.iss_gain_40W - 20*math.log(ezca['IMC-IM4_TRANS_SUM_OUTPUT']/40.0,10))
        # Set ISS turn on gain 16dB lower than final gain
        ezca['PSL-ISS_SECONDLOOP_GAIN'] = final_iss_gain - 16
        time.sleep(2)
        # Close ISS 2nd loop
        ezca['PSL-ISS_SECONDLOOP_CLOSED'] = 1
        time.sleep(0.3)
        # Bring ISS gain back in steps of 2dB, 8 times, total must equal 16dB as above
        self.iss_step = cdsutils.Step(ezca,'PSL-ISS_SECONDLOOP_GAIN','+2.0,8',time_step=0.5)


        #Test coupled cavity mode sensing signals #CB commissioning periods 22 Mar 24
        #VB20240618 Disabled below lines as CB confiremd he has more than enough data. The L1:PSL-ISS_INJ_MTRX elements are set by SDF
        #ezca.switch('PSL-ISS_TRANSFER2_INJ','INPUT','OUTPUT','ON')
        #ezca['PSL-ISS_TRANSFER2_INJ_TRAMP'] = 5
        #ezca['PSL-ISS_TRANSFER2_INJ_GAIN'] = 1

        #ezca['PSL-ISS_TRANSFER2_INJ_OSC_FREQ'] = 10620
        #ezca['PSL-ISS_TRANSFER2_INJ_OSC2_FREQ'] = 10530

        #ezca['PSL-ISS_TRANSFER2_INJ_OSC_TRAMP'] = 5
        #ezca['PSL-ISS_TRANSFER2_INJ_OSC2_TRAMP'] = 5
        #time.sleep(0.1)
        #ezca['PSL-ISS_TRANSFER2_INJ_OSC_SINGAIN'] = 4
        #ezca['PSL-ISS_TRANSFER2_INJ_OSC2_SINGAIN'] = 4


        #------------------------------------------
        ## Correct DARM normalization factor 
        trx_nom = 48000.0
        trx_current = ezca['LSC-TR_X_NORM_OUTPUT']
        norm_factor = math.sqrt(trx_nom/trx_current)
        omc_gain = matrix.lsc_input_arm['DARM','OMC_DC']
        matrix.lsc_input_arm['DARM','OMC_DC'] = omc_gain/norm_factor
        matrix.lsc_input_arm.TRAMP = 10
        time.sleep(0.3)
        matrix.lsc_input_arm.load()
        #------------------------------------------
        
        self.timer["wait"] = 10
        self.wait = True
        self.set_null_path = True

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            return

        # ISS gain stepping (final value at 44W: 25dB)
        if not self.iss_step.step():
            return

        if self.wait:
            self.timer["wait"] = 2
            self.wait = False
            return

        if self.set_null_path:
            #Set correct gain on NULL DCPD path JCB 21/2/2019
            #Moved from TURN_ON_CALIBRATION_LINES to here, added in simple gain correction JCB 2023/02/15
            trx_current = ezca['LSC-TR_X_NORM_OUTPUT']
            dcpd_path_norm_gain = ezca['LSC-POW_NORM_MTRX_1_17']
            dcpd_path_input_gain = ezca['LSC-ARM_INPUT_MTRX_1_1']
            dcpd_path_simple_gain = ezca['OMC-READOUT_SIMPLE_GAIN']
            ezca['OMC-READOUT_SIMPLE_NULL_GAIN'] = \
                 dcpd_path_input_gain*dcpd_path_simple_gain/(math.sqrt(trx_current)*dcpd_path_norm_gain)
            ezca['CAL-CS_DARM_ERR_NULL_GAIN'] = ezca['CAL-CS_DARM_ERR_GAIN']
            self.set_null_path = False


        return True


#################################################
def final_asc_prc2_inp_mtrx():

    matrix.asc_input_pit.zero(row='PRC2')
    matrix.asc_input_yaw.zero(row='PRC2')

    matrix.asc_input_pit['PRC2','REFL_A_RF9_I'] = -1.68 # -3.98 AE new matrix jan 2023, scaled for 1 pregain 
    matrix.asc_input_pit['PRC2','REFL_A_RF45_I'] = 3.04     # 1.75 
    matrix.asc_input_pit['PRC2','REFL_B_RF45_I'] = 0.2 # 0.41

    matrix.asc_input_yaw['PRC2','REFL_A_RF9_I'] = -2.2 # -11.28 
    matrix.asc_input_yaw['PRC2','REFL_A_RF45_I'] = 3.34   # 2.83
    matrix.asc_input_yaw['PRC2','REFL_B_RF45_I'] = 0.54   # 0.99
################################

class ASC_LOW_NOISE(GuardState):
    index = 1810
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        t_ramp = 5.0
        for dof1 in ["CHARD","DHARD"]:
            for dof2 in ["P","Y"]:
                ezca["ASC-{}_{}_PRE_TRAMP".format(dof1,dof2)] = t_ramp
        time.sleep(0.1)

        # Set ETMY spot vertical position #TODO: record what was stored and write back here
        #ezca['SUS-ETMY_L2_DRIVEALIGN_P2L_GAIN'] = 3.3

        # ezca['ASC-CHARD_Y_PRE_GAIN'] = 0.16
  
        # Reduce the dither amplitudes
        if not ifoconfig.cam_spot_centering:
            for dof in ['PIT1','PIT3','PIT4','YAW1','YAW3','YAW4']:
                ezca['ASC-ADS_'+ dof +'_OSC_CLKGAIN'] = ifoconfig.quad_clk_gain/3.0

        self.timer["wait"] = 1.0

        # reduce REFL B centering gain AE20240311
        ezca['ASC-DC2_P_PRE_GAIN'] = 0.5
        ezca['ASC-DC2_Y_PRE_GAIN'] = 0.5        

        # Set this to true if you want to wait for spots
        self.checkspots = False

        # Set this to true if you want to switch PRC2 to REFL_WFS
        self.switch_prc2_to_refl = False
        self.switch_on_pr2m1 = True

        # Set this to true to set final ASC SRC1 Gains
        self.set_src_asc_gains = True

        if self.set_src_asc_gains:
            self.src_tramp = 10
            for dof1 in ['1','2']:
                for dof2 in ['P','Y']:
                    ezca.write("ASC-SRCL{}_{}_PRE_TRAMP".format(dof1,dof2),self.src_tramp)
            time.sleep(0.1)

        # Set this to true if you want to turn on cuttoffs
        self.turn_on_cutoffs = True

        # Set this to true if you want to turn on HAM1 to CHARD FF
        self.ham1_to_chard = True

        self.turn_on_spots = not ifoconfig.cam_spot_centering

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.timer["wait"]:
            return

        if self.checkspots:
            time.sleep(1)   # Only check once a second
            prg = cdsutils.avg(-10,'LSC-PRC_GAIN_MON',stddev=True)
            xspotp = cdsutils.avg(-10,'ASC-ADS_PIT3_DEMOD_I_OUTPUT',stddev=True)
            yspotp = cdsutils.avg(-10,'ASC-ADS_PIT4_DEMOD_I_OUTPUT',stddev=True)
            xspoty = cdsutils.avg(-10,'ASC-ADS_YAW3_DEMOD_I_OUTPUT',stddev=True)
            yspoty = cdsutils.avg(-10,'ASC-ADS_YAW4_DEMOD_I_OUTPUT',stddev=True)
            if (prg[0] < 40 or prg[1] > 0.2 or
                abs(xspotp[0]) > 0.3 or
                abs(yspotp[0]) > 0.3 or
                abs(xspoty[0]) > 0.3 or
                abs(yspoty[0]) > 0.3):
                log('waiting for spots to center and PRG to settle higher than 38')
                return
            self.checkspots = False
            return

        if self.switch_prc2_to_refl:
            log('Switching PRC2 to use REFL WFS')

            # Freeze M1
            ezca.switch('SUS-PR2_M1_LOCK_P','INPUT','OFF')
            ezca.switch('SUS-PR2_M1_LOCK_Y','INPUT','OFF')

            # turn on PR2 M2 A2L gains
            for dof in ['P','Y']:
                ezca['SUS-PR2_M2_DRIVEALIGN_{}2L_TRAMP'.format(dof)] = 0
                time.sleep(0.1)
                ezca['SUS-PR2_M2_DRIVEALIGN_{}2L_GAIN'.format(dof)] = 0
                time.sleep(0.1)
                ezca.switch('SUS-PR2_M2_DRIVEALIGN_{}2L'.format(dof),'OUTPUT','ON')
                ezca['SUS-PR2_M2_DRIVEALIGN_{}2L_TRAMP'.format(dof)] = 3
            time.sleep(0.1)
            ezca['SUS-PR2_M2_DRIVEALIGN_P2L_GAIN'] = 2.25
            ezca['SUS-PR2_M2_DRIVEALIGN_Y2L_GAIN'] = 1.25

            # turn off PRC2
            ezca.switch('ASC-PRC2_P', 'INPUT', 'OFF')
            ezca.switch('ASC-PRC2_Y', 'INPUT', 'OFF')
            time.sleep(0.3)
            ezca['ASC-PRC2_Y_PRE_GAIN'] = 1.0

            # switch matrices from popx to refl
            final_asc_prc2_inp_mtrx()
            time.sleep(5)
            # switch loops back on
            ezca.switch('ASC-PRC2_P', 'INPUT', 'ON')
            time.sleep(10) #break for pitch to settle
            ezca.switch('ASC-PRC2_Y', 'INPUT', 'ON')

            # disengage POPX centering
            ezca.switch('ASC-DC5_P','INPUT','OFF')
            ezca.switch('ASC-DC5_Y','INPUT','OFF')
            ezca['ASC-DC5_P_GAIN'] = 0
            ezca['ASC-DC5_Y_GAIN'] = 0

            # close POP diverter
            log('Closing POP Beam Diverter')
            ezca['SYS-MOTION_C_BDIV_A_CLOSE'] = 1

            self.switch_on_pr2m1 = True
            self.switch_prc2_to_refl = False
            self.timer["wait"] = 5.0
            return

        if ifoconfig.popx:
            ezca['ASC-PRC2_P_PRE_GAIN'] = 0.6 #  added AE20240301 as measured at 56W, low wrt 2W
            ezca['ASC-PRC2_Y_PRE_GAIN'] = 1 # 

        if self.switch_on_pr2m1:
            # Turn PR2 M1 back on
            ezca.switch('SUS-PR2_M1_LOCK_P','INPUT','OFF')
            ezca.switch('SUS-PR2_M1_LOCK_Y','INPUT','OFF')
            self.switch_on_pr2m1 = False
            return

        if self.set_src_asc_gains:

            src_tramp = 5
            #srasc_juggle('srm',tramp=src_tramp)
            #FIXME: Currently being done earlier, so we might remove this from here
            log('Setting SRC ASC Pre Gains')
            ezca['ASC-SRCL1_P_PRE_GAIN'] = 0.05
            ezca['ASC-SRCL1_Y_PRE_GAIN'] = 0.05
            ezca['ASC-SRCL2_P_PRE_GAIN'] = 1.0  #0.6
            ezca['ASC-SRCL2_Y_PRE_GAIN'] = 1.0  #0.6

            log('Turning on SR2 M1 ASC offload')
            for dof in ['P','Y']:
                ezca.switch('SUS-SR2_M1_LOCK_{}'.format(dof),'INPUT','ON')

            self.set_src_asc_gains = False
            self.timer["wait"] = src_tramp
            return

        if self.turn_on_cutoffs:
            log('Turning on hard cut-offs')
            for filt in ['FM10','FM5']:
                for dof1 in ['C','D']:
                    for dof2 in ['P']:
                        ezca.switch('ASC-'+dof1+'HARD_'+dof2,filt,'ON')
                        time.sleep(2)

            log('Turning on hard cut-offs')
            for filt in ['FM10','FM5']:
                for dof1 in ['C','D']:
                    for dof2 in ['Y']:
                        ezca.switch('ASC-'+dof1+'HARD_'+dof2,filt,'ON')
                        time.sleep(2)

           #log('Turning on DHARD Pitch 0.45Hz resonant gain')
            ezca.switch('ASC-DHARD_P','FM3','ON')
            self.turn_on_cutoffs = False
            self.timer['wait'] = 3
            return

        # feed forward noise subtraction for HAM1-REFL WFS sensors
        if self.ham1_to_chard:
            log('Turning on HAM1 feedforward to CHARD P')
            ezca['ASC-CHARD_HAM1_P_A_TRAMP'] = 1
            ezca['ASC-CHARD_HAM1_Y_A_TRAMP'] = 1
            time.sleep(0.2)
            ezca['ASC-CHARD_HAM1_P_A_GAIN'] = 1 #Gain 1 turns subtraction on
            ezca['ASC-CHARD_HAM1_Y_A_GAIN'] = 0
            self.ham1_to_chard = False
            self.timer['wait'] = 1 #This timer provides a small buffer for SDF difference logging purposes
            return

        if self.turn_on_spots:
            # Set final ADS Yaw Gains, Added by MN 20211216
            ezca['ASC-ADS_YAW3_DOF_GAIN'] = 150
            ezca['ASC-ADS_YAW4_DOF_GAIN'] = -150
            ezca['ASC-ADS_GAIN'] = 0.02
            self.turn_on_spots = False

        return True

####################################################################################
class WAIT_SQZ(GuardState):
    index = 1950
    request = False

    @assert_mc_lock
    @nodes.checker()
    def main(self):

        self.timer["wait"] = 5
        
        if not sqzconfig.nosqz:
            nodes_loose['SQZ_MANAGER'] = 'FREQ_DEPENDENT_SQZ'
            None

        self.wait_for_sqz = False

    @assert_mc_lock
    @nodes.checker()
    def run(self):

        if not self.wait_for_sqz:
            return True

        if not sqzconfig.nosqz:
            return nodes_loose['SQZ_MANAGER'].completed
        else:
            return True
        
####################################################################################
class LOW_NOISE(GuardState):
    index = 2000
    request = True

    @assert_mc_lock
    @nodes.checker()
    def main(self):

#    nodes.set_managed()

        # return QUAD and BS guardians to ALIGNED to make guardians happy
        for quad in ['ITMX','ITMY','ETMX','ETMY','BS']:
            nodes_optics['SUS_{}'.format(quad)] = 'ALIGNED'

        # set ALL front end TABLES to the 'observe' state - S. Aston 09/28/2018
        # subprocess.call(['/opt/rtcds/userapps/trunk/isc/l1/guardian/All_SDF_observe.sh'])
        set_sdf_to_observe = True
        if set_sdf_to_observe:
            FRONT_ENDS.sdf_load('observe', loadto='table', fast=True)

        self.timer["wait"] = 30
        self.timer["cal correct"] = 0*60
        self.timer["loop iter"] = 1
        self.set_final_kappa = True
        self.set_power_normalized_optical_gain = True

    @assert_mc_lock
    @nodes.checker()
    def run(self):
    
#        for node in nodes.get_stalled_nodes():
#            log("reviving node: %s" % node.name)
#            node.revive()

        #log('{}'.format(matrix.asc_ads_sensors))
        #log('{}'.format(matrix.asc_ads_actuators_dither))

        #if not self.timer["wait"]:
        #    return
        if self.set_power_normalized_optical_gain:
            reference_sensing_gain = -0.06279
            reference_power = 63.528
            power = ezca['IMC-IM4_TRANS_SUM_OUTPUT']
            omc_scale = ezca['OMC-READOUT_SCALE_OFFSET']
            simple_gain = ezca['OMC-READOUT_SIMPLE_GAIN']
            input_matrix_gain = ezca['LSC-ARM_INPUT_MTRX_1_1']
            norm_matrix = ezca['LSC-POW_NORM_MTRX_1_17']
            trx_hold = ezca['LSC-TR_X_NORM_OUTPUT']
            digital_sensing_gain = omc_scale*simple_gain*input_matrix_gain/(norm_matrix*trx_hold**0.5)
            pwr_norm_digital_sensing_gain = 1/((digital_sensing_gain/reference_sensing_gain)*(power/reference_power)**0.5)
            ezca['CAL-CS_TDEP_OPT_GAIN_MA_PER_CNT'] = pwr_norm_digital_sensing_gain
            self.set_power_normalized_optical_gain = False
        
        if not self.timer["cal correct"]:
            if self.timer["loop iter"]:
                log("Updating Calibration")
                kappa_c = float(ezca['CAL-CS_TDEP_KAPPA_C_OUTPUT'])
                invkappa_c = 1/kappa_c
                ezca['CAL-CS_DARM_ERR_GAIN'] = invkappa_c
                ezca['CAL-CS_DARM_ERR_NULL_GAIN'] = invkappa_c
                ezca['CAL-CS_DARM_DELTAL_RESIDUAL_WHITEN_GAIN'] = kappa_c
                self.timer["loop iter"] = 30
            return True #Note if this is happening OBS-MODE won't get set below

        if self.set_final_kappa:
            kappa_c = 1.045
            invkappa_c = 1/kappa_c
            kappa_tst = 1.00
            invkappa_tst = 1/kappa_tst
            ezca['CAL-CS_DARM_ERR_GAIN'] = invkappa_c
            ezca['CAL-CS_DARM_ERR_NULL_GAIN'] = invkappa_c
            ezca['CAL-CS_DARM_DELTAL_RESIDUAL_WHITEN_GAIN'] = kappa_c
            #JCB 20240817 Added kappa_tst correction temporarily
            ezca['CAL-CS_DARM_ANALOG_ETMX_L3_GAIN'] = kappa_tst
            ezca['CAL-CS_DARM_DELTAL_CTRL_L3_WHITEN_GAIN'] = invkappa_tst
            ezca['CAL-CS_DARM_CFTD_DELTAL_CTRL_L3_HP_GAIN'] = invkappa_tst

            self.set_final_kappa = False

        #if ezca['GRD-IFO_OK'] == 1:
        #    ezca['ODC-OBSERVATORY_MODE'] = 10
        return True

####################################################################################
#### OLD SCRIPTS FOR REFERENCE FROM HERE ON DOWN 
####################################################################################
#class SWITCH_DHARD_SENSOR(GuardState):
#    index = 770
#    request = False
#
#    @assert_mc_lock
#    @nodes.checker()
#    def main(self):
#        # Replace AS B 45Q with AS A 45Q
#        self.steps = [
#            cdsutils.Step(ezca, matrix.asc_input_pit('DHARD', 'AS_A_RF45_Q'), '+0.03,10', time_step=0.1),
#            cdsutils.Step(ezca, matrix.asc_input_pit('DHARD', 'AS_B_RF45_Q'), '-0.1,10', time_step=0.1),
#            cdsutils.Step(ezca, matrix.asc_input_yaw('DHARD', 'AS_A_RF45_Q'), '+0.03,10', time_step=0.1),
#            cdsutils.Step(ezca, matrix.asc_input_yaw('DHARD', 'AS_B_RF45_Q'), '-0.1,10', time_step=0.1),
#            ]
#
#    @assert_mc_lock
#    @nodes.checker()
#    def run(self):
#        ret = True
#        for step in self.steps:
#            ret &= step.step()
#        return ret
#


##################################################
#class DISTRIBUTE_CM_GAIN(GuardState):
#    index = 1140
#    request = False

#    @assert_mc_lock
#    @nodes.checker()
#    def run(self):
#        if ezca['LSC-REFL_SERVO_IN1GAIN'] < -15:
#            ezca['LSC-REFL_SERVO_IN1GAIN'] += 1
#            ezca['LSC-REFL_SERVO_FASTGAIN'] -= 1
#            ezca['LSC-MCL_GAIN'] *= 0.89125
#            time.sleep(0.1)

#        if ezca['LSC-REFL_SERVO_IN1GAIN'] >= -15:
#            return True

##################################################
#class ETM_L2_LOW_NOISE(GuardState):
#    index = 1165
#    request = False

#    # Commented out transition of ETMs L2 to state 3
#    # left in state 1 to reduce RMS of control signal
#    # and possible saturations
#    # Main reason is that lock is lost every 3-5 hours
#    # lately for unknown reason
#    # Den -- June 5, 2015

#    @assert_mc_lock
#    @nodes.checker()
#    def main(self):
#        # ETMX L2
#        ezca['SUS-ETMX_BIO_L2_STATEREQ'] = 1
#        time.sleep(3)
#        # ezca['SUS-ETMX_BIO_L2_STATEREQ'] = 3
#        # time.sleep(3)
#        # ETMY 
#        ezca['SUS-ETMY_BIO_L2_STATEREQ'] = 1
#        time.sleep(5)
#        # ezca['SUS-ETMY_BIO_L2_STATEREQ'] = 3
#        # time.sleep(5)

#    @assert_mc_lock
#    @nodes.checker()
#    def run(self):
#        return True

##################################################
#class SWITCH_TO_ITMX_ESD(GuardState):
#    index = 1170
#    request = False

#    @assert_mc_lock
#    @nodes.checker()
#    def main(self):
#        # Make sure other stages are turned off
#        ezca['SUS-ITMX_L1_LOCK_OUTSW_L'] = 0

#        ezca['SUS-ITMX_L2_LOCK_OUTSW_L'] = 0
#        # Set ITMX L3 stage
#        ezca['SUS-ITMX_L3_DRIVEALIGN_L2L_TRAMP'] = 0
#        time.sleep(0.1)
#        ezca['SUS-ITMX_L3_DRIVEALIGN_L2L_GAIN'] = 0
#        time.sleep(0.3)
#        ezca['SUS-ITMX_L3_LOCK_OUTSW_L'] = 1
#        ezca['SUS-ITMX_L3_LOCK_L_GAIN'] = 1
#        ezca.switch('SUS-ITMX_L3_DRIVEALIGN_L2L', 'INPUT', 'OUTPUT', 'ON')
#        ezca['SUS-ITMX_L3_DRIVEALIGN_L2L_TRAMP'] = 10

#        # Set ETM ramp times
#        #ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_TRAMP'] = 10
#        #ezca['SUS-ETMX_L3_LOCK_BIAS_TRAMP'] = 10
#        ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_TRAMP'] = 10
#        ezca['SUS-ETMY_L3_LOCK_BIAS_TRAMP'] = 10
#        time.sleep(0.3)

#        # Set LSC matrix
#        matrix.lsc_output_arm['ITMX', 'DARM'] = -1
#        time.sleep(0.3)

#        # Make the transition
#        ezca['SUS-ITMX_L3_DRIVEALIGN_L2L_GAIN'] = -22
#        #ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_GAIN'] = 0 #nominal 1.2 when driving ETMX ESD only
#        ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN'] = 0 #nominal 1.0 when driving ETMY ESD only
#        time.sleep(10)

#        # Engage Violin Notches in ITMX L3
#        #ezca.switch('SUS-ITMX_L3_LOCK_L', 'FM7', 'FM8', 'ON')

#    @assert_mc_lock
#    @nodes.checker()
#    def run(self):
#        return True


if ifoconfig.omc_autolock:
    auto_path = 1
    manual_path = 2
else:
    auto_path = 2
    manual_path = 1 


if ifoconfig.cam_spot_centering:
    cam_path = 1
    dither_path = 2
else:
    cam_path = 2
    dither_path = 1


if ifoconfig.split_darm_stage == 'L1':
    single_drive_path = 3
    split_l1_path = 1
    split_l2_path = 3
elif ifoconfig.split_darm_stage == 'L2':
    single_drive_path = 3
    split_l1_path = 3
    split_l2_path = 1
else:
    single_drive_path = 1
    split_l1_path = 2
    split_l2_path = 3


if ifoconfig.lnlv_switch_on_current_esd:
    current_esd_path = 1
    other_esd_path = 2
else:
    current_esd_path = 2
    other_esd_path = 1


##################################################
# ordering most recently set by rdr on 20160826
edges = [
    ('INIT','IDLE'),
    ('IDLE','DOWN'),
    #Initial Alignment State
    ('DOWN','INITIAL_ALIGNMENT'),
    ('INITIAL_ALIGNMENT','DOWN'),
    ('DOWN','CORNER_INITIAL_ALIGNMENT'),
    ('CORNER_INITIAL_ALIGNMENT','DOWN'),
    #AS Port protection check
    ('DOWN','CHECK_FAST_SHUTTER_AND_WATCHDOGS'),
    #('DOWN', 'LOCK_ALS_XARM'),
    # close the ALS COMM loop
    #('DOWN','LOCK_ALS_ARMS'),
    #('CHECK_FAST_SHUTTER_AND_WATCHDOGS','LOCK_ALS_XARM'),
    #('LOCK_ALS_XARM','START_ALS_COMM'),
    ('CHECK_FAST_SHUTTER_AND_WATCHDOGS','LOCK_ALS_ARMS'),
    ('LOCK_ALS_ARMS','START_ALS_COMM'),
    ('START_ALS_COMM','ALS_COMM_UP'),
    ('ALS_COMM_UP','ALS_COMM_LOCKED'),
# close the ALS DIFF loop 
    ('ALS_COMM_LOCKED','RESET_ALS_DIFF'),
    ('RESET_ALS_DIFF','DIFF_BEATNOTE_CHECK'),
    ('DIFF_BEATNOTE_CHECK','MISALIGN_RECYCLING_MIRRORS'),
    ('MISALIGN_RECYCLING_MIRRORS','ALS_Y_RELOCK'),
    ('ALS_Y_RELOCK','START_ALS_DIFF'),
    ('START_ALS_DIFF','OFFLOAD_DIFF_TO_M0'),
    ('OFFLOAD_DIFF_TO_M0','ALS_DIFF_RAMP_GAIN'),
    ('ALS_DIFF_RAMP_GAIN','ALS_DIFF_BOOST'),
    ('ALS_DIFF_BOOST','ALS_DIFF_LOCKED'),
# find the red resonance offsets
    ('ALS_DIFF_LOCKED','COARSE_SCAN_FOR_IR_RESONANCES'),
    ('COARSE_SCAN_FOR_IR_RESONANCES','FINE_SCAN_FOR_IR_RESONANCES'),
    ('FINE_SCAN_FOR_IR_RESONANCES','ALS_COMPLETE'),
    ('ALS_COMPLETE','OFFSET_COMM'),
    ('OFFSET_COMM','ALIGN_RECYCLING_MIRRORS'),
    ('OFFSET_COMM','ALIGN_BS',2),
    ('ALIGN_BS','ALIGN_RECYCLING_MIRRORS'),
    ('ALIGN_RECYCLING_MIRRORS','FREEZE_BS'),
# lock the vertex interferometer with offset arm cavities
    ('FREEZE_BS','LOCK_DRMI'),
    ('FREEZE_BS', 'POWER_DOWN_500mW'),
    ('POWER_DOWN_500mW','LOCK_DRMI'),
    ('LOCK_DRMI','DRMI_LOCKED'),
    ('DRMI_LOCKED','ENGAGE_DRMI_ASC_CENTERING'),
    ('ENGAGE_DRMI_ASC_CENTERING','ENGAGE_DRMI_ASC'),
    ('ENGAGE_DRMI_ASC','OFFLOAD_DRMI_ASC'),
#    ('OFFLOAD_DRMI_ASC','TURN_ON_BS_STAGE2'),
#    ('TURN_ON_BS_STAGE2','DRMI_LOCKED_ASC'),
    ('OFFLOAD_DRMI_ASC','HEAT_BS'),
    ('HEAT_BS','DRMI_LOCKED_ASC'),
#    ('OFFLOAD_DRMI_ASC','DRMI_LOCKED_ASC'), 
    ('DRMI_LOCKED_ASC','POWER_UP_2W'),
    ('POWER_UP_2W','ZERO_3F_OFFSETS'),
    ('ZERO_3F_OFFSETS','DRMI_TO_3F'),
    #('POWER_UP_4W','DRMI_TO_3F'),
# bring the arms to the bottom of the fringe and transition CARM to control based on transmitted powers
    ('DRMI_TO_3F','COMM_SLEW',),
    ('COMM_SLEW','MOVE_CARM_TR'),
    ('MOVE_CARM_TR','CARM_ON_TR'),
# move slightly up the fringe and transition DARM to control based on AS 45Q (length and angle)
    ('CARM_ON_TR','CARM_100PM'),
    ('CARM_100PM','MOVE_DARM_ASQ'),
    ('MOVE_DARM_ASQ','TURN_ON_DHARD'),
    ('TURN_ON_DHARD','DARM_ON_ASQ'),
# move further up the fringe and transition CARM to control based on REFL 9I 
    #('DARM_ON_ASQ','TURN_OFF_DRMI_ASC'),
    #('TURN_OFF_DRMI_ASC','CARM_10PM'), 
    ('DARM_ON_ASQ','CARM_10PM'),
    ('CARM_10PM','MOVE_CARM_REFL9'),
# zero the CARM offset, engage analog CARM loop, CARM ASC, and DRMI 3f -> 1f
    ('MOVE_CARM_REFL9','CARM_ZERO_OFFSET'),
    #('CARM_ZERO_OFFSET','TURN_ON_ARM_WFS'),
    #('TURN_ON_ARM_WFS','CARM_SERVO_ADDITIVE_OFFSET'), Move arm wfs after CARM_SERVO_BOOST CDA 181018
    ('CARM_ZERO_OFFSET','CARM_SERVO_AO'),
    ('CARM_SERVO_AO', 'CARM_SERVO_SLOW'),
    ('CARM_SERVO_SLOW','CARM_SERVO_FAST'),
#    ('CARM_SERVO_SLOW','CARM_SERVO_FAST_0'),
#    ('CARM_SERVO_FAST_0','CARM_SERVO_FAST_1'),
#    ('CARM_SERVO_FAST_1','CARM_SERVO_FAST_2'),
#    ('CARM_SERVO_FAST_2','CARM_SERVO_BOOST'),
    ('CARM_SERVO_FAST','CARM_SERVO_BOOST'),
    ('CARM_SERVO_BOOST','TURN_ON_ARM_WFS'),
    ('TURN_ON_ARM_WFS','DRMI_TO_1F'),
# finalize RF lock
    ('DRMI_TO_1F','TURN_ON_VERTEX_WFS'),
    ('TURN_ON_VERTEX_WFS','TURN_ON_TIDAL_OFFLOAD'),
#    ('SPLIT_DARM_DRIVE_AT_RF','TURN_ON_TIDAL_OFFLOAD'),
    ('TURN_ON_TIDAL_OFFLOAD','TURN_ON_BS_STAGE2'),
    ('TURN_ON_BS_STAGE2','SET_CALIBRATION'),
#    ('TURN_ON_TIDAL_OFFLOAD','SET_CALIBRATION'),
    ('SET_CALIBRATION','SWITCH_AS_CENTERING_MTRX'),
    ('SWITCH_AS_CENTERING_MTRX','FINALIZE_RF_LOCK'),
    ('FINALIZE_RF_LOCK','RF_LOCKED'),
# low noise steps from here on in
    ('RF_LOCKED','DITHER_PRM'),
    #('DITHER_PRM','SWITCH_ACTUATORS'),
    #('SWITCH_ACTUATORS','SHUTTER_ALS'),
    ('DITHER_PRM','SHUTTER_ALS'),
    ('RF_LOCKED','SHUTTER_ALS'),
    ('SHUTTER_ALS','DARM_NORM_FIX'),
    ('DARM_NORM_FIX','TURN_ON_SOFT_DAMPING'),
    ('TURN_ON_SOFT_DAMPING','POWER_UP_10W'),
    #('POWER_UP_10W','TURN_ON_CALIBRATION_LINES'),
    #('TURN_ON_CALIBRATION_LINES','RF_LOCKED_10W'), #JCB
    #('POWER_UP_10W','SWITCH_ESD_TO_LOW_NOISE'),
    ('POWER_UP_10W','ASC_HARD_10W'),
    ('ASC_HARD_10W','CENTER_SPOTS_WITH_CAMERAS'),
    #('CENTER_SPOTS_WITH_CAMERAS','SWITCH_ESD_TO_LOW_NOISE'),
    ('CENTER_SPOTS_WITH_CAMERAS','SWITCH_ACTUATORS'),
    ('SWITCH_ACTUATORS','SWITCH_ESD_TO_LOW_NOISE'),
    ('SWITCH_ESD_TO_LOW_NOISE','RF_LOCKED_10W'),
    ('RF_LOCKED_10W','LOCKING_OMC',auto_path),
    ('LOCKING_OMC','RF_TO_DC_READOUT'),
    ('RF_LOCKED_10W','MANUAL_OMC',manual_path),
    ('MANUAL_OMC','RF_TO_DC_READOUT'),
    ('RF_TO_DC_READOUT','RELOCATE_DARM_OFFSET'),
    #('RF_TO_DC_READOUT','OFFLOAD_OMS'),
    #('OFFLOAD_OMS','RELOCATE_DARM_OFFSET'),
    ('RELOCATE_DARM_OFFSET','DC_READOUT'),
    ('DC_READOUT','DARM_TO_L2'),
    ('DARM_TO_L2','SPLIT_DARM_L1_DRIVE',split_l1_path),
    ('SPLIT_DARM_L1_DRIVE','SWITCH_SRC2_SENSOR'),
    ('DARM_TO_L2','SPLIT_DARM_L2_DRIVE',split_l2_path),
    ('SPLIT_DARM_L2_DRIVE','SWITCH_SRC2_SENSOR'),
    ('DARM_TO_L2','SWITCH_SRC2_SENSOR',single_drive_path),
    ('SWITCH_SRC2_SENSOR','POWER_UP_25W'),
    #('POWER_UP_25W','CENTER_SPOTS_WITH_DITHERS',3),
    #('CENTER_SPOTS_WITH_DITHERS','ASC_HARD_25W'),
    ('POWER_UP_25W','ASC_HARD_25W'),
    ('ASC_HARD_25W','POWER_UP_45W'),
    ('POWER_UP_45W','ASC_HIGH_POWER'),
    ('ASC_HIGH_POWER','POWER_UP_FINAL'),
    ('POWER_UP_FINAL','CLOSE_CORNER_DIVERTERS'),
#    ('CLOSE_CORNER_DIVERTERS','SWITCH_TO_PCALX'),
#    ('SWITCH_TO_PCALX','RAMP_DOWN_ETMY_BIAS'),
 #   ('RAMP_DOWN_ETMY_BIAS','SWITCH_ETMY_TO_LNLV'),
#    ('SWITCH_ETMY_TO_LNLV','RAMP_UP_ETMY_BIAS'),
#    ('RAMP_UP_ETMY_BIAS','SWITCH_TO_LOWNOISE_ESD'),
    ('CLOSE_CORNER_DIVERTERS','SWITCH_TO_LOWNOISE_ESD',other_esd_path),
    ('SWITCH_TO_LOWNOISE_ESD','SWITCH_OFF_OTHER_ESD'),
    ('CLOSE_CORNER_DIVERTERS','SWITCH_CURRENT_ESD_TO_LOWNOISE',current_esd_path),
    ('SWITCH_CURRENT_ESD_TO_LOWNOISE','SWITCH_OFF_OTHER_ESD'),
    ('SWITCH_OFF_OTHER_ESD','TURN_ON_DAC_DITHERS'),
    ('TURN_ON_DAC_DITHERS','TURN_ON_CALIBRATION_LINES'),
    #('SWITCH_OFF_OTHER_ESD','TWO_LEGGED_HARDASC'),
    ('TURN_ON_CALIBRATION_LINES','SET_IMC_FINAL'),
    ('SET_IMC_FINAL','SET_DCPD_CURRENT'),
    ('SET_DCPD_CURRENT','DRMI_FF'),
    ('DRMI_FF','FINALIZE_LOW_NOISE'),
    ('FINALIZE_LOW_NOISE', 'ASC_LOW_NOISE'),
    ('ASC_LOW_NOISE','WAIT_SQZ'),
    ('WAIT_SQZ','LOW_NOISE'),
    ('LOCKLOSS','DOWN'),
    #('MISALIGN_MIRRORS_AS_TEST', 'AS_TEST_10W'),
    #('AS_TEST_10W','TEST_FAST_SHUTTER'),
    #('TEST_FAST_SHUTTER', 'AS_TEST_RESET'),
    #('AS_TEST_RESET','ALIGN_MIRRORS_AS_TEST'),
    #('ALIGN_MIRRORS_AS_TEST','LOCK_ALS_ARMS'),
    ('CHECK_FAST_SHUTTER_AND_WATCHDOGS','MISALIGN_MIRRORS_AS_TEST'),
    ('MISALIGN_MIRRORS_AS_TEST','AS_TEST_4W'),
    ('AS_TEST_4W','TEST_FAST_SHUTTER'),
    ('TEST_FAST_SHUTTER','AS_TEST_RESET'),
    ('AS_TEST_RESET', 'ALIGN_MIRRORS_AS_TEST'),
    ]

#for repeat_state in ['DOWN']:
#    edges += [(repeat_state,repeat_state)]
edges += [('DOWN','DOWN')]
##################################################
# SVN $Id: IFO_LOCK.py 10763 2015-06-05 22:31:13Z jameson.rollins@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/sys/l1/guardian/IFO_LOCK.py $
##################################################
