#!/usr/bin/env python

import time
import cdsutils
import numpy as np

from cdsutils import CDSMatrix


dofs = {'L': '1',
      'P': '2',
      'Y': '3',
      }

esds = {'UL': '1',
      'LL': '2',
      'UR': '3',
      'LR': '4',
      }

eul2esd = CDSMatrix(
    'SUS-ETMX_L3_EUL2ESD',
    ramping=True,
    cols={'L': 1,
          'P': 2,
          'Y': 3,
          },
    rows={'UL': 1,
          'LL': 2,
          'UR': 3,
          'LR': 4,
          },
    )

lll = 0.25
ppp = 2.5254
yyy = 2.5254

matrix_default = np.matrix(
    [[ lll,    ppp,   -yyy],
     [ lll,   -ppp,   -yyy],
     [ lll,    ppp,    yyy],
     [ lll,   -ppp,    yyy]])

matrix_off = {
    'NONE': matrix_default,

    'UL': np.matrix(
        [[0,        0,        0    ],
         [2*lll,    0,       -2*yyy],
         [2*lll,    2*ppp,    0    ],
         [0,       -2*ppp,    2*yyy]]),

    'LL': np.matrix(
        [[2*lll,    0,       -2*yyy],
         [0,        0,        0    ],
         [0,        2*ppp,    2*yyy],
         [2*lll,   -2*ppp,    0    ]]),

    'UR': np.matrix(
        [[2*lll,    2*ppp,    0    ],
         [0,       -2*ppp,   -2*yyy],
         [0,        0,        0    ],
         [2*lll,    0,        2*yyy]]),

    'LR': np.matrix(
        [[0,        2*ppp,   -2*yyy],
         [2*lll,   -2*ppp,    0    ],
         [2*lll,    0,        2*yyy],
         [0,        0,        0    ]]),
    }


### Variables

#esd_highvolt_l2lgain = {'ETMX':1.2,'ETMY':1.0}
#esd_lownoise_l2lgain = {'ETMX':380,'ETMY':331}
#esd_lownoise_l2lgain = {'ETMX':613,'ETMY':562}

### Functions

def prep_eul2esd_stepping(optic,esdZero,nStep=40,tRamp=20.0):

    mtrx = matrix_off[esdZero]
    matrixSteps = []
    nStep = int(nStep)
    tRamp = float(tRamp)
    tStep = tRamp/nStep
    cmd_strings = {}

    ii=0
    for esd in ['UL','LL','UR','LR']:
        ii+=1
        jj=0
        for dof in ['L']:
        #for dof in ['L','P','Y']:
            jj+=1
            channel = 'SUS-'+optic+'_L3_EUL2ESD_SETTING_'+ esds[esd] + '_' + dofs[dof]
            current_val = ezca[channel]
            final_val = mtrx[ii-1,jj-1]
            diff = final_val - current_val

            # If no steps required move to next iteration
            if diff == 0:
                continue

            dStep = diff/nStep

            cmd_string = str(dStep) + ',' + str(nStep)
            cmd_strings[channel] = cmd_string

            # Make an array for stepping the matrix elements
            matrixSteps.append(cdsutils.Step(ezca,channel,cmd_string,time_step=tStep))

    return matrixSteps


def enforce_eul2esd(optic,esdZero):

    mtrx = matrix_off[esdZero]

    ii=0
    for esd in ['UL','LL','UR','LR']:
        ii+=1
        jj=0
        #for dof in ['L','P','Y']:
        for dof in ['L']:
            jj+=1
            channel = 'SUS-'+optic+'_L3_EUL2ESD_SETTING_'+esds[esd]+'_'+dofs[dof]
            current_val = ezca[channel]

            final_val = mtrx[ii-1,jj-1]

            diff = final_val - current_val

            # If at final value move on, otherwise write final value
            if diff == 0:
                continue
            ezca[channel] = final_val


def ramp_quadrant_output(optic,quadrants,gain,tramp):

    if quadrants[0] == 'NONE':
        return

    # Ramp ESDOUT gain
    for esd in quadrants:
        ezca['SUS-'+optic+'_L3_ESDOUTF_'+esd+'_TRAMP'] = tramp
    time.sleep(0.2)

    for esd in quadrants:
        ezca['SUS-'+optic+'_L3_ESDOUTF_'+esd+'_GAIN'] = gain
    time.sleep(0.1)


def switch_eul2esd_mtrx(coil,tramp):
    """Switch eul2osem matrix to remove drive from given coil

    This is done by reconfiguring the EUL2OSEM matrix to shut off output to 
    that coil.

    """

    eul2esd.TRAMP = tramp
    eul2esd.put_matrix(matrix_off[coil])
    eul2esd.load(wait=True)


def switch_quadrants_to_lownoise(optic,quadrants):

    if quadrants[0] == 'NONE':
        return

    # Turn off linearization
    for esd in quadrants:
        ezca['SUS-'+optic+'_L3_ESDOUTF_'+esd+'_LIN_BYPASS_SW'] = 1

    # Switch ESDs to correct state
    for esd in quadrants:
        ezca['SUS-'+optic+'_BIO_L3_'+esd+'_VOLTAGE_SW'] = 0
        ezca['SUS-'+optic+'_BIO_L3_'+esd+'_HVDISCONNECT_SW'] = 0
        ezca['SUS-'+optic+'_BIO_L3_'+esd+'_STATEREQ'] = 2

'''
############################################
if __name__ == '__main__':

    from ezca import Ezca

    Ezca().export()
'''
