import time
import sys
import numpy as np


offsets_fieldnames = ["Time","Comm Offset","Diff Offset","Comm VCO freq","Diff VCO Freq","X PLL Freq","Y PLL Freq"]
offsets_pathname = "/opt/rtcds/userapps/trunk/isc/l1/guardian/"
offsets_filename = offsets_pathname + "irResonanceOffsets.csv"


# Classes
class ScanAlsOffset(object):

    def __init__(self,ezca,scanChan_bit,xPowerChan,yPowerChan,stepSize,scanRange, sleepTime=0.2, rampTime=0.05):
        self._ezca = ezca
        self._scanChan_bit = scanChan_bit
        self._xPowerChan = xPowerChan
        self._yPowerChan = yPowerChan
        self._stepSize = stepSize
        self._scanRange = scanRange
        self._sleepTime = sleepTime
        self._rampTime = rampTime


        # TODO: make range actual range and not amplitude?
        # Number of steps
        #nSteps = int(2*scanRange/stepSize + 1)
        nSteps = int(scanRange/stepSize + 1)
        self._nSteps = nSteps        


        #offsetInitial = ezca[scanChan_bit + '_OFFSET']

        # Generate array of offsets
        #self._offsetArray = offsetInitial + np.linspace(-scanRange, scanRange, nSteps)
        #self._offsetArray = np.linspace(-scanRange, scanRange, nSteps)
        self._offsetArray = np.linspace(-scanRange/2.0, scanRange/2.0, nSteps)

        # Initialize power arrays
        self._xPowerArray = np.zeros(nSteps)
        self._yPowerArray = np.zeros(nSteps)

        # Move slowly to the start
        #ezca[scanChan_bit + '_TRAMP'] = 3
        #time.sleep(0.2)
        #ezca[scanChan_bit + '_OFFSET'] = offsetInitial - scanRange
        #ezca[scanChan_bit + '_OFFSET'] = offsetInitial - scanRange/2
        #time.sleep(4)

        # Set ramp time for scan        
        #ezca[scanChan_bit + '_TRAMP'] = rampTime
        self._counter = 0
        time.sleep(0.5)


    def scan(self):

        # Get the current 
        if self._counter == 0:
            self._offsetInitial = ezca[self._scanChan_bit + '_OFFSET']
            ezca[self._scanChan_bit + '_TRAMP'] = 3
            time.sleep(0.5)

        # Sweep offset
        if self._counter < self._nSteps:
            self._ezca[self._scanChan_bit + '_OFFSET'] = self._offsetInitial + self._offsetArray[self._counter]
            #TODO: do we need to wait on first iteration?
            # Allow the loop to follow
            if self._counter == 0:
                time.sleep(3)
                ezca[self._scanChan_bit + '_TRAMP'] = self._rampTime
                time.sleep(1)
            else:
                time.sleep(self._sleepTime)

            # Read power levels
            self._xPowerArray[self._counter] = ezca[self._xPowerChan]
            self._yPowerArray[self._counter] = ezca[self._yPowerChan]
            self._counter += 1
            return False, None, None

        # Find maximum X power and offset
        index_xPowerMax = np.argmax(self._xPowerArray)
        #xIRPowerMax = self._xPowerArray[index_xPowerMax]
        xOffset = self._offsetInitial + self._offsetArray[index_xPowerMax]
        xResonance = {'power':self._xPowerArray[index_xPowerMax],'frequency':xOffset}

        # Find maximum X power and offset
        index_yPowerMax = np.argmax(self._yPowerArray)
        #yIRPowerMax = self._yPowerArray[index_yPowerMax]
        yOffset = self._offsetInitial + self._offsetArray[index_yPowerMax]
        yResonance = {'power':self._yPowerArray[index_yPowerMax],'frequency':yOffset}

        return True, xResonance, yResonance


    def flipOffset(self, flipChan_bit, offsetPlus, offsetMinus, wait=4):
        # Ramp slowly
        self._ezca[flipChan_bit + '_TRAMP'] = 3
        time.sleep(0.2)

        midpoint = (offsetPlus + offsetMinus)/2

        # Change offset
        if self._ezca[flipChan_bit + '_OFFSET'] < midpoint:
        #if self._ezca[flipChan_bit + '_OFFSET'] - self._scanRange/2 < midpoint:
                self._ezca[flipChan_bit + '_OFFSET'] = offsetPlus
        else:
                self._ezca[flipChan_bit + '_OFFSET'] = offsetMinus

        time.sleep(wait)

        # Put offset ramp back to 1 sec
        self._ezca[flipChan_bit + '_TRAMP'] = 1
        # Reset the counter for the scan
        self._counter = 0


    def reset(self):
        self._counter = 0

#-------------------------------------------------------------------------------
# Functions

def flipOffset(flipChan_bit, offsetPlus, offsetMinus, currPos, wait=4):
        # Ramp slowly
        ezca[flipChan_bit + '_TRAMP'] = 3
        time.sleep(0.2)

        midpoint = (offsetPlus + offsetMinus)/2

        # Change offset
        if currPos:
            ezca[flipChan_bit + '_OFFSET'] = offsetMinus
        else:
            ezca[flipChan_bit + '_OFFSET'] = offsetPlus

        time.sleep(wait)

        # Put offset ramp back to 1 sec
        ezca[flipChan_bit + '_TRAMP'] = 1

        return not currPos

