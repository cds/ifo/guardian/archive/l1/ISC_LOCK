#!/usr/bin/env python

import time

################################
# turn off the centering servos and WFS

DRMI_ASC_FILTERS = [
    'DC1',
    'DC2',
    'DC3',
    'DC4',
    'DC5',
    'INP2',
    'PRC1',
    'PRC2',
    'MICH',
    'SRC1',
    'SRC2',
    ]

def drmi_wfs_off():

    for dof in DRMI_ASC_FILTERS:
        ezca.switch('ASC-' + dof + '_P', 'INPUT', 'OFF')
        ezca.switch('ASC-' + dof + '_Y', 'INPUT', 'OFF')

    time.sleep(1)

    # clear centering and asc servoes
    for dof in DRMI_ASC_FILTERS:
        ezca['ASC-' + dof + '_P_RSET'] = 2
        ezca['ASC-' + dof + '_Y_RSET'] = 2

    # clear PR2/SR2 M1 controls
    ezca['SUS-PR2_M1_LOCK_P_RSET'] = 2
    ezca['SUS-PR2_M1_LOCK_Y_RSET'] = 2
    ezca['SUS-SR2_M1_LOCK_P_RSET'] = 2
    ezca['SUS-SR2_M1_LOCK_Y_RSET'] = 2

##################################################

if __name__ == '__main__':

    from ezca import Ezca

    Ezca().export()

    drmi_wfs_off()
