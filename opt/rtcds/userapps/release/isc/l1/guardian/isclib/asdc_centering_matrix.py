#!/usr/bin/env python

import time
import numpy as np
import cdsutils

chanbit = 'ASC-OUTMATRIX_'

dofs ={
    'DC3': 14,
    'DC4': 15,
    }

oms ={
    'OM1': 18,
    'OM2': 19,
    'OM3': 20,
}

asdc_omtrx_ind_pit = cdsutils.CDSMatrix(
    chanbit+'P',
    cols = dofs,
    rows = oms,
    )

asdc_omtrx_ind_yaw = cdsutils.CDSMatrix(
    chanbit+'Y',
    cols = dofs,
    rows = oms,
    )

asdc_omtrx_vals = {
          'INITIAL_P': np.matrix(
                     [[-0.0248,   1.32], # was [[0.0171,   0.6916],
                     [-1.86, -0.683],    # was [-1.0781, -0.21],
                     [3.94,  -1.51]]),   # was [2.7447,  -0.6998]]),

          'INITIAL_Y': np.matrix(
                    [[-0.105, 1.21],   # was [[-0.0301, 0.6959],
                     [1.92,  0.722],   # was [1.0717,  0.1894],
                     [2.83, -1.10]]),  # was [2.4848, -0.6029]]),

           'FINAL_P': np.matrix(
                    [[1.04,  0.0],   # was [[0.7712,  0.0],
                     [-2.41, 0.0],   # was  [-1.3071, 0.0],
                     [2.73,  0.0]]), # was  [1.9816,  0.0]]),

           'FINAL_Y': np.matrix(
                    [[0.836,   0.0], # was [[0.849,   0.0], 
                     [2.48,  0.0],   # was  [1.3104,  0.0], 
                     [1.98,  0.0]]), # was  [1.7251,  0.0]]),
           }



# Switch the asdc out matrix, use 'INITIAL' for CARM offset reduction,
# use 'FINAL' for after CARM offset reduction
def switch_to_mtrx(mode):
    asdc_omtrx_ind_pit.put_matrix(asdc_omtrx_vals[mode+'_P'])
    asdc_omtrx_ind_yaw.put_matrix(asdc_omtrx_vals[mode+'_Y'])
#FIXME: This doesn't work properly!!!! put_matrix is for whole matrix?

'''
def ramp_to_mtrx_construct(mode):

    matrixSteps = []
    for rot in ['P','Y']:
        mtrx = asdc_omtrx_vals[mode+'_'+rot]

        ii=0
        for dof in ['DC3','DC4']:
            ii+=1
            jj=0
            for om in ['OM1','OM2','OM3']:
                jj+=1
                channel = chanbit+rot+'_'+oms[om]+'_'dofs[dof]
                current_val = ezca[channel]

                final_val = mtrx[jj-1][ii-1]

                diff = final_val - current_val

                # If no steps required move to next iteration
                if diff == 0:
                    continue

                dStep = diff/nStep

                cmd_string = str(dStep) + ',' + str(nStep)

                # Make an array for stepping the matrix elements
                matrixSteps.append(cdsutils.Step(ezca,channel,cmd_string,time_step=tStep))
                return matrixSteps

'''



