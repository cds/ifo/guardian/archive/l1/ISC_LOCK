# <heat_state> = CENTRAL, ANNULAR or NONE
# 
# flipper 1, sensor 1: central
# flipper 2, sensor 2: annular
#
# Channel name suffixes
# FLIP1MON : sensor
# ACTFLIP1 : actuator
#
# Written by Aidan Brooks. 15-Oct-2014
# Modified heavily by Joseph Betzwieser 5-Feb-2024
# Modified by Carl B & Karla R March-22-2024
# Modified by  Karla Ramirez 04 16 2024  - Added flipstate for TCS SIM block

import time

# define functions
def risingEdge(channel):
    # initialize to Invalid
    ezca[channel] = 'Invalid'
    time.sleep(0.1)
    # raise to Valid - this is the rising edge
    ezca[channel] = 'Valid'
    # reset to Invalid
    time.sleep(0.25)
    ezca[channel] = 'Invalid'

def setTCSMask(optic,state):

    if not (state in ['NONE','CENTRAL','ANNULAR']):
        raise RuntimeError('Unknown state request %r' % state)

    #1 is Central, 2 is Annular
    #Mon readback 0 or Invalid is in beam path, 1 or Valid is out of beam path
    flipAct1 = 'TCS-'+optic+'_CO2_ACTFLIP1'
    flipAct2 = 'TCS-'+optic+'_CO2_ACTFLIP2'
    sens1 = 'TCS-'+optic+'_CO2_FLIP1MON'
    sens2 = 'TCS-'+optic+'_CO2_FLIP2MON'
    filt1 = 'TCS-SIM_'+optic+'_SUB_DEFOCUS_CO2'
    filtNM = 'TCS-SIM_'+optic+'_SUB_DEF_CO2_NOMASK'
    filtC = 'TCS-SIM_'+optic+'_SUB_DEF_CO2_CENT'
    filtA = 'TCS-SIM_'+optic+'_SUB_DEF_CO2_ANN'
    flipstate = 'TCS-SIM_'+optic+'_CO2_FLIP_STATE'
    flipstatus1 = ezca[sens1]
    flipstatus2 = ezca[sens2]

    # throw an exception if the flipper channels are not read
    if flipstatus1 is None:
        raise RuntimeError('Cannot connect to %r' % sens1)
    if flipstatus2 is None:
        raise RuntimeError('Cannot connect to %r' % sens2)

    flipperstate = 0;
    if flipstatus1 == 0 :
        flipperstate = flipperstate + 1
    if flipstatus2 == 0 :
        flipperstate = flipperstate + 2

    # flipperstate 0: NONE
    # flipperstate 1: CENTRAL: flipper 1 down, flipper 2 up
    # flipperstate 2: ANNULAR: flipper 2 down, flipper 1 up
    # flipperstate 3: both down

    if state  == 'CENTRAL':
        ezca[flipstate] = 2
        #ezca.switch(filt1,'FM2','FM3','OFF')
        #ezca.switch(filt1,'FM1','ON')
        ezca.switch(filt1,'FM1','FM2','FM3','FM10','OFF')
        ezca.switch(filtC,'FM1','FM10','ON') 
        ezca.switch(filtNM,'FM1','FM10','OFF')
        ezca.switch(filtA,'FM1','FM10','OFF')
        if flipperstate == 0:
            risingEdge(flipAct1)    # lower central
        elif flipperstate == 2:
            risingEdge(flipAct2)    # raise annular then lower central
            time.sleep(0.5)
            risingEdge(flipAct1)
        elif flipperstate == 3:
            risingEdge(flipAct2)    # raise annular

    if state == 'ANNULAR':
        ezca[flipstate] = 3
        #ezca.switch(filt1,'FM1','FM3','OFF')
        #ezca.switch(filt1,'FM2','ON')
        ezca.switch(filt1,'FM1','FM2','FM3','FM10','OFF')
        ezca.switch(filtC,'FM1','FM10','OFF') 
        ezca.switch(filtNM,'FM1','FM10','OFF')
        ezca.switch(filtA,'FM1','FM10','ON')
        if flipperstate == 0:
            risingEdge(flipAct2)    # lower annular
        elif flipperstate == 1:
            risingEdge(flipAct1)    # raise central & then lower annular
            time.sleep(0.5)
            risingEdge(flipAct2)
        elif flipperstate == 3:
            risingEdge(flipAct1)    # raise central

    if state == 'NONE': 
        ezca[flipstate] = 1
        #ezca.switch(filt1,'FM1','FM2','OFF')
        #ezca.switch(filt1,'FM3','ON')
        ezca.switch(filt1,'FM1','FM2','FM3','FM10','OFF')
        ezca.switch(filtC,'FM1','FM10','OFF') 
        ezca.switch(filtNM,'FM1','FM10','ON')
        ezca.switch(filtA,'FM1','FM10','OFF')
        if flipperstate == 1:
            risingEdge(flipAct1)    # raise central
        elif flipperstate == 2:
            risingEdge(flipAct2)    # raise both
        elif flipperstate == 3:
            risingEdge(flipAct1)
            time.sleep(0.5)
            risingEdge(flipAct2)    # raise annular
