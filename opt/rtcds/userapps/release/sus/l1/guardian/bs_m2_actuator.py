#!/usr/bin/env python

import time
import numpy as np

from cdsutils import CDSMatrix


ACTUATOR_DEFAULT = 2
ACTUATOR_LOWNOISE = 3

eul2osem = CDSMatrix(
    'SUS-BS_M2_EUL2OSEM',
    ramping=True,
    cols={'L': 1,
          'P': 2,
          'Y': 3,
          },
    rows={'UL': 1,
          'LL': 2,
          'UR': 3,
          'LR': 4,
          },
    )

lll = 0.25
ppp = 3.5361
yyy = 3.5361

matrix_default = np.matrix(
    [[ lll,    ppp, -yyy],
     [ lll,   -ppp, -yyy],
     [ lll,    ppp,  yyy],
     [ lll,   -ppp,  yyy]])

matrix_off = {
    'NONE': matrix_default,

    'UL': np.matrix(
        [[0,      0,        0    ],
         [2*lll,  0,       -2*yyy],
         [2*lll,  2*ppp,    0    ],
         [0,     -2*ppp,    2*yyy]]),

    'LL': np.matrix(
        [[2*lll,  0,       -2*yyy],
         [0,      0,        0    ],
         [0,      2*ppp,    2*yyy],
         [2*lll, -2*ppp,    0    ]]),

    'UR': np.matrix(
        [[2*lll,  2*ppp,    0    ],
         [0,     -2*ppp,   -2*yyy],
         [0,      0,        0    ],
         [2*lll,  0,        2*yyy]]),

    'LR': np.matrix(
        [[0,      2*ppp,   -2*yyy],
         [2*lll, -2*ppp,    0    ],
         [2*lll,  0,        2*yyy],
         [0,      0,        0    ]]),
    }


def switch_lownoise_all():
    """Switch the BS M2 actuators to LOW NOISE

    This is done by going through each coil in sequence, reconfiguring
    the EUL2OSEM matrix to shut off output to that coil, then switching
    the actuator.

    The EUL2OSEM matrix is reset after all actuators are switched.

    """
    # get the current matrix
    current = eul2osem.get_matrix()

    eul2osem.TRAMP = 6 

    # switch actuator for each coil
    for coil in ['UL', 'LL', 'UR', 'LR']:
        # start by reconfiguring the EUL2OSEM matrix shut off the coil
        # output and compensate with the other coils
        print('Switching off %s...' % coil)
        eul2osem.put_matrix(matrix_off[coil])
        eul2osem.load(wait=True)

        #Settle time for analog electronics
        time.sleep(eul2osem.TRAMP+0.5)

        # switch actuator
        print('Switching %s actuator...' % coil)
        ezca['SUS-BS_M2_COILOUTF_'+coil+'_RSET'] = 2
        time.sleep(1) # AP changed from 1 to 0.1 Feb 16th
        # We make 2 small jumps instead of 1 big jump
        ezca['SUS-BS_BIO_M2_'+coil+'_STATEREQ'] = 1
        time.sleep(1) # AP changed from 1 to 0.1
        ezca['SUS-BS_BIO_M2_'+coil+'_STATEREQ'] = ACTUATOR_LOWNOISE
        time.sleep(3) # AP changed from 3 to 1

    # then go back to the default
    #eul2osem.put_matrix(matrix_default)
    eul2osem.put_matrix(current)
    eul2osem.load(wait=True)


def switch_eul2osem_mtrx(coil,tramp):
    """Switch eul2osem matrix to remove drive from given coil

    This is done by reconfiguring the EUL2OSEM matrix to shut off output to 
    that coil.

    """

    eul2osem.TRAMP = tramp
    eul2osem.put_matrix(matrix_off[coil])
    eul2osem.load(wait=True)


def switch_coil_to_lownoise(coil):
    """Switch the BS M2 coil to LOW NOISE

    """
    if coil == 'NONE':
        return

    ezca['SUS-BS_M2_COILOUTF_'+coil+'_RSET'] = 2
    time.sleep(1) # AP changed from 1 to 0.1 Feb 16th
    # We make 2 small jumps instead of 1 big jump
    ezca['SUS-BS_BIO_M2_'+coil+'_STATEREQ'] = 1
    time.sleep(1) # AP changed from 1 to 0.1
    ezca['SUS-BS_BIO_M2_'+coil+'_STATEREQ'] = ACTUATOR_LOWNOISE
    time.sleep(3) # AP changed from 3 to 1
    


def reset_all():
    """Reset BS M2 actuators to default acquisition state.

    Actuators are switched to default state, and EUL2OSEM matrix is
    reset to default.  Done in a "rough" way, without shutting off
    coil drive.

    """
    eul2osem.TRAMP = 0
    eul2osem.zero()
    eul2osem.load(wait=True)
    for coil in ['UL', 'LL', 'UR', 'LR']:
        ezca['SUS-BS_BIO_M2_'+coil+'_STATEREQ'] = ACTUATOR_DEFAULT
    eul2osem.put_matrix(matrix_default)
    eul2osem.load(wait=True)


##################################################

if __name__ == '__main__':
    import os
    import sys
    from ezca import Ezca
    Ezca().export()

    pgrm = os.path.basename(sys.argv[0])
    try:
        cmd = sys.argv[1].upper()
    except IndexError:
        sys.exit("usage: %s [LOWNOISE|RESET]" % pgrm)

    if cmd == 'LOWNOISE':
        switch_all()
    elif cmd == 'RESET':
        reset_all()
